jQuery(document).ready(function($){

    /*-----NAVIGATION-----*/
    //Ensure mobile nav is always hidden when back / forward cache is used 
    function pageShown(evt)
    {
        if (evt.persisted) {
            $('#nav_mobile_toggle').hide();
          }
    }

    if(window.addEventListener) {
      window.addEventListener("pageshow", pageShown, false);
    } else {
      window.attachEvent("pageshow", pageShown, false);
    }

    /*-----IE 7/8-----*/
    if ($('html').hasClass('lt-ie10')) {  
      $('#mce-EMAIL').after('<small>Email Address</small>');      
    };
      
    /*-----FOOTER SCROLL TO TOP-----*/      
    /*$("#back_top").hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#back_top').fadeIn();
        } else {
            $('#back_top').fadeOut();
        }
    });*/
    // scroll body to 0px on click
    $('.back_top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    });
        
    /*-----INTERNAL LINKS SCROLL TO-----*/            
    $('.int_scroll').click(function () {
        var e = $(this).attr('href');
        $('body,html').animate({
            scrollTop: $(e).offset().top
        }, 600);
        return false;
    });
    
    /*-----MOBILE NAV-----*/                
    $('#nav_mobile_menu a').click(function(e) {
        $('#nav_mobile_toggle').slideToggle();
        e.preventDefault();
    });

    /*-----TABS-----*/
    $('ul.home_search_tab_headings').each(function(){
      // For each set of tabs, we want to keep track of
      // which tab is active and it's associated content
      var $active, $content, $links = $(this).find('a');

      // If the location.hash matches one of the links, use that as the active tab.
      // If no match is found, use the first link as the initial active tab.
      $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
      $active.addClass('active');
      $content = $($active.attr('href'));

      // Hide the remaining content
      $links.not($active).each(function () {
        $($(this).attr('href')).hide();
      });

      // Bind the click event handler
      $(this).on('click', 'a', function(e){
        // Make the old tab inactive.
        $active.removeClass('active');
        $content.hide();

        // Update the variables with the new link and content
        $active = $(this);
        $content = $($(this).attr('href'));

        // Make the tab active.
        $active.addClass('active');
        $content.show();

        // Prevent the anchor's default click action
        e.preventDefault();
      });
    });

    $("#home_carousel_load").flexisel({
      visibleItems: 3,
      animationSpeed: 400,
      autoPlay: true,
      autoPlaySpeed: 6000,
      pauseOnHover: true,
      enableResponsiveBreakpoints: true,
      responsiveBreakpoints: {
        portrait: {
          changePoint:480,
          visibleItems: 1
        }, 
        landscape: {
          changePoint:640,
          visibleItems: 2
        },
        tablet: {
          changePoint:768,
          visibleItems: 3
        }
      }
    });

});
