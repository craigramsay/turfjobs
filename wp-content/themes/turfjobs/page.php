<?php
/**
 * Default template for full width pages
 */
get_header(); ?>

	<div id="content" class="clearfix">
		<div class="col col_span_10_10">

			<? if (!is_page('jobs')) { ?>
			<div id="home_banner" class="clearfix">
				<div class="col col_span_10_10">
					<h3>List your job vacancy for just £99</h3>
					<?php if ( is_user_logged_in() ) { ?>
						<a href="<?php echo esc_url( site_url( 'employer/add-job' ) ); ?>" class="button">Add Job</a>
					<?php } else { ?>
						<a href="<?php echo esc_url( site_url( 'register' ) ); ?>" class="button">Register Now</a>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; // end of the loop. ?>
			
		</div>
	</div>

<?php get_footer(); ?>