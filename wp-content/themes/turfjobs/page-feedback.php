<?php
/**
 * Template Name: Visitor Feedback
 */
get_header(); ?>

	<div id="content" class="clearfix">
		<div class="col col_span_10_10">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; // end of the loop. ?>

			<?php if (get_field('visitor_feedback_repeater')):
				while(has_sub_field('visitor_feedback_repeater')): ?>
					<div class="visitor_feedback clearfix">
						<div class="col col_span_2_10">
							<?php $image = wp_get_attachment_image_src(get_sub_field('visitor_image'), 'vistor_feedback_main'); ?>
							<img src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo (get_sub_field('visitor_name')) ?>" />
						</div>
						<div class="col col_span_8_10">
							<?php the_sub_field('visitor_quote'); ?>
							<p class="visitor_feedback_name"><?php esc_attr( the_sub_field('visitor_name') ); ?></p>
							<p class="visitor_feedback_position"><?php esc_attr( the_sub_field('visitor_position') ); ?></p>
						</div>
					</div>
				<?php endwhile;
			endif; ?>
		</div>
	</div>

<?php get_footer(); ?>