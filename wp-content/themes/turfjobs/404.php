<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="content" class="clearfix">
		<div class="col col_span_10_10">

			<article id="post-0" class="post error404 no-results not-found">
					<h1 class="entry-title">We're sorry the page could not be found.</h1>
					<p>Please try using the main navigation.</p>
			</article><!-- #post-0 -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>