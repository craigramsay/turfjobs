<?php
/**
 * The template used for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if (!is_page('jobs')) : ?>
		<h1><?php the_title(); ?></h1>
	<?php endif; ?>
		
	<?php if (is_page('employer') && $_GET['action'] && $_GET['action'] == 'profile_updated') {
		echo '<p class="message">Your profile has been updated.</p>';
	}
	?>
	<?php the_content(); ?>
</article><!-- #post -->
