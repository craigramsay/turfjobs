<li <?php job_listing_class(); ?>>
	<h4 class="job_listings_title"><a href="<?php the_job_permalink(); ?>"><?php the_title(); ?></a></h4>
	<table class="job_listings_table">
		<tr>
			<td>Job Type</td>
			<td class="job_listings_job_type <?php echo get_the_job_type() ? sanitize_title( get_the_job_type()->slug ) : ''; ?>"><?php the_job_type(); ?></td>
		</tr>
		<tr>
			<td>Date Posted</td>
			<td><?php esc_attr (the_time('d/m/Y') ); ?></td>
		</tr>
		<tr>
			<td>Area</td>
			<td><?php esc_attr (the_job_location() ); ?></td>
		</tr>
		<tr>
			<td>Organisation Type</td>
			<td><?php esc_attr (the_job_organisation_type() ); ?></td>
		</tr>			
	</table>	
	<div class="job_listings_description">		
		<p><?php echo strip_tags((preg_replace('/\s+?(\S+)?$/', '', substr(get_the_content(), 0, 300)))) . '... '; ?><a href="<?php the_job_permalink(); ?>">Read More</a></p>
	</div>
	<?php if (single_job_listing_post_meta('', '_job_company_hide_details', true, false) != 1) { ?>
	<?php if (the_company_logo()) { ?>
		<img src="<?php echo esc_url( the_company_logo() ); ?>" alt="Company Logo" class="job_listings_logo" />
	<?php } ?>
	<?php } ?>
</li>