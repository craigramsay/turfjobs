<?php
/**
 * Job Submission Form
 */
if ( ! defined( 'ABSPATH' ) ) exit;

global $job_manager;
?>

<?php if ($_GET['action'] == 'edit')
	echo '<p>Use the sections and facilities below to help you best describe the job you wish to advertise. These details will appear on our website.</p>';
?>

<form action="<?php echo $action; ?>" method="post" id="submit-job-form" class="job-manager-form" enctype="multipart/form-data">

	<?php if ( job_manager_user_can_post_job() ) : ?>

		<!-- Job Information Fields -->
		<?php do_action( 'submit_job_form_job_fields_start' ); ?>

		<fieldset id="fieldset_add_job_details" class="employer_fieldset">
			<legend>Job Details</legend>
			<?php foreach ( $job_fields as $key => $field ) : ?>
				<div class="field clearfix">
					<div class="col col_span_3_10">
						<label for="<?php esc_attr_e( $key ); ?>"><?php echo $field['label'] . ' <small>' . $field['label_type'] . '</small>' ; ?></label>
					</div>
					<div class="col col_span_6_10">	
						<?php get_job_manager_template( 'form-fields/' . $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
						<?php if ($error_messages[$key . '_error'][0]) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
					</div>
					<div class="col col_span_1_10">
						<div class="field_error_icon"></div>					
					</div>
				</div>				
			<?php endforeach; ?>
		</fieldset>

		<?php do_action( 'submit_job_form_job_fields_end' ); ?>

		<?php do_action( 'submit_job_form_company_fields_start' ); ?>

		<fieldset id="fieldset_add_job_details" class="employer_fieldset">
			<legend>Company Details</legend>
			<!--<p>Amend your company details below if this job is not based at your registered address.</p>-->
			<?php foreach ( $company_fields as $key => $field ) : ?>
				<div class="field clearfix">
					<div class="col col_span_3_10">
						<label for="<?php esc_attr_e( $key ); ?>"><?php echo $field['label'] . ' <small>' . $field['label_type'] . '</small>'; ?></label>
					</div>
					<div class="col col_span_6_10">	
						<?php get_job_manager_template( 'form-fields/' . $field['type'] . '-field.php', array( 'key' => $key, 'field' => $field ) ); ?>
						<?php if ($error_messages[$key . '_error'][0]) echo '<p class="error">' . $error_messages[$key . '_error'][0] . '</p>'; ?>
					</div>
					<div class="col col_span_1_10">
						<div class="field_error_icon"></div>					
					</div>
				</div>	
			<?php endforeach; ?>
		</fieldset>

		<?php do_action( 'submit_job_form_company_fields_end' ); ?>

		<?php wp_nonce_field( 'submit_form_posted' ); ?>
		<input type="hidden" name="job_manager_form" value="<?php echo $form; ?>" />
		<input type="hidden" name="job_id" value="<?php echo esc_attr( $job_id ); ?>" />
		<input type="hidden" name="step" value="0" />
		<input type="submit" name="submit_job" id="submit" class="button button_full_width" value="<?php esc_attr_e( $submit_button_text ); ?>" />

	<?php else : ?>

		<?php do_action( 'submit_job_form_disabled' ); ?>

	<?php endif; ?>
</form>