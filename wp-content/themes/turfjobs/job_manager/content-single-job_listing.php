<?php global $job_manager; ?>

	<?php if ( $post->post_status == 'expired' ) : ?>

		<div class="job-manager-info"><?php _e( 'This job listing has expired.', 'job_manager' ); ?></div>

	<?php elseif ( $post->post_status == 'pending_payment' && !is_user_logged_in() ) : ?>

			<div class="job-manager-info"><?php _e( 'This job listing does not exist.', 'job_manager' ); ?></div>

	<?php else: ?>

			<div id="job_listing_meta" class="clearfix">
				<div class="col col_span_3_10">
					<p id="job_listing_type" class="<?php echo get_the_job_type() ? sanitize_title( get_the_job_type()->slug ) : ''; ?>"><?php esc_attr( the_job_type() ); ?></p>
					<?php // Check if a turf jobs added listing and whether to display organisation type from the post.
					if ( get_field( 'turf_jobs_listing' ) ) { ?>
						<p id="job_listing_organisation_type"><?php echo esc_html( get_field( 'organisation_type' )->name ); ?></p>
					<?php // Else will be a employers added job.
					} else { ?>
						<p id="job_listing_organisation_type"><?php esc_attr( the_job_organisation_type() ); ?></p>
					<?php } ?>	
				</div>
				<div class="col col_span_7_10">
					<div class="job_listing_meta_data">
						<p><span class="job_listing_meta_label">Date Posted</span>
						<span id="job_listing_posted"><?php esc_attr( the_time('d/m/Y') ); ?></span></p>
						<p><span class="job_listing_meta_label">Closing Date</span>
						<span id="job_listing_expires"><?php echo date( "d/m/Y", strtotime( single_job_listing_post_meta('', '_job_expires', true, false) ) ); ?></span></p>
					</div>
					<div class="job_listing_meta_data">
						<p><span class="job_listing_meta_label">Salary Type</span>
						<span id="job_listing_salary_type"><?php echo esc_attr( ucwords(str_replace('-',' ', single_job_listing_post_meta('', '_job_salary_type', true, false) ) ) ); ?></span></p>
						<p><span class="job_listing_meta_label">Salary</span>
						<span id="job_listing_salary_amount"><?php echo single_job_listing_post_meta('', '_job_salary_amount', true, false) != '' ? '&pound;' . esc_attr( single_job_listing_post_meta('', '_job_salary_amount', true, false) ) : '-'; ?></span></p>
					</div>
					<div class="job_listing_meta_data">
						<p><span class="job_listing_meta_label">Area</span>
						<span id="job_listing_location"><?php esc_attr( the_job_location() ); ?></span></p>
						<p><span class="job_listing_meta_label">Reference</span>
						<span id="job_listing_reference"><?php esc_attr( single_job_listing_post_meta('', '_job_employers_reference') ); ?></span></p>
					</div>
				</div>
			</div>

			<div id="job_listing_content" class="clearfix">
				<div class="col col_span_7_10">
					<h3>Description</h3>
					<?php the_content(); ?>
					<h3>Person Specification</h3>
					<?php esc_attr( single_job_listing_post_meta('', '_job_person_specification') ); ?>
					<h3>How to Apply</h3>
					<?php esc_attr( single_job_listing_post_meta('', '_job_how_to_apply') ); ?>
					<h3>Share on a Social Network</h3>
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
					<a class="addthis_button_facebook"></a>
					<a class="addthis_button_twitter"></a>
					<a class="addthis_button_google_plusone_share"></a>
					<a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52b87ac90cd5ff2d"></script>
					<!-- AddThis Button END -->
				</div>
				<div class="col col_span_3_10">
					<?php // Check if a turf jobs added listing and whether to display logo.
					if ( get_field( 'turf_jobs_listing' ) ) { ?>
						<?php if (the_company_logo()) { ?>
							<img src="<?php echo esc_url( the_company_logo() ); ?>" alt="Company Logo" class="job_listings_logo" />
						<?php } ?>
					<?php // Else will be a employers added job.
					} elseif (single_job_listing_post_meta('', '_job_company_hide_details', true, false) != 1) { ?>
						<?php if (the_company_logo()) { ?>
							<img src="<?php echo esc_url( the_company_logo() ); ?>" alt="Company Logo" class="job_listings_logo" />
						<?php } ?>
					<?php } ?>
				</div>
			</div>

			<?php // Check if a turf jobs added listing and whether to display company details from the post. ?>
			<?php if ( get_field( 'turf_jobs_listing' ) ) { ?>
				<?php if ( get_field( 'company_name' ) ) { ?>
					<div id="job_listing_company" class="clearfix">
						<div class="col col_span_10_10">
							<h3>Company Information</h3>
							<p>
								<?php echo esc_html( get_field( 'company_name' ) ); ?><br />
								<?php echo esc_html( get_field( 'company_address' ) ); ?><br />
								<?php echo esc_html( get_field( 'company_town' ) ); ?><br />
								<?php echo esc_html( get_field( 'company_town' ) ); ?><br />
								<?php echo esc_html( get_field( 'company_county' ) ); ?><br />
								<?php echo esc_html( strtoupper( get_field( 'company_postcode' ) ) ); ?>
							</p>
							<div id="job_listing_company_social">
								<?php if ( get_field( 'company_website' ) ) { ?>
									<a href="<?php echo esc_attr( get_field( 'company_website' ) ); ?>" target="_blank" id="job_listing_company_website">Company Website</a>
								<?php } ?>
								<?php if ( get_field( 'company_twitter' ) ) { ?>
									<a href="<?php echo esc_attr( get_field( 'company_twitter' ) ); ?>" target="_blank" id="job_listing_company_website">Company Twitter</a>
								<?php } ?>
								<?php if ( get_field( 'company_facebook' ) ) { ?>
									<a href="<?php echo esc_attr( get_field( 'company_facebook' ) ); ?>" target="_blank" id="job_listing_company_website">Company Facebook</a>
								<?php } ?>
								<?php if ( get_field( 'company_googleplus' ) ) { ?>
									<a href="<?php echo esc_attr( get_field( 'company_googleplus' ) ); ?>" target="_blank" id="job_listing_company_website">Company Google Plus</a>
								<?php } ?>
							</div>
						</div>
					</div>
				<?php } ?>
			<?php // Else will be a employers added job.
			} elseif (single_job_listing_post_meta('', '_job_company_hide_details', true, false) != 1) { ?>
				<div id="job_listing_company" class="clearfix">
					<div class="col col_span_10_10">
						<h3>Company Information</h3>
						<p>
							<?php esc_attr( single_job_listing_user_meta('', '_company_name') ); ?><br />
							<?php esc_attr( single_job_listing_user_meta('', '_company_address') ); ?><br />
							<?php esc_attr( single_job_listing_user_meta('', '_company_town') ); ?><br />
							<?php echo esc_attr( ucwords(str_replace('-',' ', single_job_listing_user_meta('', '_company_county', true, false) ) ) ); ?><br />
							<?php echo esc_attr( strtoupper(single_job_listing_user_meta('', '_company_postcode', true, false) ) ); ?>
						</p>
						<div id="job_listing_company_social">
							<?php if (single_job_listing_user_meta('', '_company_website', true, false)) { ?>
							<a href="<?php esc_attr( single_job_listing_user_meta('', '_company_website') ); ?>" target="_blank" id="job_listing_company_website"><?php _e( 'Company Website', 'job_manager' ); ?></a>
							<?php } ?>
							<?php if (single_job_listing_user_meta('', '_company_twitter', true, false)) { ?>
							<a href="<?php esc_attr( single_job_listing_user_meta('', '_company_twitter') ); ?>" target="_blank" class="job_listing_company_social" id="job_listing_company_twitter"><?php _e( 'Company Twitter', 'job_manager' ); ?></a>
							<?php } ?>
							<?php if (single_job_listing_user_meta('', '_company_facebook', true, false)) { ?>
							<a href="<?php esc_attr( single_job_listing_user_meta('', '_company_facebook') ); ?>" target="_blank" class="job_listing_company_social" id="job_listing_company_facebook"><?php _e( 'Company Facebook', 'job_manager' ); ?></a>
							<?php } ?>
							<?php if (single_job_listing_user_meta('', '_company_googleplus', true, false)) { ?>
							<a href="<?php esc_attr( single_job_listing_user_meta('', '_company_googleplus') ); ?>" target="_blank" class="job_listing_company_social" id="job_listing_company_googleplus"><?php _e( 'Company Google Plus', 'job_manager' ); ?></a>
							<?php }	?>
						</div>
					</div>
				</div>			
			<?php } ?>										

			<?php if ( is_position_filled() ) : ?>
				<?php _e( 'This position has been filled', 'job_manager' ); ?>
			<?php endif; ?>		

	<?php endif; ?>