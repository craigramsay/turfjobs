<?php wp_enqueue_script( 'wp-job-manager-ajax-filters' ); ?>
<?php  
//check for get from homepage search
if (isset($_GET))
	$homepage_query = $_GET;
	
if (!empty($homepage_query)) {
	//if more than one value and someone is tinkering remove last values
	if (count($homepage_query) > 1) {
		array_splice($homepage_query, 1);
	}
	//set to numerical array just to run pregmatch
	$numerical_homepage_query = array_values($homepage_query);
	if(!preg_match('/^[0-9a-zA-Z\-]+$/', $numerical_homepage_query[0])) {
	//if there is an issue unset the original array with associative keys
	$homepage_query = null;
	}
}
?>
<div id="turfjobs_listings" class="clearfix"><!-- this ends in shortcode outputjobs line 198 -->
	<div class="col col_span_3_10">
		
		<?php //check for zero listings
		if (get_job_listing_categories_hide_empty()) : ?>

			<h3 id="filter_jobs_heading">Filter Jobs By</h3>
			
			<form class="job_filters">

			<?php if ( $categories ) : ?>
				<?php foreach ( $categories as $category ) : ?>
					<input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( $category ); ?>" />
				<?php endforeach; ?>
			<?php elseif ( $show_categories && get_option( 'job_manager_enable_categories' ) && ! is_tax( 'job_listing_category' ) ) : ?>
				<?php //MALINKY__ADDED ?>
				<h4>Job Title</h4>
				<ul class="job_types list_no_style">
					<?php foreach ( get_job_listing_categories_hide_empty() as $category ) : ?>
						<li><label for="job_category_<?php echo $category->slug; ?>" class="<?php echo sanitize_title( $category->name ); ?>"><input type="checkbox" name="filter_job_category[]" value="<?php echo $category->term_id; ?>" id="job_category_<?php echo $category->slug; ?>" <?php if (isset($homepage_query['job-title'])) { echo $category->term_id == $homepage_query['job-title'] ? 'checked' : ''; } ?> /> <?php echo $category->name; ?></label></li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>

			<?php //MALINKY__ADDED ?>
			<?php if ( ! is_tax( 'job_listing_type' ) && empty( $job_locations ) ) : ?>
				<h4>Job Location</h4>
				<ul class="job_types list_no_style">
					<?php foreach ( get_job_listing_locations_hide_empty() as $location ) : ?>
						<li><label for="job_location_<?php echo $location->slug; ?>" class="<?php echo sanitize_title( $location->name ); ?>"><input type="checkbox" name="filter_job_location[]" value="<?php echo $location->slug; ?>" id="job_location_<?php echo $location->slug; ?>" <?php if (isset($homepage_query['location'])) { echo $location->slug == $homepage_query['location'] ? 'checked' : ''; } ?> /> <?php echo $location->name; ?></label></li>
					<?php endforeach; ?>
				</ul>
			<?php elseif ( $job_locations ) : ?>
				<?php foreach ( $job_locations as $job_location ) : ?>
					<input type="hidden" name="filter_job_location[]" value="<?php echo sanitize_title( $job_location ); ?>" />
				<?php endforeach; ?>
			<?php endif; ?>

			<?php //MALINKY__ADDED ?>
			<?php if ( ! is_tax( 'job_listing_organisation_type' ) && empty( $job_organisation_types ) ) : ?>
				<h4>Organisation Type</h4>
				<ul class="job_types list_no_style">
					<?php foreach ( get_job_listing_organisation_types_hide_empty() as $organisation_type ) : ?>
						<li><label for="job_organisation_type_<?php echo $organisation_type->slug; ?>" class="<?php echo sanitize_title( $organisation_type->name ); ?>"><input type="checkbox" name="filter_job_organisation_type[]" value="<?php echo $organisation_type->slug; ?>" id="job_organisation_type_<?php echo $organisation_type->slug; ?>" <?php if (isset($homepage_query['organisation'])) { echo $organisation_type->slug == $homepage_query['organisation'] ? 'checked' : ''; } ?> /> <?php echo $organisation_type->name; ?></label></li>
					<?php endforeach; ?>
				</ul>
			<?php elseif ( $job_organisation_types ) : ?>
				<?php foreach ( $job_organisation_types as $job_organisation_type ) : ?>
					<input type="hidden" name="filter_job_organisation_type[]" value="<?php echo sanitize_title( $job_organisation_type ); ?>" />
				<?php endforeach; ?>
			<?php endif; ?>	

			<?php if ( ! is_tax( 'job_listing_type' ) && empty( $job_types ) ) : ?>
				<h4>Job Type</h4>
				<ul class="job_types list_no_style">
					<?php foreach ( get_job_listing_types_hide_empty() as $type ) : ?>
						<li><label for="job_type_<?php echo $type->slug; ?>" class="<?php echo sanitize_title( $type->name ); ?>"><input type="checkbox" name="filter_job_type[]" value="<?php echo $type->slug; ?>" id="job_type_<?php echo $type->slug; ?>" <?php if (isset($homepage_query['type'])) { echo $type->slug == $homepage_query['type'] ? 'checked' : ''; } ?> /> <?php echo $type->name; ?></label></li>
					<?php endforeach; ?>
				</ul>
			<?php elseif ( $job_types ) : ?>
				<?php foreach ( $job_types as $job_type ) : ?>
					<input type="hidden" name="filter_job_type[]" value="<?php echo sanitize_title( $job_type ); ?>" />
				<?php endforeach; ?>
			<?php endif; ?>	

			<!-- <input type="hidden" name="author" id="author" value="<?php echo $homepage_query['employer'] ?>" /> -->

			</form>

		<?php endif; //end check for zero listings ?>

		<div class="sidebar_block <?php if (!get_job_listing_categories_hide_empty()) : echo 'no_job_listings_sidebar'; endif; ?>">
			<h3 class="content">National Apprenticeships</h3>
			<p><a href="<?php echo esc_url( site_url( 'national-apprenticeships' ) ); ?>">Click here</a> for the latest sports turf and grounds maintenance apprenticeships.</p>
			<a href="<?php echo esc_url( site_url( 'national-apprenticeships' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/apprenticeships_logo.png" class="filters_apprenticeships_logo" alt="Apprenticeships_logo" /></a>
		</div>

		<?php if (!is_user_logged_in()) : ?>
			<div class="sidebar_block clearfix">
				<h3 class="content">Are you an employer?</h3>
				<p>Looking to recruit someone who works with sports or amenity turf?</p>
				<a href="<?php echo esc_url( site_url( 'register' ) ); ?>" class="button">Register</a>
			</div>
		<?php endif; ?>
	
		<div class="sidebar_block clearfix">
			<!-- Begin MailChimp Signup Form -->
			<div id="mc_embed_signup">
			<form action="http://turfjobs.us3.list-manage.com/subscribe/post?u=3a66ad38e4d82885c793b0b8f&amp;id=5092dc8a06" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
				<h2>Receive our free jobs e-bulletin</h2>
			<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
			<div class="mc-field-group">
				<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
			</div>
				<div id="mce-responses" class="clear">
					<div class="response" id="mce-error-response" style="display:none"></div>
					<div class="response" id="mce-success-response" style="display:none"></div>
				</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
			    <div style="position: absolute; left: -5000px;"><input type="text" name="b_3a66ad38e4d82885c793b0b8f_5092dc8a06" value=""></div>
				<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
			</form>
			</div>
			<!--End mc_embed_signup-->
		</div>

	</div><!-- end .col col_span_3_10 -->