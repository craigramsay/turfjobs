	<p><?php _e( 'Click on the icons in the Action section to update your advertised jobs.', 'job_manager' ); ?></p>
	<table class="employer_dashboard">
		<thead>
			<tr>
				<th class="job_title"><?php _e( 'Job Title', 'job_manager' ); ?></th>
				<th class="date"><?php _e( 'Date Posted', 'job_manager' ); ?></th>
				<th class="expires"><?php _e( 'Expires', 'job_manager' ); ?></th>
				<th class="status"><?php _e( 'Status', 'job_manager' ); ?></th>
				<th class="filled"><?php _e( 'Filled', 'job_manager' ); ?></th>
				<th class="actions"><?php _e( 'Actions', 'job_manager' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php if ( ! $jobs ) : ?>
				<tr>
					<td colspan="6"><?php _e( 'You do not have any active job listings.', 'job_manager' ); ?></td>
				</tr>
			<?php else : ?>
				<?php foreach ( $jobs as $job ) : ?>
					<tr>
						<td class="job_title">
							<?php if ($job->post_status != 'pending_payment' && $job->post_status != 'expired') { ?>
								<a href="<?php echo get_permalink( $job->ID ); ?>"><?php echo $job->post_title; ?></a>	
							<?php } else {
								echo $job->post_title;
							} ?>
						</td>
						<td class="date"><?php echo date('d/m/Y', strtotime($job->post_date)); ?></td>
						<td class="expires"><?php
							$expires = $job->_job_expires;

							echo $expires ? date('d/m/Y', strtotime($expires)) : '&ndash;';
						?></td>
						<td class="status"><?php the_job_status( $job ); ?></td>
						<td class="filled"><?php if ( is_position_filled( $job ) ) echo '&#10004;'; else echo '&ndash;'; ?></td>
						<td class="actions">
							<?php
							$actions = array();

							switch ( $job->post_status ) {
								case 'publish' :
									$actions['edit'] = array( 'label' => __( 'Edit', 'job_manager' ), 'nonce' => false );

									if ( is_position_filled( $job ) )
										$actions['mark_not_filled'] = array( 'label' => __( 'Mark Not Filled', 'job_manager' ), 'nonce' => true );
									else
										$actions['mark_filled'] = array( 'label' => __( 'Mark Filled', 'job_manager' ), 'nonce' => true );

									break;
								//MALINK__ADDED
								case 'expired' :
									$actions['relist'] = array( 'label' => __( 'Relist', 'job_manager' ), 'nonce' => false, 'different_uri' => 'employer/add-job');
									break;											
							}

							$actions['delete'] = array( 'label' => __( 'Delete', 'job_manager' ), 'nonce' => true );
							$actions           = apply_filters( 'job_manager_my_job_actions', $actions, $job );

							//MALINK__ADDED
							foreach ( $actions as $action => $value ) {
								$action_url = add_query_arg( array( 'action' => $action, 'job_id' => $job->ID ), $value['different_uri'] );
								if ( $value['nonce'] )
									$action_url = wp_nonce_url( $action_url, 'job_manager_my_job_actions' );
								$button = $action == 'delete' ? ' button red' : ' button';
								echo '<a href="' . $action_url . '" class="job-dashboard-action-' . $action . $button . '">' . $value['label'] . '</a>';
							}
							?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<?php get_job_manager_template( 'pagination.php', array( 'max_num_pages' => $max_num_pages ) ); ?>