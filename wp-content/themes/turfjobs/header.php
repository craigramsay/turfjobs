<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <!-- Use latest IE engine --> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile viewport -->    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icons for mobile for touch home screens and bookmarks. Remember to upload favicon.ico too! -->

    <!-- Chrome and Android (192 x 192) -->
    <meta name="application-name" content="<?php echo esc_attr( bloginfo( 'name' ) ); ?>">
    <link rel="icon" sizes="192x192" href="<?php echo esc_url( site_url( 'chrome-touch-icon-192x192.png' ) ); ?>">

    <!-- Safari on iOS (152 x 152) -->
    <meta name="apple-mobile-web-app-title" content="<?php echo esc_attr( bloginfo( 'name' ) ); ?>">
    <link rel="apple-touch-icon" href="<?php echo esc_url( site_url( 'apple-touch-icon.png' ) ); ?>">
    <meta name="format-detection" content="telephone=no">

    <!-- IE on Win8 (144 x 144 + tile color) -->
    <meta name="msapplication-TileImage" content="<?php echo esc_url( site_url( 'ms-touch-icon-144x144-precomposed.png' ) ); ?>">
    <meta name="msapplication-TileColor" content="#00793a">

    <!-- Google Fonts -->
    
    <!-- Meta -->
    <?php global $post;
    if ($post->post_type == 'job_listing') {
        $meta_desc = $post->post_content; ?>
        <title><?php the_title(); echo ' Job | '; the_job_location(); echo ' | '; the_job_type(); echo ' | '; the_job_organisation_type(); ?></title>
        <meta name="description" content="<?php echo strip_tags((preg_replace('/\s+?(\S+)?$/', '', substr($meta_desc, 0, 165)))) . '... '; ?>" />
    <?php } else { ?>
        <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php } ?>

    <?php if ( WP_ENV != 'local' ) { ?>

        <style type="text/css">
            <?php
            //Load critical path CSS, replacing img/ with absolute urls.
            $critical_css = file_get_contents(get_template_directory() . '/style-critical.css');
            $critical_css = str_replace('url(img/', 'url(' . get_template_directory_uri() . '/img/', $critical_css);
            echo $critical_css;
            ?>
        </style>

        <script>
        /*!
        loadCSS: load a CSS file asynchronously.
        [c]2014 @scottjehl, Filament Group, Inc.
        Licensed MIT
        */
        function loadCSS( href, before, media, callback ){
            "use strict";
            // Arguments explained:
            // `href` is the URL for your CSS file.
            // `before` optionally defines the element we'll use as a reference for injecting our <link>
            // By default, `before` uses the first <script> element in the page.
            // However, since the order in which stylesheets are referenced matters, you might need a more specific location in your document.
            // If so, pass a different reference element to the `before` argument and it'll insert before that instead
            // note: `insertBefore` is used instead of `appendChild`, for safety re: http://www.paulirish.com/2011/surefire-dom-element-insertion/
            var ss = window.document.createElement( "link" );
            var ref = before || window.document.getElementsByTagName( "script" )[ 0 ];
            var sheets = window.document.styleSheets;
            ss.rel = "stylesheet";
            ss.href = href;
            // temporarily, set media to something non-matching to ensure it'll fetch without blocking render
            ss.media = "only x";
            // DEPRECATED
            if( callback ) {
                ss.onload = callback;
            }

            // inject link
            ref.parentNode.insertBefore( ss, ref );
            // This function sets the link's media back to `all` so that the stylesheet applies once it loads
            // It is designed to poll until document.styleSheets includes the new sheet.
            ss.onloadcssdefined = function( cb ){
                var defined;
                for( var i = 0; i < sheets.length; i++ ){
                    if( sheets[ i ].href && sheets[ i ].href.indexOf( href ) > -1 ){
                        defined = true;
                    }
                }
                if( defined ){
                    cb();
                } else {
                    setTimeout(function() {
                        ss.onloadcssdefined( cb );
                    });
                }
            };
            ss.onloadcssdefined(function() {
                ss.media = media || "all";
            });
            return ss;
        }
        loadCSS('<?php echo get_template_directory_uri(); ?>/style.css', window.document.getElementsByTagName( 'script' )[ 1 ]);
        </script>
        <noscript><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css" media="all" /></noscript>

    <?php } ?>

    <?php if ( WP_ENV == 'dev' ) { ?>
        <meta name="robots" content="noindex,nofollow">
    <?php } ?>
    
    <?php wp_head(); ?>

    <!--https://github.com/scottjehl/Respond-->
    <!--https://github.com/aFarkas/html5shiv-->
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/respond.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
    <![endif]-->

</head>

<body <?php body_class(); ?>>
	<!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->

<?php
global $current_user;
get_currentuserinfo();
?>

<div class="wrap">
    <div class="wrap_inner">
        <div id="login_bar" class="clearfix">
            <div class="col col_span_10_10">
                <?php if (!is_user_logged_in()) : ?>
                    <p><span>Employer </span><a href="<?php echo esc_url( wp_login_url() ); ?>">Login</a> / <a href="<?php echo esc_url( site_url( 'register' ) ); ?>">Register</a></p>
                <?php elseif (is_user_logged_in()) : ?>
                    <p id="logged_in_as">Logged in as <span><?php echo $current_user->user_login; ?></span></p>
                    <p><a href="<?php echo esc_url( site_url( 'employer' ) ); ?>">My Account</a></p> / 
                    <p><a href="<?php echo esc_url( wp_logout_url( home_url() ) ); ?>">Logout</a></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap">  

    <div id="nav" class="clearfix">
        <div class="wrap_inner">
            <div class="col col_span_2_10">
                <a href="<?php echo esc_url( site_url() ); ?>" id="logo"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" width="180px" height="78px" alt="Turf Jobs Logo" class="non_responsive" /></a>
            </div>
            <div class="col col_span_8_10">
                <?php
                    $args = array(
                        'menu'              => 'Front End',
                        'container_id'      => 'menu-employer-container',
                        'container_class'   => 'clearfix'
                    );
                    wp_nav_menu($args);
                ?> 
            
                <div id="social_media" class="clearfix">
                    <a href="<?php echo esc_url( 'https://twitter.com/TurfJobsUK' ); ?>" target="_blank" class="desk-twitter"><span class="fa fa-twitter"></span></a>
                    <a href="<?php echo esc_url( 'http://www.facebook.com/turfjobs' ); ?>" target="_blank" class="desk-facebook"><span class="fa fa-facebook"></span></a>
                    <a href="<?php echo esc_url( 'https://uk.linkedin.com/groups/UK-Turf-Industry-3395568/about' ); ?>" target="_blank" class="desk-linked"><span class="fa fa-linkedin"></span></a>
                    <a href="<?php echo esc_url( site_url('feed-job-listings') ); ?>" target="_blank" class="desk-rss"><span class="fa fa-rss"></span></a>
                </div>
            </div> 
        </div><!--END .wrap_inner-->          
    </div> 

    <div id="login_bar_mobile" class="clearfix">
        <div class="col col_span_10_10">
            <?php if (!is_user_logged_in()) : ?>
                <p><span>Employer </span><a href="<?php echo esc_url( wp_login_url() ); ?>">Login</a> / <a href="<?php echo esc_url( site_url( 'register' ) ); ?>">Register</a></p>
            <?php elseif (is_user_logged_in()) : ?>
                <p><a href="<?php echo esc_url( site_url( 'employer' ) ); ?>">My Account</a></p> / 
                <p><a href="<?php echo esc_url( wp_logout_url( home_url() ) ); ?>">Logout</a></p>
            <?php endif; ?>
        </div>
    </div>

    <div id="nav_mobile" class="clearfix">
        <div class="col col_span_6_10" id="nav_mobile_menu"><a href=""><span class="fa fa-navicon"></span></a></div>
        <div class="col col_span_1_10 nav_mobile_icon"><a href="<?php echo esc_url( 'https://twitter.com/TurfJobsUK' ); ?>" target="_blank"><span class="fa fa-twitter"></span></a></div>
        <div class="col col_span_1_10 nav_mobile_icon"><a href="<?php echo esc_url( 'http://www.facebook.com/turfjobs' ); ?>" target="_blank"><span class="fa fa-facebook"></span></a></div>
        <div class="col col_span_1_10 nav_mobile_icon"><a href="<?php echo esc_url( 'https://uk.linkedin.com/groups/UK-Turf-Industry-3395568/about' ); ?>" target="_blank"><span class="fa fa-linkedin"></span></a></div>
        <div class="col col_span_1_10 nav_mobile_icon"><a href="<?php echo esc_url( site_url('feed-job-listings') ); ?>" target="_blank"><span class="fa fa-rss"></span></a></div>                        
        <?php
		    $args = array(
		        'menu'              => 'Front End',
                'container'         => false,
                'menu_id'           => 'nav_mobile_toggle',
                'menu_class'        => 'list_no_style'
		    );
		    wp_nav_menu($args);
		?>                        
    </div>            
    <div id="nav_mobile_logo" class="clearfix">
        <div class="col col_span_10_10">
            <a href="<?php echo esc_url( site_url() ); ?>" id="logo"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" width="180px" height="78px" alt="Turf Jobs Logo" class="non_responsive" /></a>
        </div>
    </div>

</div><!--END .wrap-->

<div class="wrap">
    <div class="wrap_inner">