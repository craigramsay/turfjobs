<?php
/**
 * Twenty Twelve functions and definitions
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook.
 *
 * For more information on hooks, actions, and filters, @link http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

/**
 * Twenty Twelve setup.
 *
 * Sets up theme defaults and registers the various WordPress features that
 * Twenty Twelve supports.
 *
 * @uses load_theme_textdomain() For translation/localization support.
 * @uses add_editor_style() To add a Visual Editor stylesheet.
 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
 * 	custom background, and post formats.
 * @uses register_nav_menu() To add support for navigation menus.
 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_setup() {
	/*
	 * Makes Twenty Twelve available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Twenty Twelve, use a find and replace
	 * to change 'twentytwelve' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'twentytwelve', get_template_directory() . '/languages' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// This theme supports a variety of post formats.
	add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'front-end', 'Front End Menu' );
	register_nav_menu( 'employer', 'Employer Menu' );
	register_nav_menu( 'administrator', 'Administrator Menu' );

	/*
	 * This theme supports custom background color and image,
	 * and here we also set up the default background color.
	 */
	add_theme_support( 'custom-background', array(
		'default-color' => 'e6e6e6',
	) );

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 624, 9999 ); // Unlimited height, soft crop
}
add_action( 'after_setup_theme', 'twentytwelve_setup' );

/**
 * Add support for a custom header image.
 */
require( get_template_directory() . '/inc/custom-header.php' );

/**
 * Enqueue scripts and styles for front-end.
 *
 * @since Twenty Twelve 1.0
 *
 * @return void
 */
function twentytwelve_scripts_styles() {
	global $wp_styles;

	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	//if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
	//	wp_enqueue_script( 'comment-reply' );

	// Adds JavaScript for handling the navigation menu hide-and-show behavior.
	//wp_enqueue_script( 'twentytwelve-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0', true );

	// Loads the Internet Explorer specific stylesheet.
	//wp_enqueue_style( 'twentytwelve-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentytwelve-style' ), '20121010' );
	//$wp_styles->add_data( 'twentytwelve-ie', 'conditional', 'lt IE 9' );
}
add_action( 'wp_enqueue_scripts', 'twentytwelve_scripts_styles' );

/**
 * Filter the page menu arguments.
 *
 * Makes our wp_nav_menu() fallback -- wp_page_menu() -- show a home link.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'twentytwelve_page_menu_args' );

/**
 * Register sidebars.
 *
 * Registers our main widget area and the front page widget areas.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Side Bar One', 'twentytwelve' ),
		'id' => 'sidebar-1',
		'description' => 'Appears in the right side bar',
		'before_widget' => '',
		'after_widget' => '',		
		'before_title' => '<h3 class="content">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Side Bar Two', 'twentytwelve' ),
		'id' => 'sidebar-2',
		'description' => 'Appears in the right side bar',
		'before_widget' => '',
		'after_widget' => '',		
		'before_title' => '<h3 class="content">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => 'Email Subscribe',
		'id' => 'email-subscribe',
		'description' => 'Appears where we need email subscribers',
		'before_widget' => '',
		'after_widget' => '',		
		'before_title' => '<h3 class="content">',
		'after_title' => '</h3>',
	) );

}
add_action( 'widgets_init', 'twentytwelve_widgets_init' );

if ( ! function_exists( 'twentytwelve_content_nav' ) ) :
/**
 * Displays navigation to next/previous pages when applicable.
 *
 * @since Twenty Twelve 1.0
 */
function twentytwelve_content_nav( $html_id ) {
	global $wp_query;

	$html_id = esc_attr( $html_id );

	if ( $wp_query->max_num_pages > 1 ) : ?>
		<nav id="<?php echo $html_id; ?>" class="navigation" role="navigation">
			<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentytwelve' ); ?></h3>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentytwelve' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentytwelve' ) ); ?></div>
		</nav><!-- #<?php echo $html_id; ?> .navigation -->
	<?php endif;
}
endif;

if ( ! function_exists( 'twentytwelve_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own twentytwelve_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Twenty Twelve 1.0
 *
 * @return void
 */
function twentytwelve_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php _e( 'Pingback:', 'twentytwelve' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<header class="comment-meta comment-author vcard">
				<?php
					echo get_avatar( $comment, 44 );
					printf( '<cite><b class="fn">%1$s</b> %2$s</cite>',
						get_comment_author_link(),
						// If current post author is also comment author, make it known visually.
						( $comment->user_id === $post->post_author ) ? '<span>' . __( 'Post author', 'twentytwelve' ) . '</span>' : ''
					);
					printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
						esc_url( get_comment_link( $comment->comment_ID ) ),
						get_comment_time( 'c' ),
						/* translators: 1: date, 2: time */
						sprintf( __( '%1$s at %2$s', 'twentytwelve' ), get_comment_date(), get_comment_time() )
					);
				?>
			</header><!-- .comment-meta -->

			<?php if ( '0' == $comment->comment_approved ) : ?>
				<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentytwelve' ); ?></p>
			<?php endif; ?>

			<section class="comment-content comment">
				<?php comment_text(); ?>
				<?php edit_comment_link( __( 'Edit', 'twentytwelve' ), '<p class="edit-link">', '</p>' ); ?>
			</section><!-- .comment-content -->

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'twentytwelve' ), 'after' => ' <span>&darr;</span>', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // end comment_type check
}
endif;

if ( ! function_exists( 'twentytwelve_entry_meta' ) ) :
/**
 * Set up post entry meta.
 *
 * Prints HTML with meta information for current post: categories, tags, permalink, author, and date.
 *
 * Create your own twentytwelve_entry_meta() to override in a child theme.
 *
 * @since Twenty Twelve 1.0
 *
 * @return void
 */
function twentytwelve_entry_meta() {
	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'twentytwelve' ) );

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'twentytwelve' ) );

	$date = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>',
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() )
	);

	$author = sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'View all posts by %s', 'twentytwelve' ), get_the_author() ) ),
		get_the_author()
	);

	// Translators: 1 is category, 2 is tag, 3 is the date and 4 is the author's name.
	if ( $tag_list ) {
		$utility_text = __( 'This entry was posted in %1$s and tagged %2$s on %3$s<span class="by-author"> by %4$s</span>.', 'twentytwelve' );
	} elseif ( $categories_list ) {
		$utility_text = __( 'This entry was posted in %1$s on %3$s<span class="by-author"> by %4$s</span>.', 'twentytwelve' );
	} else {
		$utility_text = __( 'This entry was posted on %3$s<span class="by-author"> by %4$s</span>.', 'twentytwelve' );
	}

	printf(
		$utility_text,
		$categories_list,
		$tag_list,
		$date,
		$author
	);
}
endif;

/**
 * Extend the default WordPress body classes.
 *
 * Extends the default WordPress body class to denote:
 * 1. Using a full-width layout, when no active widgets in the sidebar
 *    or full-width template.
 * 2. Front Page template: thumbnail in use and number of sidebars for
 *    widget areas.
 * 3. White or empty background color to change the layout and spacing.
 * 4. Custom fonts enabled.
 * 5. Single or multiple authors.
 *
 * @since Twenty Twelve 1.0
 *
 * @param array $classes Existing class values.
 * @return array Filtered class values.
 */
function twentytwelve_body_class( $classes ) {
	$background_color = get_background_color();
	$background_image = get_background_image();

	if ( ! is_active_sidebar( 'sidebar-1' ) || is_page_template( 'page-templates/full-width.php' ) )
		$classes[] = 'full-width';

	if ( is_page_template( 'page-templates/front-page.php' ) ) {
		$classes[] = 'template-front-page';
		if ( has_post_thumbnail() )
			$classes[] = 'has-post-thumbnail';
		if ( is_active_sidebar( 'sidebar-2' ) && is_active_sidebar( 'sidebar-3' ) )
			$classes[] = 'two-sidebars';
	}

	if ( empty( $background_image ) ) {
		if ( empty( $background_color ) )
			$classes[] = 'custom-background-empty';
		elseif ( in_array( $background_color, array( 'fff', 'ffffff' ) ) )
			$classes[] = 'custom-background-white';
	}

	// Enable custom font class only if the font CSS is queued to load.
	if ( wp_style_is( 'twentytwelve-fonts', 'queue' ) )
		$classes[] = 'custom-font-enabled';

	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	return $classes;
}
add_filter( 'body_class', 'twentytwelve_body_class' );

/**
 * Adjust content width in certain contexts.
 *
 * Adjusts content_width value for full-width and single image attachment
 * templates, and when there are no active widgets in the sidebar.
 *
 * @since Twenty Twelve 1.0
 *
 * @return void
 */
function twentytwelve_content_width() {
	if ( is_page_template( 'page-templates/full-width.php' ) || is_attachment() || ! is_active_sidebar( 'sidebar-1' ) ) {
		global $content_width;
		$content_width = 960;
	}
}
add_action( 'template_redirect', 'twentytwelve_content_width' );

/**
 * Register postMessage support.
 *
 * Add postMessage support for site title and description for the Customizer.
 *
 * @since Twenty Twelve 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 * @return void
 */
function twentytwelve_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'twentytwelve_customize_register' );

/**
 * Enqueue Javascript postMessage handlers for the Customizer.
 *
 * Binds JS handlers to make the Customizer preview reload changes asynchronously.
 *
 * @since Twenty Twelve 1.0
 *
 * @return void
 */
function twentytwelve_customize_preview_js() {
	wp_enqueue_script( 'twentytwelve-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20130301', true );
}
add_action( 'customize_preview_init', 'twentytwelve_customize_preview_js' );





/* ------------------------------------------------------------------------ *
 * Front End Scripts
 * ------------------------------------------------------------------------ */

/**
 * Enqueue frontend scripts.
 */
function malinky_scripts()
{

	/* -------------------------------- *
	 * Local && Dev && Prod
	 * -------------------------------- */

	/**
	 * Load WP jQuery and jQuery migrate in the footer.
	 */
	if ( ! is_admin() && tree() != 'employer' ) {

		wp_deregister_script( 'jquery' );
		wp_deregister_script( 'jquery-migrate' );

		wp_register_script( 'jquery',
							'/wp-includes/js/jquery/jquery.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'jquery' );

		wp_register_script( 'jquery-migrate',
							'/wp-includes/js/jquery/jquery-migrate.min.js',
							false,
							NULL,
							true
		);
		wp_enqueue_script( 'jquery-migrate' );

	}


	if ( WP_ENV == 'local' ) {

		/* -------------------------------- *
		 * Local
		 * -------------------------------- */

		/**
		 * Stylesheet which includes normalize.
		 */
		wp_enqueue_style( 'turfjobs-style', get_stylesheet_uri() );


		/**
		 * Font awesome font.
		 *
		 * @link http://fortawesome.github.io/Font-Awesome/
		 */		
		wp_register_style( 'turfjobs-font-awesome', 
						   '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css', 
						   false, 
						   NULL
		);
		wp_enqueue_style( 'turfjobs-font-awesome' );


		/**
		 * Flexisel carousel
		 */
		wp_register_script( 'turfjobs-flexisel-js', 
							get_template_directory_uri() . '/js/jquery.flexisel.min.js', 
							array( 'jquery' ), 
							NULL, 
							true 
		);
		wp_enqueue_script( 'turfjobs-flexisel-js' );


		/**
		 * Turfjobs related javascript and jQuery.
		 */
		wp_register_script( 'turfjobs-main-js',
							get_template_directory_uri() . '/js/main.js',
							array( 'jquery' ),
							NULL,
							true
		);
		wp_enqueue_script( 'turfjobs-main-js' );
	
	}


	if ( WP_ENV == 'dev' || WP_ENV == 'prod' ) {

		/* -------------------------------- *
		 * Dev && Prod
		 * -------------------------------- */

		/**
		 * Enqueue normal css for employer section rather than critical.
		 */
		if ( tree() == 'employer' ) {
			/**
			 * Stylesheet which includes normalize.
			 */
			wp_enqueue_style( 'turfjobs-style', get_stylesheet_uri() );
		}

		/*
		 * jquery.flexisel.min.js, main.js
		 */
		wp_register_script( 'malinky-scripts-min-js',
							get_template_directory_uri() . '/js/scripts.min.js',
							array( 'jquery' ),
							NULL,
							true
		);
		wp_enqueue_script( 'malinky-scripts-min-js' );

	}

}

add_action( 'wp_enqueue_scripts', 'malinky_scripts' );

/*add_action('init', 'jobsRSS');
function jobsRSS()
{
    add_feed('job-listings', 'customjobsRSS');
}

function customjobsRSS()
{
    get_template_part('rss', 'job-listings');
}*/

//used to climb the tree to the parent page and return the slug
function tree()
{
	$parent_post_name = '';
  	if( is_page() ) {
  		global $post;
      	/* Get an array of Ancestors and Parents if they exist */
  		$parents = get_post_ancestors( $post->ID );
      	/* Get the top Level page->ID count base 1, array base 0 so -1 */ 
  		$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
     	/* Get the parent and set the $parent_post_name with the page slug (post_name) */
  		$parent = get_page( $id );
  		$parent_post_name = $parent->post_name;
	}
	return $parent_post_name;
}

add_image_size( 'vistor_feedback_sidebar', 80, 9999 );
add_image_size( 'vistor_feedback_main', 180, 9999 );

add_filter('the_generator', 'turfjobs_remove_version');
function turfjobs_remove_version() {
return '';
}

//add speical class to show active menu from within a job
add_filter( 'nav_menu_css_class', 'my_special_nav_class', 10, 2 );
function my_special_nav_class( $classes, $item )
{
	global $post;
	$post_type = get_post_type( $post );
    if( $post_type == 'job_listing' && $item->title == 'Jobs' )
    {
        $classes[] = 'jobs_menu_active_class';
    }
    if ($item->title == 'Turf Jobs Home')
    {
    	$classes[] = 'employer_turf_jobs_home_class';	
    }
    return $classes;
}

/**
 * Change login screen logo url.
 *
 * @return string
 */
function malinky_login_logo_url()
{
	return esc_url ( home_url( '', 'http' ) );
}

add_filter( 'login_headerurl', 'malinky_login_logo_url' );


/**
 * Remove shake on login.
 */
function malinky_login_shake()
{
    remove_action( 'login_head', 'wp_shake_js', 12 );
}

add_action( 'login_head', 'malinky_login_shake' );


/**
 * Style login logo and page.
 */
function malinky_login_screen()
{ ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri(); ?>/img/logo.png);
            background-size: 160px;
            width: auto;
        }
        body.login form {
			border: 1px solid #e5e5e5;
			box-shadow: none;
        }
        body.login form .input {
        	background: none;
        }
        body.login h1 a {
        	background-size: auto;
        }
        html.lt-ie9 body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri(); ?>/img/logo-fixed@2x.png);
        }
        body.login .button.button-large {
        	height: auto;
        	line-height: normal;
		    padding: 10px;
        }        
        body.login .button-primary {
		    width: auto;
		    height: auto;
		    display: inline-block;
		    background: #81C175;
		    border: none;
		    border-radius: 0;
		    box-shadow: none;
            text-shadow: none;
        }
        body.login .button-primary:hover,
        body.login .button-primary:focus,
        body.login .button-primary:active {
    		background: #81C175;
    		text-shadow: none;
    		box-shadow: none;
        }        
        body.login #nav {
        	text-shadow: none;
			padding: 26px 24px;
			background: #FFFFFF;
			border: 1px solid #e5e5e5;
			box-shadow: none;
			text-align: center;
		}
		body.login #nav a {
		    width: auto;
		    height: auto;
		    display: block;
		    padding: 10px;
		    background: #81C175;
		    border: none;
		    color: #fff;
		    border-radius: none;
		}
		body.login #nav a:hover,
		body.login #nav a:focus,
		body.login #nav a:active {
			color: #fff;
			background: #81C175;
    		text-shadow: none;
    		box-shadow: none;
		}
		body.login #login #backtoblog a:hover {
			color: #999;
		}
    </style>
<?php }

add_action( 'login_enqueue_scripts', 'malinky_login_screen' );

ini_set('display_errors', '0');