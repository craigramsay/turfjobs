<?php
/**
 * Template Name: Home
 */
get_header(); ?>

	<div id="content" class="clearfix">
		<div class="col col_span_10_10">
			
			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<div id="home_banner" class="clearfix">
						<div class="col col_span_10_10">
							<h3>List your job vacancy for just £99</h3>
							<?php if ( is_user_logged_in() ) { ?>
								<a href="<?php echo esc_url( site_url( 'jobs/add-job' ) ); ?>" class="button">Add Job</a>
							<?php } else { ?>
								<a href="<?php echo esc_url( site_url( 'register' ) ); ?>" class="button">Register Now</a>
							<?php } ?>
						</div>
					</div>

					<div id="home_search" class="clearfix">
						<div class="col col_span_7_10">
							<?php 
								$job_title = get_job_listing_categories_hide_empty();
								$job_location = get_job_listing_locations_hide_empty();
								$organisation_type = get_job_listing_organisation_types_hide_empty();
								$job_type = get_job_listing_types_hide_empty();
								?>
								<ul class="home_search_tab_headings clearfix list_no_style">
									<li class="home_search_tab">Search Current Jobs</li>
								    <li><a href='#job_title'>Job Title</a></li>
								    <li><a href='#job_location'>Location</a></li>
								    <li><a href='#organisation_type'>Organisation Type</a></li>
								    <li><a href='#job_type'>Job Type</a></li>
				  				</ul>
								<ul id="job_title" class="home_search_tabs list_no_style">
									<?php foreach ($job_title as $key => $value) :
										echo '<li><a href="' . esc_url( site_url( 'jobs?job-title=' . $value->term_id ) ) . '">' . $value->name . '</a></li>';
									endforeach; ?>
									<li><a href="<?php echo esc_url( site_url( 'national-apprenticeships' ) ); ?>">All National Apprenticeships</a></li>
									<li><a href="<?php echo esc_url( site_url( 'jobs' ) ); ?>">All Job Titles</a></li>
								</ul>
								<ul id="job_location" class="home_search_tabs list_no_style">
									<?php foreach ($job_location as $key => $value) :
										echo '<li><a href="' . esc_url( site_url( 'jobs?location=' . $value->slug ) ) . '">' . $value->name . '</a></li>';
									endforeach; ?>
									<li><a href="<?php echo esc_url( site_url( 'jobs' ) ); ?>">All Locations</a></li>
								</ul>
								<ul id="organisation_type" class="home_search_tabs list_no_style">
									<?php if ($organisation_type) : foreach ($organisation_type as $key => $value) :
										echo '<li><a href="' . esc_url( site_url( 'jobs?organisation=' . $value->slug ) ) . '">' . $value->name . '</a></li>';
									endforeach; endif; ?>
									<li><a href="<?php echo esc_url( site_url( 'jobs' ) ); ?>">All Organisation Types</a></li>
								</ul>
								<ul id="job_type" class="home_search_tabs list_no_style">
									<?php foreach ($job_type as $key => $value) :
										echo '<li><a href="' . esc_url( site_url( 'jobs?type=' . $value->slug ) ) . '">' . $value->name . '</a></li>';
									endforeach; ?>
									<li><a href="<?php echo esc_url( site_url( 'jobs' ) ); ?>">All Job Types</a></li>
								</ul>	
						</div>
						<div class="col col_span_3_10">
							<h2 id="home_bullets_header"><?php esc_attr( the_field('home_right_panel_header') ); ?></h2>
							<h3 id="home_bullets_sub_header"><?php esc_attr( the_field('home_right_panel_sub_header') ); ?></h3>
							<?php
							if (get_field('home_right_panel_bullet_points')): ?>
								<ul id="home_bullets" class="list_no_style">
								<?php while(has_sub_field('home_right_panel_bullet_points')):
 									echo '<li>' . esc_attr( get_sub_field('bullet_point') ) . '</li>';
 								endwhile; ?>
 								</ul>
 							<?php endif; ?>
							<div class="sidebar_block home clearfix">
								<!-- Begin MailChimp Signup Form -->
								<div id="mc_embed_signup">
								<form action="http://turfjobs.us3.list-manage.com/subscribe/post?u=3a66ad38e4d82885c793b0b8f&amp;id=5092dc8a06" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
									<h2>Receive our free jobs e-bulletin</h2>
								<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
								<div class="mc-field-group">
									<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address">
								</div>
									<div id="mce-responses" class="clear">
										<div class="response" id="mce-error-response" style="display:none"></div>
										<div class="response" id="mce-success-response" style="display:none"></div>
									</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								    <div style="position: absolute; left: -5000px;"><input type="text" name="b_3a66ad38e4d82885c793b0b8f_5092dc8a06" value=""></div>
									<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
								</form>
								</div>
								<!--End mc_embed_signup-->
							</div>
						</div>
					</div>

					<div id="home_content" class="clearfix">
						<div class="col col_span_4_10">
							<h3 class="content"><?php the_field('home_employee_header'); ?></h3>
							<?php the_field('home_employee_content'); ?>
							<a href="<?php echo esc_url( site_url( 'jobs' ) ); ?>"><?php esc_attr( the_field('home_employee_anchor_text') ); ?></a>
						</div>
						<div class="col col_span_4_10">
							<h3 class="content"><?php the_field('home_employer_header'); ?></h3>
							<?php the_field('home_employer_content'); ?>
							<?php if (!is_user_logged_in())	: ?>						
								<a href="<?php echo esc_url( site_url( 'register' ) ); ?>"><?php esc_attr( the_field('home_employer_anchor_text') ); ?></a>
							<?php else : ?>
								<a href="<?php echo esc_url( site_url( 'employer/add-job' ) ); ?>">Add Job</a>
							<?php endif; ?>
						</div>
						<div class="col col_span_2_10">
							<h3 class="content"><?php esc_attr( the_field('home_charity_header') ); ?></h3>
							<p><?php esc_attr( the_field('home_charity_text') ); ?></p>
							<img src="<?php esc_url( the_field('home_charity_logo') ); ?>" data-at2x=<?php echo esc_url( str_replace( '.' . pathinfo(get_field('home_charity_logo'), PATHINFO_EXTENSION), '@2x.' . pathinfo(get_field('home_charity_logo'), PATHINFO_EXTENSION), get_field('home_charity_logo') ) ); ?> alt="<?php the_field('home_charity_header'); ?>" />
						</div>
					</div>

					<?php
					$args = array(
						'post_type' 	=> 'job_listing',
						'post_status'	=> 'publish',
						'orderby'       => 'rand',
						'meta_query'	=> array(
							array(
								'key'     => '_filled',
								'value'   => '1',
								'compare' => '!='
								),
							array(
								'key'     => '_job_company_hide_details',
								'value'   => '1',
								'compare' => '!='
								)
							)						
					);
					$carousel = new WP_Query( $args );		
					if ($carousel->post_count >= 2) :
						if ( $carousel->have_posts() ) : ?>
						<div id="home_carousel" class="clearfix">
							<div class="col col_span_10_10">
								<h3 class="content">Current listed employers</h3>
								<ul id="home_carousel_load">
								<?php while ( $carousel->have_posts() ) : $carousel->the_post(); ?>
									<li><a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url( the_company_logo($post->post_author) ); ?>" alt="<?php echo esc_attr( $post->post_title ); ?>" /></a></li>
								<?php endwhile; ?>
									<li><a href="<?php echo esc_url( site_url( 'national-apprenticeships' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/apprenticeships_logo.png" alt="Apprenticeships_logo" /></a></li>
								</ul>
							</div>
						</div>				
						<?php endif; ?>
					<?php endif; ?>	

					<div id="home_award" class="clearfix">
						<div class="col col_span_10_10">
							<h3 class="content">Golf Industry Magazine Award</h3>
							<p>We're pleased to announced that Ellie Parry received a social media award for TurfJobs from Golf Course Industry magazine in the United States. The award for ‘Best use of Twitter’ was presented at the Golf Industry Show in Orlando, Florida.  <a href="<?php echo esc_url(site_url('news/turfjobs-award-winning-twitter-feed')); ?>">Read More</a>
						</div>
					</div>

				</article><!-- #post -->
			<?php endwhile; // end of the loop. ?>
			
		</div>
	</div>

<?php get_footer(); ?>