		<div id="footer" class="clearfix">
			<div class="col col_span_3_10">
				<p class="bold_text">&copy; <?php echo date('Y'); ?> Turf Links Ltd</p>
			</div>
			<div class="col col_span_7_10">
				<a href="<?php echo esc_url( site_url( 'terms-of-use' ) ); ?>">Terms of Use</a>
				<a href="<?php echo esc_url( site_url( 'privacy-policy' ) ); ?>">Privacy Policy</a>
				<a href="<?php echo esc_url( site_url( 'cookies-policy' ) ); ?>">Cookies Policy</a>
			</div>
		</div>
		<div id="footer_bottom" class="clearfix">
			<div class="col col_span_10_10">
				<p>TurfJobs is a trading name of Turf Links Ltd, a company registered in England and Wales. Company number: 8769806.</p>
				<p>Turf Links Ltd manages data responsibly and is registered with the Information Commissioner’s Office. No: A1032685</p>
				<p>Registered office address: 1 Princes Court, Royal Way, Loughborough, LE11 5XR, UK</p>
			</div>
		</div>
	</div><!-- .wrap_inner -->
</div><!-- .wrap -->

<?php if ( WP_ENV == 'prod' ) { ?>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-46674371-1','auto');ga('send','pageview');
</script>

<?php } ?>

<script type="text/javascript">
    (function() {

        var tabHeadings = document.querySelectorAll('ul.home_search_tab_headings');

        var     active, 
                content, 
                links = document.querySelectorAll('ul.home_search_tab_headings a');

        for (var i = 0; i < links.length; i++) {
            isActive = [links[i]].filter(hashTest);
            if (isActive.length) {
                active = links[i];
                links[i].classList.add('active');
            } else {
                active = links[0];
                links[0].classList.add('active');
            }
            
            content = document.querySelector(active.hash);

            if (!links[i].classList.contains('active')) {
                document.querySelector(links[i].hash).style.display = 'none';
            }
            links[i].addEventListener('click', activeTab);  
        }

        function hashTest(value) {
            return value.hash == location.hash;
        }

        function activeTab(event) {
            //Hide Old
            for (var i = 0; i < links.length; i++) {
                links[i].classList.remove('active');
                document.querySelector(links[i].hash).style.display = 'none';
            }
            //Show New
            this.classList.add('active');
            document.querySelector(this.hash).style.display = 'block';
            //Prevent Default
            event.preventDefault();
        }

    })()
</script>
                    
<?php wp_footer(); ?>

<?php if ( WP_ENV == 'local' ) { ?>

<!-- Live Reload -->
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

<?php } ?>

</body>
</html>