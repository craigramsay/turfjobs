<?php
/**
 * Template Name: Default Template With Sidebar
 */
get_header(); ?>

<div id="content" class="clearfix">

	<? if (!is_page('jobs')) { ?>
		<div id="home_banner" class="clearfix">
			<div class="col col_span_10_10">
				<h3>List your job vacancy for just £99</h3>
				<?php if ( is_user_logged_in() ) { ?>
					<a href="<?php echo esc_url( site_url( 'employer/add-job' ) ); ?>" class="button">Add Job</a>
				<?php } else { ?>
					<a href="<?php echo esc_url( site_url( 'register' ) ); ?>" class="button">Register Now</a>
				<?php } ?>
			</div>
		</div>
	<?php } ?>

	<div id="content_with_sidebar" class="clearfix">

		<div class="col col_span_7_10">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; // end of the loop. ?>
			
		</div>

		<div class="col col_span_3_10">
			<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
				<div class="sidebar_block first clearfix">
					<?php dynamic_sidebar( 'sidebar-1' ); ?>
				</div>
			<?php endif; ?>
			<?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
				<div class="sidebar_block clearfix">
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</div>
			<?php endif; ?>
		</div>

	</div>	

</div>

<?php get_footer(); ?>