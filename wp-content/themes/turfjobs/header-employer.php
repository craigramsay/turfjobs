<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">

    <!-- Use latest IE engine --> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile viewport -->    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Icons for mobile for touch home screens and bookmarks. Remember to upload favicon.ico too! -->

    <!-- Chrome and Android (192 x 192) -->
    <meta name="application-name" content="<?php echo esc_attr( bloginfo( 'name' ) ); ?>">
    <link rel="icon" sizes="192x192" href="<?php echo esc_url( site_url( 'chrome-touch-icon-192x192.png' ) ); ?>">

    <!-- Safari on iOS (152 x 152) -->
    <meta name="apple-mobile-web-app-title" content="<?php echo esc_attr( bloginfo( 'name' ) ); ?>">
    <link rel="apple-touch-icon" href="<?php echo esc_url( site_url( 'apple-touch-icon.png' ) ); ?>">
    <meta name="format-detection" content="telephone=no">

    <!-- IE on Win8 (144 x 144 + tile color) -->
    <meta name="msapplication-TileImage" content="<?php echo esc_url( site_url( 'ms-touch-icon-144x144-precomposed.png' ) ); ?>">
    <meta name="msapplication-TileColor" content="#00793a">

    <!-- Google Fonts -->
    
    <!-- Meta -->
    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <?php if ( WP_ENV == 'dev' ) { ?>
        <meta name="robots" content="noindex,nofollow">
    <?php } ?>
    
    <?php wp_head(); ?>

    <!--https://github.com/scottjehl/Respond-->
    <!--https://github.com/aFarkas/html5shiv-->
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/respond.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
    <![endif]-->

</head>

<body <?php body_class(); ?>>
	<!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->

<?php
global $current_user;
get_currentuserinfo();
?>

<div class="wrap">
    <div class="wrap_inner">
        <div id="login_bar" class="clearfix">
            <div class="col col_span_10_10">
                <?php if (!is_user_logged_in()) : ?>
                    <p><span>Employer </span><a href="<?php echo esc_url( wp_login_url() ); ?>">Login</a> / <a href="<?php echo esc_url( site_url( 'register' ) ); ?>">Register</a></p>
                <?php elseif (is_user_logged_in()) : ?>
                    <p id="logged_in_as">Logged in as <span><?php echo $current_user->user_login; ?></span></p>
                    <p><a href="<?php echo esc_url( site_url( 'employer' ) ); ?>">My Account</a></p> / 
                    <p><a href="<?php echo esc_url( wp_logout_url( home_url() ) ); ?>">Logout</a></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="wrap">  

    <div id="nav" class="clearfix">
        <div class="wrap_inner">
            <div class="col col_span_2_10">
                <a href="<?php echo esc_url( site_url() ); ?>" id="logo"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" width="180px" height="78px" alt="Turf Jobs Logo" class="non_responsive" /></a>
            </div>
            <div class="col col_span_8_10">
                <?php
                if (in_array('employer', (array) $current_user->roles)) :
                    $args = array(
                        'menu'              => 'Employer',
                        'container_id'      => 'menu-employer-container',
                        'container_class'   => 'clearfix'
                    );
                    wp_nav_menu($args);
                endif; ?>
                <div id="social_media" class="clearfix employer_social_media">
                    <a href="<?php echo esc_url( 'https://twitter.com/TurfJobsUK' ); ?>" target="_blank" class="desk-twitter"><span class="fa fa-twitter"></span></a>
                    <a href="<?php echo esc_url( 'http://www.facebook.com/turfjobs' ); ?>" target="_blank" class="desk-facebook"><span class="fa fa-facebook"></span></a>
                    <a href="<?php echo esc_url( 'https://uk.linkedin.com/groups/UK-Turf-Industry-3395568/about' ); ?>" target="_blank" class="desk-linked"><span class="fa fa-linkedin"></span></a>
                    <a href="<?php echo esc_url( site_url('feed-job-listings') ); ?>" target="_blank" class="desk-rss"><span class="fa fa-rss"></span></a>
                </div>
            </div>  
        </div><!--END .wrap_inner-->          
    </div> 

    <div id="login_bar_mobile" class="clearfix">
        <div class="col col_span_10_10">
            <?php if (!is_user_logged_in()) : ?>
                <p><span>Employer </span><a href="<?php echo esc_url( wp_login_url() ); ?>">Login</a> / <a href="<?php echo esc_url( site_url( 'register' ) ); ?>">Register</a></p>
            <?php elseif (is_user_logged_in()) : ?>
                <p><a href="<?php echo esc_url( site_url( 'employer' ) ); ?>">My Account</a></p> / 
                <p><a href="<?php echo esc_url( wp_logout_url( home_url() ) ); ?>">Logout</a></p>
            <?php endif; ?>
        </div>
    </div>

    <div id="nav_mobile" class="clearfix">
        <div class="col col_span_6_10" id="nav_mobile_menu"><a href=""><span class="fa fa-navicon"></span></a></div>
        <div class="col col_span_1_10 nav_mobile_icon"><a href="<?php echo esc_url( 'https://twitter.com/TurfJobsUK' ); ?>" target="_blank"><span class="fa fa-twitter"></span></a></div>
        <div class="col col_span_1_10 nav_mobile_icon"><a href="<?php echo esc_url( 'http://www.facebook.com/turfjobs' ); ?>" target="_blank"><span class="fa fa-facebook"></span></a></div>
        <div class="col col_span_1_10 nav_mobile_icon"><a href="<?php echo esc_url( 'https://uk.linkedin.com/groups/UK-Turf-Industry-3395568/about' ); ?>" target="_blank"><span class="fa fa-linkedin"></span></a></div>
        <div class="col col_span_1_10 nav_mobile_icon"><a href="<?php echo esc_url( site_url('feed-job-listings') ); ?>" target="_blank"><span class="fa fa-rss"></span></a></div>                      
        <?php
        if (in_array('employer', (array) $current_user->roles)) :
            $args = array(
                'menu'              => 'Employer',
                'container'         => false,
                'menu_id'           => 'nav_mobile_toggle',
                'menu_class'        => 'list_no_style'
            );
            wp_nav_menu($args);
        endif; ?>                              
    </div>            
    <div id="nav_mobile_logo" class="clearfix">
        <div class="col col_span_10_10">
            <a href="<?php echo esc_url( site_url() ); ?>" id="logo"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" width="180px" height="78px" alt="Turf Jobs Logo" class="non_responsive" /></a>
        </div>
    </div>

</div><!--END .wrap-->

<div class="wrap">
    <div class="wrap_inner">