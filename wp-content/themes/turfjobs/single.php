<?php
/**
 * The Template for displaying all single posts, including a job
 */

get_header(); ?>

	<div id="content" class="clearfix">
		<div class="col col_span_10_10">

			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1><?php the_title(); ?></h1>
					<p class="single_blog_date"><?php the_date('F j, Y', 'Posted on '); ?></p>
					<?php the_content(); ?>
				</article><!-- #post -->
			<?php endwhile; // end of the loop. ?>

		</div>
	</div>

<?php get_footer(); ?>