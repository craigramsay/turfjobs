<?php
/**
 * Template Name: Job Listings RSS
 */
/**
 * RSS2 Feed Template for displaying RSS2 Posts feed.
 *
 * @package WordPress
 */
$args = array(
	'post_type' 	=> 'job_listing',
	'post_status'	=> 'publish',
	'posts_per_page'=> '10',
	'meta_query'	=> array(
		array(
			'key'     => '_filled',
			'value'   => '1',
			'compare' => '!='
			)
		)
);
$job_listings = new WP_Query( $args );

header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);
$more = 1;

echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>

<rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	<?php
	/**
	 * Fires at the end of the RSS root to add namespaces.
	 *
	 * @since 2.0.0
	 */
	do_action( 'rss2_ns' );
	?>
>
<channel>
	<title><?php bloginfo_rss('name'); ?></title>
	<atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
	<link><?php bloginfo_rss('url') ?></link>
	<description><?php bloginfo_rss("description") ?></description>
	<lastBuildDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_lastpostmodified('GMT'), false); ?></lastBuildDate>
	<language><?php bloginfo_rss( 'language' ); ?></language>
	<category>Sports and Amenity Turf Jobs</category>
	<?php
	$duration = 'hourly';
	/**
	 * Filter how often to update the RSS feed.
	 *
	 * @since 2.1.0
	 *
	 * @param string $duration The update period.
	 *                         Default 'hourly'. Accepts 'hourly', 'daily', 'weekly', 'monthly', 'yearly'.
	 */
	?>
	<sy:updatePeriod><?php echo apply_filters( 'rss_update_period', $duration ); ?></sy:updatePeriod>
	<?php
	$frequency = '1';
	/**
	 * Filter the RSS update frequency.
	 *
	 * @since 2.1.0
	 *
	 * @param string $frequency An integer passed as a string representing the frequency
	 *                          of RSS updates within the update period. Default '1'.
	 */
	?>
	<sy:updateFrequency><?php echo apply_filters( 'rss_update_frequency', $frequency ); ?></sy:updateFrequency>
	<?php
	/**
	 * Fires at the end of the RSS2 Feed Header.
	 *
	 * @since 2.0.0
	 */
	do_action( 'rss2_head');

	function yoast_rss_text_limit($string, $length, $replacer = '...')
	{ 
		$string = strip_tags($string);
			if(strlen($string) > $length) 
			return (preg_match('/^(.*)\W.*$/', substr($string, 0, $length+1), $matches) ? $matches[1] : substr($string, 0, $length)) . $replacer;   
		return $string; 
	}

	while( $job_listings->have_posts()) : $job_listings->the_post();
	$content = yoast_rss_text_limit($post->post_content, 400);
	//if (single_job_listing_post_meta('', '_job_company_hide_details', true, false) != 1) :
	//	$content .= '<p>Company: ' . single_job_listing_post_meta('', '_job_company_name', true, false) .'</p>';
	//endif;	
	$content .= '<p>Organisation Type: ' . the_job_organisation_type('', false) . '<br />';	
	$content .= 'Job Type: ' . the_job_type('', false) . '<br />';
	$content .= 'Location: ' . the_job_location('', false) . '<br />';
	$content .= 'Closing Date: ' . date( "d/m/Y", strtotime( single_job_listing_post_meta('', '_job_expires', true, false) ) ) . '</p>';	
	?>
	<item>
		<title><![CDATA[<?php the_title_rss() ?>]]></title>
		<link><?php the_permalink_rss() ?></link>
		<pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
		<dc:creator><![CDATA[<?php the_author() ?>]]></dc:creator>
		<?php the_category_rss('rss2') ?>
		<guid isPermaLink="false"><?php the_guid(); ?></guid>
		<category><?php the_job_organisation_type(); ?></category>
		<description><![CDATA[<?php echo $content; ?>]]></description>
<?php rss_enclosure(); ?>
	<?php
	/**
	 * Fires at the end of each RSS2 feed item.
	 *
	 * @since 2.0.0
	 */
	do_action( 'rss2_item' );
	?>
	</item>
	<?php endwhile; ?>
</channel>
</rss>