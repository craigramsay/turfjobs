<?php
/**
 * Template Name: National Apprenticeships
 */
get_header(); ?>

	<div id="content" class="clearfix">
		<div class="col col_span_10_10">

			<?php while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
					<?php
					// $data = file_get_contents("https://apprenticeshipvacancymatchingservice.lsc.gov.uk/VacancyRssFeedService/VacancyRss.aspx?feedType=1&dayRange=30&occupationCode=ALB"); 
					$data = file_get_contents("https://rss.findapprenticeship.service.gov.uk/VacancyRssFeedService/VacancyRss.aspx?feedType=1&dayRange=28&occupationCode=ALB"); 
					$data = simplexml_load_string($data);

					$external_apprenticeships = array(); 
					 
					foreach($data->channel->item as $item) 
					{     
					 
					    $external_apprenticeships[] = array 
					    ( 
					        'link' => (string)$item->link, 
					        'category' => (string)$item->category, 
					        'title' => (string)$item->title, 
					        'description' => (string)$item->description, 
					        'pubDate' => (string)$item->pubDate
					    ); 
					}

					//if not Horticulture category then unset
					foreach ($external_apprenticeships as $k => $apprenticeship) {
					    if (strpos($apprenticeship['category'], 'Horticulture') === false) {
					        unset($external_apprenticeships[$k]);
					    }
					}

					//renumber indexes from 0 again
					$external_apprenticeships = array_values($external_apprenticeships);

					//set up job title searches
					$search_terms = array('grounds man', 'groundsman', 'groundsperson', 'grounds person', 'greenkeeper', 'green keeper', 'grounds maintenance', 'greens keeper', 'grounds keeper', 'greenkeeping', 'groundscare', 'groundskeeper');

					//create new array where a job title is found
					foreach ($search_terms as $key => $term) {
					    foreach ($external_apprenticeships as $k => $apprenticeship) {
					        if (stripos($apprenticeship['title'], $term) !== false) {
					            $ext_app[] = $external_apprenticeships[$k];
					        }
					    }
					}

					?>

					<div class="ext_app_showing_jobs" id="external_apprenticeships">
					    Showing <?php echo esc_attr(count($ext_app)); ?> External Apprenticeships
					</div>
					<?php if ($ext_app != '') { ?>	
						<ul class="ext_app_listings list_no_style">
						<?php foreach ($ext_app as $app_key => $app_value) : ?>
						<li>
						    <h4 class="ext_app_listings_title"><a href="<?php echo esc_url( $app_value['link'] ) ?>" target="_blank"><?php echo esc_attr( $app_value['title'] ); ?></a></h4>
						    <table class="ext_app_listings_table">
						        <tr>
						            <td>Industry</td>
						            <td class="ext_app_listings_job_type"><?php echo esc_attr( $app_value['category'] ); ?></td>
						        </tr>
						        <tr>
						            <td>Date Posted</td>
						            <td><?php echo esc_attr( date( 'd/m/Y', strtotime($app_value['pubDate']) ) ); ?></td>
						        </tr>          
						    </table>    
						    <div class="ext_app_listings_description">      
						        <p><?php echo esc_attr( $app_value['description'] ); ?></p>
						    </div>
						    <img src="<?php echo get_template_directory_uri(); ?>/img/apprenticeships_logo.png" alt="Apprenticeships_logo" class="ext_app_listings_logo" />
						</li>
						<?php endforeach; ?>
						</ul>
					<?php } else { ?>
						<p>There are currently no apprenticeships listed.</p>
					<?php } ?>
					<h3 class="content">Share on a Social Network</h3>
					<!-- AddThis Button BEGIN -->
					<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
					<a class="addthis_button_facebook"></a>
					<a class="addthis_button_twitter"></a>
					<a class="addthis_button_google_plusone_share"></a>
					<a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
					</div>
					<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52b87ac90cd5ff2d"></script>
					<!-- AddThis Button END -->
			</article><!-- #post -->
			<?php endwhile; // end of the loop. ?>
			
		</div>
	</div>

<?php get_footer(); ?>