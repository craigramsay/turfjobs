<?php
/**
 * Template Name: Careers Advice
 */
get_header(); ?>

<div id="content" class="clearfix">

	<? if (!is_page('jobs')) { ?>
		<div id="home_banner" class="clearfix">
			<div class="col col_span_10_10">
				<h3>List your job vacancy for just £99</h3>
				<?php if ( is_user_logged_in() ) { ?>
					<a href="<?php echo esc_url( site_url( 'employer/add-job' ) ); ?>" class="button">Add Job</a>
				<?php } else { ?>
					<a href="<?php echo esc_url( site_url( 'register' ) ); ?>" class="button">Register Now</a>
				<?php } ?>
			</div>
		</div>
	<?php } ?>

	<div id="content_with_sidebar" class="clearfix">

		<div class="col col_span_3_10">
			<?php if ( $page = get_page_by_path( 'careers-advice' ) ) :
				$args = array(
					'child_of'		=> $page->ID,
					'title_li'		=> '',
					'post_status'	=> 'publish'
				);
				echo '<h3 id="careers_advice_heading">' . $page->post_title . '</h3>';
				echo '<ul id="careers_advice_sidebar" class="list_no_style">';
				echo '<li '; echo $post->post_name == 'careers-advice' ? 'class="current_page_item"' : ''; echo '><a href="' . get_permalink($page->ID) . '">' . $page->post_title . ' Home</a></li>';
				wp_list_pages($args);
				echo '</ul>';
			endif; ?>
		</div>

		<div class="col col_span_7_10">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; // end of the loop. ?>
			
		</div>

	</div>	

</div>

<?php get_footer(); ?>