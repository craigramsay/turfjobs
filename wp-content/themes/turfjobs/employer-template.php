<?php
/**
 * Template Name: Employer Template
 */
if (!is_user_logged_in())
	wp_safe_redirect(wp_login_url());

//global $current_user;
//get_currentuserinfo();
//if(!in_array('employer', (array) $current_user->roles ))
//	wp_die(__('Please use the admin section.'));

get_header('employer'); ?>

	<div id="content" class="clearfix">
		<div class="col col_span_10_10">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
			<?php endwhile; // end of the loop. ?>

		</div>
	</div>	

<?php get_footer(); ?>