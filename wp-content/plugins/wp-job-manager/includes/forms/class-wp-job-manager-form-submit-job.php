<?php

/**
 * WP_Job_Manager_Form_Submit_Job class.
 */
class WP_Job_Manager_Form_Submit_Job extends WP_Job_Manager_Form {

	public    static $form_name = 'submit-job';
	protected static $job_id;
	protected static $preview_job;
	protected static $steps;
	protected static $step = 0;
	//MALINKY__ADDED
	public    static $errors;	

	/**
	 * Init form
	 */
	public static function init() {
		add_action( 'wp', array( __CLASS__, 'process' ) );

		self::$steps  = (array) apply_filters( 'submit_job_steps', array(
			'submit' => array(
				'name'     => __( 'Submit Details', 'job_manager' ),
				'view'     => array( __CLASS__, 'submit' ),
				'handler'  => array( __CLASS__, 'submit_handler' ),
				'priority' => 10
				),
			'preview' => array(
				'name'     => __( 'Preview', 'job_manager' ),
				'view'     => array( __CLASS__, 'preview' ),
				'handler'  => array( __CLASS__, 'preview_handler' ),
				'priority' => 20
			),
			'done' => array(
				'name'     => __( 'Done', 'job_manager' ),
				'view'     => array( __CLASS__, 'done' ),
				'priority' => 30
			)
		) );

		uasort( self::$steps, array( __CLASS__, 'sort_by_priority' ) );

		// Get step/job
		if ( isset( $_POST['step'] ) ) {
			self::$step = is_numeric( $_POST['step'] ) ? max( absint( $_POST['step'] ), 0 ) : array_search( $_POST['step'], array_keys( self::$steps ) );
		} elseif ( ! empty( $_GET['step'] ) ) {
			self::$step = is_numeric( $_GET['step'] ) ? max( absint( $_GET['step'] ), 0 ) : array_search( $_GET['step'], array_keys( self::$steps ) );
		}
		self::$job_id = ! empty( $_REQUEST['job_id'] ) ? absint( $_REQUEST[ 'job_id' ] ) : 0;

		// Validate job ID if set
		if ( self::$job_id && ! in_array( get_post_status( self::$job_id ), apply_filters( 'job_manager_valid_submit_job_statuses', array( 'preview' ) ) ) ) {
			self::$job_id = 0;
			self::$step   = 0;
		}
	}

	/**
	 * Get step from outside of the class
	 */
	public static function get_step() {
		return self::$step;
	}

	/**
	 * Increase step from outside of the class
	 */
	public static function next_step() {
		self::$step ++;
	}

	/**
	 * Decrease step from outside of the class
	 */
	public static function previous_step() {
		self::$step --;
	}

	/**
	 * Sort array by priority value
	 */
	private static function sort_by_priority( $a, $b ) {
		return $a['priority'] - $b['priority'];
	}

	/**
	 * Get the submitted job ID
	 * @return int
	 */
	public static function get_job_id() {
		return absint( self::$job_id );
	}

	/**
	 * init_fields function.
	 *
	 * @access public
	 * @return void
	 */
	public static function init_fields() {
		if ( self::$fields )
			return;

		self::$fields = apply_filters( 'submit_job_form_fields', array(
			'job' => array(
				'job_title' => array(
					'label'       		=> __( 'Job Title', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'text',
					'validation_rules' 	=> array('required'),
					'placeholder' 		=> '',
					'priority'    		=> 1
				),
				'job_type' => array(
					'label'       		=> __( 'Job Type', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'select',
					'validation_rules' 	=> array('please_choose'),
					'options'     		=> self::job_types(),
					'placeholder' 		=> '',
					'priority'    		=> 2
				),
				'job_category' => array(
					'label'       		=> __( 'Job Category', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'select',
					'validation_rules' 	=> array('please_choose'),
					'options'     		=> self::job_categories(),
					'placeholder' 		=> '',
					'priority'    		=> 3
				),
				'job_description' => array(
					'label'       		=> __( 'Description', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'job-description',
					'validation_rules' 	=> array('required'),
					'placeholder' 		=> '',
					'priority'    		=> 4
				),
				//MALINKY__ADDED
				'job_location' => array(
					'label'       		=> __( 'Job Location', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'select',
					'validation_rules' 	=> array('please_choose'),
					'options'     		=> self::job_locations(),
					'placeholder' 		=> '',
					'priority'    		=> 5
				),
				'job_person_specification' => array(
					'label'       		=> __( 'Person Specification', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'job-description',
					'validation_rules' 	=> array('required'),
					'placeholder' 		=> '',
					'priority'    		=> 6
				),
				'job_employers_reference' => array(
					'label'       		=> __( 'Job Reference', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'text',
					'validation_rules' 	=> array('required'),
					'placeholder' 		=> '',
					'priority'    		=> 7
				),
				'job_expires' => array(
					'label'       		=> __( 'Closing Date', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'date',
					'validation_rules' 	=> array('date'),
					'placeholder' 		=> 'DD-MM-YYYY',
					'priority'    		=> 8,
				),
				'job_how_to_apply' => array(
					'label'       		=> __( 'How to Apply', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'job-description',
					'validation_rules' 	=> array('required'),
					'placeholder' 		=> '',
					'priority'    		=> 9
				),	
				'job_salary_type' => array(
					'label'       		=> __( 'Salary Type', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'radio',
					'validation_rules' 	=> array('required'),
					'options'	  		=> array('per-hour' => 'Per Hour', 'annual' => 'Annual', 'not-applicable' => 'Not Applicable'),					
					'placeholder' 		=> '',
					'priority'    		=> 10
				),	
				'job_salary_amount' => array(
					'label'       		=> __( 'Salary Amount', 'job_manager' ),
					'label_type'		=> __( '(optional)', 'job_manager' ),
					'type'        		=> 'text',
					'validation_rules' 	=> array('salary'),
					'placeholder' 		=> 'Examples: 7.00 or 18,000.00 or 18,000.00 - 20,000.00',
					'priority'    		=> 11
				),												
				/*MALINKY__REMOVED
				'job_location' => array(
					'label'       => __( 'Job location', 'job_manager' ),
					'description' => __( 'Leave this blank if the job can be done from anywhere (i.e. telecommuting)', 'job_manager' ),
					'type'        => 'text',
					'required'    => false,
					'placeholder' => __( 'e.g. "London, UK", "New York", "Houston, TX"', 'job_manager' ),
					'priority'    => 2
				),
				'application' => array(
					'label'       => __( 'Application email/URL', 'job_manager' ),
					'type'        => 'text',
					'required'    => true,
					'placeholder' => __( 'Enter an email address or website URL', 'job_manager' ),
					'priority'    => 8
				),*/
			),
			'company' => array(
				'job_company_hide_details' => array(
					'label'       		=> __( 'Hide Company Details for Job', 'job_manager' ),
					'label_type'		=> __( '(optional)', 'job_manager' ),
					'type'        		=> 'singlecheckbox',
					'validation_rules' 	=> array('singlecheckbox'),
					'placeholder' 		=> '',
					'priority'    		=> 1,
				)/*,
				'job_company_name' => array(
					'label'       		=> __( 'Company Name', 'job_manager' ),
					'label_type'		=> __( '(optional)', 'job_manager' ),
					'type'        		=> 'text',
					'placeholder' 		=> '',
					'priority'    		=> 2,
					'value'		  		=> get_user_meta(get_current_user_id(), '_company_name', true)
				),
				'job_company_address' => array(
					'label'       		=> __( 'Address', 'job_manager' ),
					'label_type'		=> __( '(optional)', 'job_manager' ),
					'type'        		=> 'text',
					'placeholder' 		=> '',
					'priority'    		=> 3,
					'value'		  		=> get_user_meta(get_current_user_id(), '_company_address', true)
				),
				'job_company_town' => array(
					'label'       		=> __( 'Town', 'job_manager' ),
					'label_type'		=> __( '(optional)', 'job_manager' ),
					'type'        		=> 'text',
					'placeholder' 		=> '',
					'priority'    		=> 4,
					'value'		  		=> get_user_meta(get_current_user_id(), '_company_town', true)
				),
				'job_company_county' => array(
					'label'       		=> __( 'County', 'job_manager' ),
					'label_type'		=> __( '(required)', 'job_manager' ),
					'type'        		=> 'select',
					'validation_rules' 	=> array('please_choose', 'county'),
					'options'	  		=> self::company_counties(),
					'placeholder' 		=> '',
					'priority'    		=> 5,
					'value'		  		=> get_user_meta(get_current_user_id(), '_company_county', true)
				),
				'job_company_postcode' => array(
					'label'       		=> __( 'Postcode', 'job_manager' ),
					'label_type'		=> __( '(optional)', 'job_manager' ),
					'type'        		=> 'text',
					'validation_rules' 	=> array('postcode'),
					'placeholder' 		=> '',
					'priority'    		=> 6,
					'value'		  		=> get_user_meta(get_current_user_id(), '_company_postcode', true)
				),
				'job_company_website' => array(
					'label'       		=> __( 'Website URL', 'job_manager' ),
					'label_type'		=> __( '(optional)', 'job_manager' ),
					'type'        		=> 'text',
					'validation_rules' 	=> array('url'),
					'placeholder' 		=> 'Example: http://www.turfjobs.co.uk',
					'priority'    		=> 7,
					'value'		  		=> get_user_meta(get_current_user_id(), '_company_website', true)
				),
				'job_company_twitter' => array(
					'label'       		=> __( 'Twitter URL', 'job_manager' ),
					'label_type'		=> __( '(optional)', 'job_manager' ),
					'type'        		=> 'text',
					'validation_rules' 	=> array('url'),
					'placeholder' 		=> 'Example: http://www.twitter.com/turfjobs',
					'priority'    		=> 8,
					'value'		  		=> get_user_meta(get_current_user_id(), '_company_twitter', true)
				),
				'job_company_facebook' => array(
					'label'       		=> __( 'Facebook URL', 'job_manager' ),
					'label_type'		=> __( '(optional)', 'job_manager' ),
					'type'        		=> 'text',
					'validation_rules' 	=> array('url'),
					'placeholder' 		=> 'Example: http://www.facebook.com/turfjobs',
					'priority'    		=> 9,
					'value'		  		=> get_user_meta(get_current_user_id(), '_company_facebook', true)
				),
				'job_company_googleplus' => array(
					'label'       		=> __( 'Google Plus URL', 'job_manager' ),
					'label_type'		=> __( '(optional)', 'job_manager' ),
					'type'        		=> 'text',
					'validation_rules' 	=> array('url'),
					'placeholder' 		=> 'Example: http://plus.google.com/+turfjobscouk',
					'priority'    		=> 10,
					'value'		  		=> get_user_meta(get_current_user_id(), '_company_googleplus', true)
				)*/			
			)
		) );

		if ( ! get_option( 'job_manager_enable_categories' ) || wp_count_terms( 'job_listing_category' ) == 0 )
			unset( self::$fields['job']['job_category'] );

		//MALINKY__ADDED
		//remove ability to edit closing date / expires on edit
		if ($_GET['action'] && $_GET['action'] == 'edit') {
			if (is_numeric($_GET['job_id'])) {
				unset(self::$fields['job']['job_expires']);
				//if edit add the saved company fields not those stored under the employers profile
				/*self::$fields['company']['job_company_name']['value'] = get_post_meta( $_GET['job_id'], '_job_company_name', true );
				self::$fields['company']['job_company_address']['value'] = get_post_meta( $_GET['job_id'], '_job_company_address', true );
				self::$fields['company']['job_company_town']['value'] = get_post_meta( $_GET['job_id'], '_job_company_town', true );
				self::$fields['company']['job_company_county']['value'] = get_post_meta( $_GET['job_id'], '_job_company_county', true );
				self::$fields['company']['job_company_postcode']['value'] = get_post_meta( $_GET['job_id'], '_job_company_postcode', true );
				self::$fields['company']['job_company_website']['value'] = get_post_meta( $_GET['job_id'], '_job_company_website', true );
				self::$fields['company']['job_company_twitter']['value'] = get_post_meta( $_GET['job_id'], '_job_company_twitter', true );
				self::$fields['company']['job_company_facebook']['value'] = get_post_meta( $_GET['job_id'], '_job_company_facebook', true );
				self::$fields['company']['job_company_googleplus']['value'] = get_post_meta( $_GET['job_id'], '_job_company_googleplus', true );*/
			} else {
				echo wpautop( __( 'This job listing can\'t be edited.', 'job_manager' ) );
				exit();
			}
		}

		if ($_GET['action'] && $_GET['action'] == 'relist') {
			if (is_numeric($_GET['job_id']) && get_post_type($_GET['job_id']) == 'job_listing') {
				//if relist add all the job data to the add job form
				self::$fields['job']['job_title']['value'] = get_the_title($_GET['job_id']);
				self::$fields['job']['job_type']['value'] = wp_get_post_terms( $_GET['job_id'], 'job_listing_type' );
				self::$fields['job']['job_type']['value'] = self::$fields['job']['job_type']['value'][0]->slug;
				self::$fields['job']['job_category']['value'] = wp_get_post_terms( $_GET['job_id'], 'job_listing_category' );
				self::$fields['job']['job_category']['value'] = self::$fields['job']['job_category']['value'][0]->slug;
				self::$fields['job']['job_description']['value'] = get_post_field( 'post_content', $_GET['job_id'], 'display' );
				self::$fields['job']['job_location']['value'] = wp_get_post_terms( $_GET['job_id'], 'job_listing_location' );
				self::$fields['job']['job_location']['value'] = self::$fields['job']['job_location']['value'][0]->slug;
				self::$fields['job']['job_person_specification']['value'] = get_post_meta( $_GET['job_id'], '_job_person_specification', true );
				self::$fields['job']['job_employers_reference']['value'] = get_post_meta( $_GET['job_id'], '_job_employers_reference', true );
				self::$fields['job']['job_how_to_apply']['value'] = get_post_meta( $_GET['job_id'], '_job_how_to_apply', true );
				self::$fields['job']['job_salary_type']['value'] = get_post_meta( $_GET['job_id'], '_job_salary_type', true );
				self::$fields['job']['job_salary_amount']['value'] = get_post_meta( $_GET['job_id'], '_job_salary_amount', true );
				self::$fields['company']['job_company_hide_details']['value'] = get_post_meta( $_GET['job_id'], '_job_company_hide_details', true );	
				/*self::$fields['company']['job_company_name']['value'] = get_post_meta( $_GET['job_id'], '_job_company_name', true );
				self::$fields['company']['job_company_address']['value'] = get_post_meta( $_GET['job_id'], '_job_company_address', true );
				self::$fields['company']['job_company_town']['value'] = get_post_meta( $_GET['job_id'], '_job_company_town', true );
				self::$fields['company']['job_company_county']['value'] = get_post_meta( $_GET['job_id'], '_job_company_county', true );
				self::$fields['company']['job_company_postcode']['value'] = get_post_meta( $_GET['job_id'], '_job_company_postcode', true );
				self::$fields['company']['job_company_website']['value'] = get_post_meta( $_GET['job_id'], '_job_company_website', true );
				self::$fields['company']['job_company_twitter']['value'] = get_post_meta( $_GET['job_id'], '_job_company_twitter', true );
				self::$fields['company']['job_company_facebook']['value'] = get_post_meta( $_GET['job_id'], '_job_company_facebook', true );
				self::$fields['company']['job_company_googleplus']['value'] = get_post_meta( $_GET['job_id'], '_job_company_googleplus', true );*/
			} else {
				echo wpautop( __( 'This job listing can\'t be relisted.', 'job_manager' ) );
				exit();
			}
		}

	}

	/**
	 * Get post data for fields
	 *
	 * @return array of data
	 */
	protected static function get_posted_fields() {
		self::init_fields();

		$values = array();

		foreach ( self::$fields as $group_key => $fields ) {
			foreach ( $fields as $key => $field ) {
				$values[ $group_key ][ $key ] = isset( $_POST[ $key ] ) ? stripslashes( $_POST[ $key ] ) : '';

				switch ( $key ) {
					case 'job_description' :
						$values[ $group_key ][ $key ] = wp_kses_post( trim( $values[ $group_key ][ $key ] ) );
					break;
					//MALINKY__ADDED
					case 'job_person_specification' :
						$values[ $group_key ][ $key ] = wp_kses_post( trim( $values[ $group_key ][ $key ] ) );
					break;	
					case 'job_how_to_apply' :
						$values[ $group_key ][ $key ] = wp_kses_post( trim( $values[ $group_key ][ $key ] ) );
					break;										
					/*MALINKY__REMOVED
					case 'company_logo' :
						$image_url = self::upload_image( 'company_logo' );
						if ( $image_url )
							$values[ $group_key ][ $key ] = $image_url;
					break;*/
					default:
						$values[ $group_key ][ $key ] = sanitize_text_field( $values[ $group_key ][ $key ] );
					break;
				}

				// Set fields value
				self::$fields[ $group_key ][ $key ]['value'] = $values[ $group_key ][ $key ];
			}
		}

		return $values;
	}

	/**
	 * Validate the posted fields
	 *
	 * @return bool on success, WP_ERROR on failure
	 */
	protected static function validate_fields( $values ) {

		self::$errors = new WP_Error();

		foreach ( self::$fields as $group_key => $fields ) {
			foreach ( $fields as $key => $field ) {
				//MALINKY__ADDED
				if ( is_array($field['validation_rules']) ) {
					foreach ($field['validation_rules'] as $rule_key => $rule ) {
						switch ($rule) {
							case 'required':
								if ( empty( $values[ $group_key ][ $key ] ) ) {
									self::$errors->add( $key . '_error', __(self::error_messages($key . '_error')));
								}
							break;
							case 'please_choose':
								if ($values[ $group_key ][ $key ] == 'please_choose') {
									self::$errors->add( $key . '_error', __(self::error_messages($key . '_error')));
								}	
							break;
							case 'county':
								if (!preg_match('/^[a-z\-]+$/', $values[ $group_key ][ $key ])) {
									self::$errors->add( $key . '_error', __(self::error_messages($key . '_error')));
								}	
							break;							
							case 'date':
								if(preg_match("/^(\d{2})-(\d{2})-(\d{4})$/", $values[ $group_key ][ $key ], $matches)) {									
									if(!checkdate($matches[2], $matches[1], $matches[3])) {
										self::$errors->add( $key . '_error', __(self::error_messages($key . '_error')));	
									}
								} else {
									self::$errors->add( $key . '_error', __(self::error_messages($key . '_error')));
								}
							break;
							case 'salary':
								if ($values[ $group_key ][ $key ] != '' && !preg_match('/^[0-9 \-,.]+$/', $values[ $group_key ][ $key ])) {
									self::$errors->add( $key . '_error', __(self::error_messages($key . '_error')));
								}
							break;
							case 'url':
								if ($values[ $group_key ][ $key ] != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $values[ $group_key ][ $key ])) {
									self::$errors->add( $key . '_error', __(self::error_messages($key . '_error')));
								}
							break;
							case 'singlecheckbox':							
								if ($values[ $group_key ][ $key ] != '' && $values[ $group_key ][ $key ] != '1' ) {
									self::$errors->add( $key . '_error', __(self::error_messages($key . '_error')));
								}
							break;
							case 'postcode':
								if ($values[ $group_key ][ $key ] != '' && !preg_match("~^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$~i", $values[ $group_key ][ $key ])) {
									self::$errors->add( $key . '_error', __(self::error_messages($key . '_error_2')));
								}
							break;																											
						}
					}					
				}

				//MALINKY_REMOVED
				//return new WP_Error( 'validation-error', sprintf( __( '%s is a required field', 'job_manager' ), $field['label'] ) );
			}
		}

		//MALINKY__ADDED
		if ( self::$errors->get_error_code() )
			return self::$errors;

		return apply_filters( 'submit_job_form_validate_fields', true, self::$fields, $values );
	}

	//MALINKY__ADDED
	/**
	 * error_messages function.
	 *
	 * @access private
	 * @return array
	 */
	private static function error_messages($error_code)
	{
		$error_messages = array(
			'job_title_error'				=> 'Please enter a job title.',
			'job_type_error'				=> 'Please choose a job type.',
			'job_category_error'			=> 'Please choose a job category.',
			'job_description_error'			=> 'Please enter a job description.',
			'job_location_error'			=> 'Please choose a job location.',
			'job_person_specification_error'=> 'Please enter a person specification.',
			'job_employers_reference_error'	=> 'Please enter an employers reference.',
			'job_expires_error'				=> 'Please enter a valid date.',
			'job_how_to_apply_error'		=> 'Please enter how to apply.',
			'job_salary_type_error'			=> 'Please choose a salary type.',
			'job_salary_amount_error'		=> 'Please enter a valid salary amount.',
			'job_company_hide_details_error'=> 'Please make a valid selection.',
			'job_company_name_error'		=> 'Please enter your company name.',
			'job_company_address_error'		=> 'Please enter your company address.',
			'job_company_town_error'		=> 'Please enter your company town.',
			'job_company_county_error'		=> 'Please choose a county.',
			'job_company_postcode_error'	=> 'Please enter your company postcode.',
			'job_company_postcode_error_2'	=> 'Please enter a valid postcode.',	
			'job_company_website_error'		=> 'Please enter a valid company website URL.',
			'job_company_twitter_error'		=> 'Please enter a valid company Twitter URL.',
			'job_company_facebook_error'	=> 'Please enter a valid company Facebook URL.',
			'job_company_googleplus_error'	=> 'Please enter a valid company Google Plus URL.'
		);
		return $error_messages[$error_code];
	}

	/**
	 * job_types function.
	 *
	 * @access private
	 * @return void
	 */
	private static function job_types() {
		$options = array();
		//MALINKY__ADDED
		$options['please_choose'] = 'Please Choose';
		$terms   = get_job_listing_types();
		foreach ( $terms as $term ) {
			$options[ $term->slug ] = $term->name;
		}

		return $options;
	}

	/**
	 * job_categories function.
	 *
	 * @access private
	 * @return void
	 */
	private static function job_categories() {
		$options = array();
		//MALINKY__ADDED
		$options['please_choose'] = 'Please Choose';
		$terms   = get_job_listing_categories();
		foreach ( $terms as $term ) {
			$options[ $term->slug ] = $term->name;
		}
		return $options;
	}

	//MALINKY__ADDED
	/**
	 * job_locations function.
	 *
	 * @access private
	 * @return void
	 */
	private static function job_locations() {
		$options = array();
		$options['please_choose'] = 'Please Choose';		
		$terms   = get_job_listing_locations();
		foreach ( $terms as $term ) {
			$options[ $term->slug ] = $term->name;
		}
		return $options;
	}	

	//MALINKY__ADDED
	/**
	 * company_counties function.
	 *
	 * @access private
	 * @return void
	 */
	private static function company_counties() {
		$options = array(
				'please_choose' => 'Please Choose',
				'aberdeenshire' => 'Aberdeenshire',
				'angus' => 'Angus',
				'antrim' => 'Antrim',
				'argyll-and-bute' => 'Argyll and Bute',
				'armagh' => 'Armagh',
				'ayrshire-and-arran' => 'Ayrshire and Arran',
				'banffshire' => 'Banffshire',
				'bedfordshire' => 'Bedfordshire',
				'belfast-city' => 'Belfast City',
				'berkshire' => 'Berkshire',
				'berwickshire' => 'Berwickshire',
				'bristol' => 'Bristol',
				'buckinghamshire' => 'Buckinghamshire',
				'caithness' => 'Caithness',
				'cambridgeshire' => 'Cambridgeshire',
				'cheshire' => 'Cheshire',
				'city-of-london' => 'City of London',
				'clackmannan' => 'Clackmannan',
				'clwyd' => 'Clwyd',
				'cornwall' => 'Cornwall',
				'cumbria' => 'Cumbria',
				'derbyshire' => 'Derbyshire',
				'devon' => 'Devon',
				'dorset' => 'Dorset',
				'down' => 'Down',
				'dumfries' => 'Dumfries',
				'dunbartonshire' => 'Dunbartonshire',
				'durham' => 'Durham',
				'dyfed' => 'Dyfed',
				'east-lothian' => 'East Lothian',
				'east-riding-of-yorkshire' => 'East Riding of Yorkshire',
				'east-sussex' => 'East Sussex',
				'essex' => 'Essex',
				'fermanagh' => 'Fermanagh',
				'fife' => 'Fife',
				'gloucestershire' => 'Gloucestershire',
				'greater-london' => 'Greater London',
				'greater-manchester' => 'Greater Manchester',
				'gwent' => 'Gwent',
				'gwynedd' => 'Gwynedd',
				'hampshire' => 'Hampshire',
				'herefordshire' => 'Herefordshire',
				'hertfordshire' => 'Hertfordshire',
				'inverness' => 'Inverness',
				'isle-of-wight' => 'Isle of Wight',
				'kent' => 'Kent',
				'kincardineshire' => 'Kincardineshire',
				'lanarkshire' => 'Lanarkshire',
				'lancashire' => 'Lancashire',
				'leicestershire' => 'Leicestershire',
				'lincolnshire' => 'Lincolnshire',
				'londonderry' => 'Londonderry',
				'londonderry-city' => 'Londonderry City',
				'merseyside' => 'Merseyside',
				'mid-glamorgan' => 'Mid Glamorgan',
				'midlothian' => 'Midlothian',
				'moray' => 'Moray',
				'nairn' => 'Nairn',
				'norfolk' => 'Norfolk',
				'north-yorkshire' => 'North Yorkshire',
				'northamptonshire' => 'Northamptonshire',
				'northumberland' => 'Northumberland',
				'nottinghamshire' => 'Nottinghamshire',
				'orkney' => 'Orkney',
				'oxfordshire' => 'Oxfordshire',
				'perth-and-kinross' => 'Perth and Kinross',
				'powys' => 'Powys',
				'renfrewshire' => 'Renfrewshire',
				'ross-and-cromarty' => 'Ross and Cromarty',
				'roxburgh,-ettrick-and-lauderdale' => 'Roxburgh, Ettrick and Lauderdale',
				'rutland' => 'Rutland',
				'shetland' => 'Shetland',
				'shropshire' => 'Shropshire',
				'somerset' => 'Somerset',
				'south-glamorgan' => 'South Glamorgan',
				'south-yorkshire' => 'South Yorkshire',
				'staffordshire' => 'Staffordshire',
				'stirling-and-falkirk' => 'Stirling and Falkirk',
				'suffolk' => 'Suffolk',
				'surrey' => 'Surrey',
				'sutherland' => 'Sutherland',
				'the-stewartry-of-kirkcudbright' => 'The Stewartry of Kirkcudbright',
				'tweeddale' => 'Tweeddale',
				'tyne-and-wear' => 'Tyne and Wear',
				'tyrone' => 'Tyrone',
				'warwickshire' => 'Warwickshire',
				'west-glamorgan' => 'West Glamorgan',
				'west-lothian' => 'West Lothian',
				'west-midlands' => 'West Midlands',
				'west-sussex' => 'West Sussex',
				'west-yorkshire' => 'West Yorkshire',
				'western-isles' => 'Western Isles',
				'wigtown' => 'Wigtown',
				'wiltshire' => 'Wiltshire',
				'worcestershire' => 'Worcestershire'
			);
			return $options;
	}		

	/**
	 * Process function. all processing code if needed - can also change view if step is complete
	 */
	public static function process() {
		$keys = array_keys( self::$steps );

		if ( isset( $keys[ self::$step ] ) && is_callable( self::$steps[ $keys[ self::$step ] ]['handler'] ) ) {
			call_user_func( self::$steps[ $keys[ self::$step ] ]['handler'] );
		}
	}

	/**
	 * output function. Call the view handler.
	 */
	public static function output() {
		$keys = array_keys( self::$steps );

		//MALINKY__REMOVED
		//self::show_errors();

		if ( isset( $keys[ self::$step ] ) && is_callable( self::$steps[ $keys[ self::$step ] ]['view'] ) ) {
			call_user_func( self::$steps[ $keys[ self::$step ] ]['view'] );
		}
	}

	/**
	 * Submit Step
	 */
	public static function submit() {
		global $job_manager, $post;

		self::init_fields();

		// Load data if neccessary
		if ( ! empty( $_POST['edit_job'] ) && self::$job_id ) {
			$job = get_post( self::$job_id );
			foreach ( self::$fields as $group_key => $fields ) {
				foreach ( $fields as $key => $field ) {
					switch ( $key ) {
						case 'job_title' :
							self::$fields[ $group_key ][ $key ]['value'] = $job->post_title;
						break;
						case 'job_description' :
							self::$fields[ $group_key ][ $key ]['value'] = $job->post_content;
						break;
						case 'job_type' :
							self::$fields[ $group_key ][ $key ]['value'] = current( wp_get_object_terms( $job->ID, 'job_listing_type', array( 'fields' => 'slugs' ) ) );
						break;
						case 'job_category' :
							self::$fields[ $group_key ][ $key ]['value'] = current( wp_get_object_terms( $job->ID, 'job_listing_category', array( 'fields' => 'slugs' ) ) );
						break;
						//MALINKY__ADDED
						case 'job_location' :
							self::$fields[ $group_key ][ $key ]['value'] = current( wp_get_object_terms( $job->ID, 'job_listing_location', array( 'fields' => 'slugs' ) ) );
						break;						
						default:
							self::$fields[ $group_key ][ $key ]['value'] = get_post_meta( $job->ID, '_' . $key, true );
						break;
					}
				}
			}

			self::$fields = apply_filters( 'submit_job_form_fields_get_job_data', self::$fields, $job );

		// Get user meta
		}
		/*
		MALINKY__REMOVED
		elseif ( is_user_logged_in() && empty( $_POST ) ) {
			if ( is_user_logged_in() ) {
				foreach ( self::$fields[ 'company' ] as $key => $field ) {
					self::$fields[ 'company' ][ $key ]['value'] = get_user_meta( get_current_user_id(), '_' . $key, true );
				}

				self::$fields = apply_filters( 'submit_job_form_fields_get_user_data', self::$fields, get_current_user_id() );
			}
		}
		*/

		get_job_manager_template( 'job-submit.php', array(
			'form'               => self::$form_name,
			'job_id'             => self::get_job_id(),
			'action'             => self::get_action(),
			'job_fields'         => self::get_fields( 'job' ),
			'company_fields'     => self::get_fields( 'company' ),
			'submit_button_text' => __( 'Preview Job Listing', 'job_manager' ),
			//MALINKY__ADDED
			'error_messages'     => self::$errors->errors,
			) );
	}

	/**
	 * Submit Step is posted
	 */
	public static function submit_handler() {
		try {

			// Get posted values
			$values = self::get_posted_fields();

			if ( empty( $_POST['submit_job'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'submit_form_posted' ) )
				return;

			//MALINKY__REMOVED
			// Validate required
			//if ( is_wp_error( ( $return = self::validate_fields( $values ) ) ) )
				//throw new Exception( $return->get_error_message() );

			//MALINKY__ADDED
			// Validate required
			if ( is_wp_error( ( self::validate_fields( $values ) ) ) )				
				return;

			// Account creation
			if ( ! is_user_logged_in() ) {
				$create_account = false;

				if ( job_manager_enable_registration() && ! empty( $_POST['create_account_email'] ) )
					$create_account = wp_job_manager_create_account( $_POST['create_account_email'] );

				if ( is_wp_error( $create_account ) )
					throw new Exception( $create_account->get_error_message() );
			}

			if ( job_manager_user_requires_account() && ! is_user_logged_in() )
				throw new Exception( __( 'You must be signed in to post a new job listing.' ) );

			// Update the job
			self::save_job( $values['job']['job_title'], $values['job']['job_description'], self::$job_id ? '' : 'preview', $values );
			self::update_job_data( $values );

			// Successful, show next step
			self::$step ++;

		} catch ( Exception $e ) {
			self::add_error( $e->getMessage() );
			return;
		}

	}

	/**
	 * Update or create a job listing from posted data
	 *
	 * @param  string $post_title
	 * @param  string $post_content
	 * @param  string $status
	 */
	protected static function save_job( $post_title, $post_content, $status = 'preview' ) {
		$job_data = apply_filters( 'submit_job_form_save_job_data', array(
			'post_title'     => $post_title,
			'post_content'   => $post_content,
			'post_type'      => 'job_listing',
			'comment_status' => 'closed'
		) );

		if ( $status )
			$job_data['post_status'] = $status;

		if ( self::$job_id ) {
			$job_data['ID'] = self::$job_id;
			wp_update_post( $job_data );
		} else {
			self::$job_id = wp_insert_post( $job_data );
		}
	}

	/**
	 * Set job meta + terms based on posted values
	 *
	 * @param  array $values
	 */
	protected static function update_job_data( $values ) {

		wp_set_object_terms( self::$job_id, array( $values['job']['job_type'] ), 'job_listing_type', false );
		
		//MALINKY__ADDED
		wp_set_object_terms( self::$job_id, array( $values['job']['job_location'] ), 'job_listing_location', false );

		if ( get_option( 'job_manager_enable_categories' ) && isset( $values['job']['job_category'] ) ) {
			wp_set_object_terms( self::$job_id, array( $values['job']['job_category'] ), 'job_listing_category', false );
		}

		//MALINKY__ADDED
		update_post_meta( self::$job_id, '_job_person_specification', $values['job']['job_person_specification'] );
		update_post_meta( self::$job_id, '_job_employers_reference', $values['job']['job_employers_reference'] );
		//ensure expiration can't be amended in inspector tools even the form field is removed from HTML in edit
		if ( $_GET['action'] != 'edit') {
			update_post_meta( self::$job_id, '_job_expires', date("Y-m-d", strtotime( $values['job']['job_expires'])));
		}
		update_post_meta( self::$job_id, '_job_how_to_apply', $values['job']['job_how_to_apply'] );
		update_post_meta( self::$job_id, '_job_salary_type', $values['job']['job_salary_type'] );
		update_post_meta( self::$job_id, '_job_salary_amount', $values['job']['job_salary_amount'] );

		update_post_meta( self::$job_id, '_job_company_hide_details', $values['company']['job_company_hide_details'] );
		if (empty($values['company']['job_company_hide_details']))
			update_post_meta( self::$job_id, '_job_company_hide_details', 0 );

		update_post_meta( self::$job_id, '_job_organisation_type', get_user_meta(get_current_user_id(), '_organisation_type', true) );

		/*update_post_meta( self::$job_id, '_job_company_name', $values['company']['job_company_name'] );
		update_post_meta( self::$job_id, '_job_company_address', $values['company']['job_company_address'] );
		update_post_meta( self::$job_id, '_job_company_town', $values['company']['job_company_town'] );
		update_post_meta( self::$job_id, '_job_company_county', $values['company']['job_company_county'] );
		update_post_meta( self::$job_id, '_job_company_postcode', $values['company']['job_company_postcode'] );
		update_post_meta( self::$job_id, '_job_company_website', $values['company']['job_company_website'] );
		update_post_meta( self::$job_id, '_job_company_twitter', $values['company']['job_company_twitter'] );
		update_post_meta( self::$job_id, '_job_company_facebook', $values['company']['job_company_facebook'] );
		update_post_meta( self::$job_id, '_job_company_googleplus', $values['company']['job_company_googleplus'] );*/

		/*
		MALINKY__REMOVED
		update_post_meta( self::$job_id, '_application', $values['job']['application'] );
		update_post_meta( self::$job_id, '_job_location', $values['job']['job_location'] );
		update_post_meta( self::$job_id, '_company_name', $values['company']['company_name'] );
		update_post_meta( self::$job_id, '_company_website', $values['company']['company_website'] );
		update_post_meta( self::$job_id, '_company_tagline', $values['company']['company_tagline'] );
		update_post_meta( self::$job_id, '_company_twitter', $values['company']['company_twitter'] );
		update_post_meta( self::$job_id, '_company_logo', $values['company']['company_logo'] );
		*/
		update_post_meta( self::$job_id, '_filled', 0 );
		update_post_meta( self::$job_id, '_featured', 0 );

		// And user meta to save time in future
		/*MALINKY__REMOVED
		if ( is_user_logged_in() ) {
			update_user_meta( get_current_user_id(), '_company_name', $values['company']['company_name'] );
			update_user_meta( get_current_user_id(), '_company_website', $values['company']['company_website'] );
			update_user_meta( get_current_user_id(), '_company_tagline', $values['company']['company_tagline'] );
			update_user_meta( get_current_user_id(), '_company_twitter', $values['company']['company_twitter'] );
			update_user_meta( get_current_user_id(), '_company_logo', $values['company']['company_logo'] );
		}
		*/

		do_action( 'job_manager_update_job_data', self::$job_id, $values );
	}

	/**
	 * Preview Step
	 */
	public static function preview() {
		global $job_manager, $post;

		if ( self::$job_id ) {

			$post = get_post( self::$job_id );
			setup_postdata( $post );

			//MALINKY__ADDED
			if (!is_user_logged_in()) {
				echo '<div class="job-manager-form">';
				get_job_manager_template( 'account-signin.php' );
				echo '</div>';
				return;
			}

			//MALINKY__ADDED
			if (get_current_user_id() != $post->post_author) {
				echo '<p class="job-manager-error">You don\'t have access to this job listing.</p>';
				return;
			}

			?>
			<form method="post" id="job_preview">
				<div class="job_listing_preview_title">
					<input type="submit" name="continue" id="job_preview_submit_button" class="button" value="<?php echo apply_filters( 'submit_job_step_preview_submit_text', __( 'Submit Listing', 'job_manager' ) ); ?>" />
					<input type="submit" name="edit_job" class="button" value="<?php _e( 'Edit Listing', 'job_manager' ); ?>" />
					<input type="hidden" name="job_id" value="<?php echo esc_attr( self::$job_id ); ?>" />
					<input type="hidden" name="step" value="<?php echo esc_attr( self::$step ); ?>" />
					<input type="hidden" name="job_manager_form" value="<?php echo self::$form_name; ?>" />
					<h2 id="job_listing_preview">
						<?php _e( 'Preview', 'job_manager' ); ?>
					</h2>
				</div>
				<div class="job_listing_preview single_job_listing">
					<h1><?php the_title(); ?></h1>
					<?php get_job_manager_template_part( 'content-single', 'job_listing' ); ?>
				</div>
			</form>
			<?php

			?>
			<script>
				jQuery(document).ready(function($){
					//$('h1').next('p').remove();
					//MALINKY_NON_PAYMENTS $('form#job_preview').prev('p').text('Your job listing preview is shown below. If you wish to proceed please click pay for listing.');
					$('h1').next('p').text('Your job listing preview is shown below. If you wish to proceed please click pay for listing.');
				});
			</script>

			<?php 
			wp_reset_postdata();
		}
	}

	/**
	 * Preview Step Form handler
	 */
	public static function preview_handler() {
		if ( ! $_POST )
			return;

		// Edit = show submit form again
		if ( ! empty( $_POST['edit_job'] ) ) {
			self::$step --;
		}
		// Continue = change job status then show next screen
		if ( ! empty( $_POST['continue'] ) ) {

			$job = get_post( self::$job_id );

			if ( $job->post_status == 'preview' ) {
				$update_job                = array();
				$update_job['ID']          = $job->ID;
				$update_job['post_status'] = get_option( 'job_manager_submission_requires_approval' ) ? 'pending' : 'publish';
				wp_update_post( $update_job );
			}

			self::$step ++;
		}
	}

	/**
	 * Done Step
	 */
	public static function done() {
		do_action( 'job_manager_job_submitted', self::$job_id );
		
		//MALINKY__ADDED
		add_filter( 'the_content', 'turfjobs_submit_success', 20 );
		function turfjobs_submit_success( $content )
		{
	 		//$content = sprintf('<div class="job-manager-message">Job listed successfully. To view your job listing <a href="%s">click here</a></div>', get_permalink( $job_id ) );
	 		$content = '<div class="job-manager-message">Thanks your job has been listed successfully. You will be emailed a receipt.</div>';
		    return $content;
		}
		//get_job_manager_template( 'job-submitted.php', array( 'job' => get_post( self::$job_id ) ) );
	}

	/**
	 * Upload Image
	 */
	public static function upload_image( $field_key ) {

		/** WordPress Administration File API */
		include_once( ABSPATH . 'wp-admin/includes/file.php' );

		/** WordPress Media Administration API */
		include_once( ABSPATH . 'wp-admin/includes/media.php' );

		if ( isset( $_FILES[ $field_key ] ) && ! empty( $_FILES[ $field_key ] ) && ! empty( $_FILES[ $field_key ]['name'] ) ) {
			$file   = $_FILES[ $field_key ];

			if ( $_FILES[ $field_key ]["type"] != "image/jpeg" && $_FILES[ $field_key ]["type"] != "image/gif" && $_FILES[ $field_key ]["type"] != "image/png" )
    			throw new Exception( __( 'Logo needs to be jpg, gif or png.', 'job_manager' ) );

			add_filter( 'upload_dir',  array( __CLASS__, 'upload_dir' ) );

			$upload = wp_handle_upload( $file, array( 'test_form' => false ) );

			remove_filter('upload_dir', array( __CLASS__, 'upload_dir' ) );

			if ( ! empty( $upload['error'] ) ) {
				throw new Exception( $upload['error'] );
			} else {
				return $upload['url'];
			}
		}
	}

	/**
	 * Filter the upload directory
	 */
	public static function upload_dir( $pathdata ) {
		$subdir             = '/job_listing_images';
		$pathdata['path']   = str_replace( $pathdata['subdir'], $subdir, $pathdata['path'] );
		$pathdata['url']    = str_replace( $pathdata['subdir'], $subdir, $pathdata['url'] );
		$pathdata['subdir'] = str_replace( $pathdata['subdir'], $subdir, $pathdata['subdir'] );
		return $pathdata;
	}
}
