<?php
switch ( $job->post_status ) :
	case 'publish' :
		echo '<div class="job-manager-message">';
			printf( __( 'Job listed successfully. To view your job listing <a href="%s">click here</a>.', 'job_manager' ), get_permalink( $job->ID ) );
		echo '</div>';
	break;
	case 'pending' :
		echo '<div class="job-manager-message">';
			printf( __( 'Job submitted successfully. Your job listing will be visible once approved.', 'job_manager' ), get_permalink( $job->ID ) );
		echo '</div>';
	break;
	default :
		do_action( 'job_manager_job_submitted_content_' . str_replace( '-', '_', sanitize_title( $job->post_status ) ), $job );
	break;
endswitch;