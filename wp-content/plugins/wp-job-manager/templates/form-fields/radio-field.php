<?php foreach ( $field['options'] as $option_key => $value ) : ?>
	<input type="radio" name="<?php echo esc_attr( $key ); ?>" value="<?php echo esc_attr( $option_key ); ?>" <?php if ( isset( $field['value'] ) ) checked( $field['value'], $option_key ); ?>><?php echo esc_attr( $value ); ?><br>
<?php endforeach; ?>