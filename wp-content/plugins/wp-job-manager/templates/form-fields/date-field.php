<input type="text" name="<?php echo esc_attr( $key ); ?>" id="<?php echo esc_attr( $key ); ?>" value="<?php echo isset( $field['value'] ) ? esc_attr( date( "d-m-Y", strtotime( $field['value'] ) ) ) : ''; ?>" />
<?php if ( ! empty( $field['placeholder'] ) ) : ?><small><?php echo $field['placeholder']; ?></small><?php endif; ?>

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#<?php echo esc_attr( $key ); ?>').datepicker({
        dateFormat : 'dd-mm-yy'
    });
});

</script>