<?php
$editor = array(
	'media_buttons' => false,
	'textarea_rows' => 8,
	'quicktags' => false,
	'tinymce' => array(
		'plugins' => 'paste',
		'paste_auto_cleanup_on_paste' => true,
		'paste_remove_styles' => true,
		'paste_text_sticky' => true,
		'paste_text_sticky_default' => true,
		'paste_retain_style_properties' => "none",
		'paste_strip_class_attributes' => "all",
		'theme_advanced_buttons1' => 'bold,italic,|,bullist,numlist,|,justifyleft,justifycenter,justifyright,|,link,|,undo,redo,|,|,code',
		'theme_advanced_buttons2' => '',
		'theme_advanced_buttons3' => '',
		'theme_advanced_buttons4' => ''
	),
);
wp_editor( isset( $field['value'] ) ? $field['value'] : '', $key, $editor );