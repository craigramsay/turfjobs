<?php
/**
 * Plugin Name: Malinky Visitor Feedback Widget
 * Plugin URI: 
 * Description: A brief description of the Plugin.
 * Version: 1.0
 * Author: Malinky
 * Author URI: http://www.malinkymedia.com
 * Copyright: 2013 Craig Ramsay
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

class Malinky_Visitor_Feedback_Widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			'malinky_visitor_feedback_widget', // Base ID
			__('Visitor Feedback Widget', 'malinky-visitor-feedback-widget'), // Name
			array( 'description' => __( 'Display Visitor Feedback', 'malinky-visitor-feedback-widget' ), ) // Args
		);
		define( 'MALINKY_VISITOR_FEEDBACK_WIDGET_URL', plugins_url( basename( plugin_dir_path( __FILE__ ) ) ) );
	}

	// widget form creation
	function form($instance) {

	// Check values
	if( $instance) {
	     $title = esc_attr($instance['title']);
	} else {
	     $title = '';
	}
	?>

	<p>
	<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', 'wp_widget_plugin'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
	</p>

	<?php
	}

	// update widget
	function update($new_instance, $old_instance) {
	      $instance = $old_instance;
	      // Fields
	      $instance['title'] = strip_tags($new_instance['title']);
	     return $instance;
	}

	// display widget
	function widget($args, $instance) {
		extract( $args );
		// these are the widget options
		$title = apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		// Display the widget

		// Check if title is set
		if ( $title ) {
		  echo $before_title . $title . $after_title;
		}

		$page = get_page_by_path('visitor-feedback');
	    if ($page)
	        $visitor_feedback_page_id = $page->ID;

		if (get_field('visitor_feedback_repeater', $visitor_feedback_page_id)): ?>
			<?php
			wp_register_script( 'turfjobs-unslider-js', MALINKY_VISITOR_FEEDBACK_WIDGET_URL . '/js/unslider.min.js', array( 'jquery' ), NULL, true );
			wp_enqueue_script( 'turfjobs-unslider-js' );
			?>
		    <script>
		    jQuery(document).ready(function($){
				$('#visitor_feedback_widget_carousel').unslider({
					delay: 6000,
					fluid: true
				});
		    });
		    </script>
			<div id="visitor_feedback_widget_carousel">
		        <ul class="list_no_style">
					<?php while(has_sub_field('visitor_feedback_repeater', $visitor_feedback_page_id)): ?>
						<li class="visitor_feedback_widget clearfix">
							<div class="clearfix">
							<?php $image = wp_get_attachment_image_src(get_sub_field('visitor_image'), 'vistor_feedback_sidebar'); ?>
							<img src="<?php echo esc_url( $image[0] ); ?>" alt="<?php echo (get_sub_field('visitor_name')) ?>" class="clearfix" />
							<?php the_sub_field('visitor_quote'); ?>
							</div>
							<p class="visitor_feedback_widget_name"><?php the_sub_field('visitor_name'); ?></p>
							<p class="visitor_feedback_widget_position"><?php the_sub_field('visitor_position'); ?></p>
						</li>
					<?php endwhile;	?>
				</ul>
			</div>		
			<a href="<?php echo esc_url( site_url( 'visitor-feedback' ) ); ?>" class="button">View All Feedback</a>
		<?php endif;

		echo $after_widget;
	}

}

// register widget
add_action('widgets_init', create_function('', 'return register_widget("Malinky_Visitor_Feedback_Widget");'));