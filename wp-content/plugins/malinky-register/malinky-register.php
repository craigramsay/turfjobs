<?php
/**
 * Plugin Name: Malinky Register
 * Plugin URI: 
 * Description: A brief description of the Plugin.
 * Version: 1.0
 * Author: Malinky
 * Author URI: http://www.malinkymedia.com
 * Copyright: 2013 Craig Ramsay
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

class Malinky_Register {

	function __construct()
	{
		define( 'MALINKY_REGISTER_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		define( 'MALINKY_REGISTER_PLUGIN_URL', plugins_url( basename( plugin_dir_path( __FILE__ ) ) ) );

		include( 'malinky-register-functions.php' );			
		include( 'includes/malinky-register-shortcodes.php' );

		add_action( 'wp_enqueue_scripts', array( $this, 'malinky_frontend_scripts' ) );
	}

	function malinky_frontend_scripts()
	{
		if (is_front_page() || is_page('jobs')) {
		wp_register_script( 'malinky_validate_js', MALINKY_REGISTER_PLUGIN_URL . '/js/jquery.validate.js', array( 'jquery' ), NULL, true );		
		wp_enqueue_script( 'malinky_validate_js' );			
		
		wp_register_script( 'malinky_email_newsletter_validate_js', MALINKY_REGISTER_PLUGIN_URL . '/js/email_newsletter_validate.js', array( 'jquery', 'malinky_validate_js' ), NULL, true );
		wp_enqueue_script( 'malinky_email_newsletter_validate_js' );
		}

		if ( tree() == 'employer' && !is_page('edit-profile') ) {
			wp_register_script( 'malinky_validate_js', MALINKY_REGISTER_PLUGIN_URL . '/js/jquery.validate.js', array( 'jquery' ), NULL, true );		
			wp_enqueue_script( 'malinky_validate_js' );

			wp_register_script( 'malinky_addjob_validate_js', MALINKY_REGISTER_PLUGIN_URL . '/js/addjob_validate.js', array( 'jquery', 'malinky_validate_js' ), NULL, true );
			wp_enqueue_script( 'malinky_addjob_validate_js' );

			wp_enqueue_script('jquery-ui-datepicker');
			$jquery_version = isset( $wp_scripts->registered['jquery-ui-core']->ver ) ? $wp_scripts->registered['jquery-ui-core']->ver : '1.9.2';
			wp_enqueue_style( 'jquery-ui-style', '//ajax.googleapis.com/ajax/libs/jqueryui/' . $jquery_version . '/themes/smoothness/jquery-ui.css' );			
		}			

		if ( is_page( 'register' ) || is_page('edit-profile') ) {
			wp_register_script( 'malinky_validate_js', MALINKY_REGISTER_PLUGIN_URL . '/js/jquery.validate.js', array( 'jquery' ), NULL, true );		
			wp_enqueue_script( 'malinky_validate_js' );			
			
			wp_register_script( 'malinky_registration_validate_js', MALINKY_REGISTER_PLUGIN_URL . '/js/registration_validate.js', array( 'jquery', 'malinky_validate_js' ), NULL, true );
			wp_enqueue_script( 'malinky_registration_validate_js' );
		}

	}

}

$malinky_register = new Malinky_Register();

?>