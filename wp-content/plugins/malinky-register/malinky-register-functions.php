<?php
/**
 * ----------------------------------------------------------------------------
 * REMOVE ADMIN BAR IN ACTION AS BETTER To CALL wp_get_current_user() IN ACTION
 * REMOVE FOR ALL BUT ADMINIISTRATORS
 * ----------------------------------------------------------------------------
 */
add_action('init','malinky_register_remove_admin_bar');
function malinky_register_remove_admin_bar()
{
	$current_user = wp_get_current_user();
	if (!in_array('administrator', (array) $current_user->roles))
		show_admin_bar(false);
}

/**
 * --------------------
 * REDIRECT AFTER LOGIN
 * --------------------
 */
add_filter('login_redirect','malinky_register_login_form_redirect', 10, 3);
function malinky_register_login_form_redirect($redirect_to, $request, $user)
{
    global $user;
    if(isset($user->roles) && is_array($user->roles)) {
        if(in_array('employer', (array) $user->roles )) {
            return site_url('employer');
        } else {
        	//doesn't work but should return to admin_url()
        	//return $redirect_to;
        	return admin_url();
        }
    }
}

/**
 * ----------------------------
 * REDIRECT THE REGISTER FORM
 * ----------------------------
 */
add_action('login_init','malinky_register_register_form_redirect');
function malinky_register_register_form_redirect()
{
	global $pagenow;
 	if ( ('wp-login.php' == $pagenow) && ($_GET['action'] == 'register') ) :
  		wp_safe_redirect(site_url('register'));
  		exit();
  	endif;
}

/**
 * --------------------------------------------------------------------------------------------------
 * REDIRECT THE PROFILE.PHP FORM FOR AN EMPLOYER
 * USED ADMIN_HEAD INSTEAD OF ADMIN_INIT BECAUSE OF PLUG IN CONFLICTS
 * --------------------------------------------------------------------------------------------------
 */
add_action('admin_head','malinky_register_profile_form_redirect');
function malinky_register_profile_form_redirect()
{
	global $pagenow;

	if (is_user_logged_in()) {
		global $current_user;
	    get_currentuserinfo();
		//$current_user = malinky_register_get_current_user();
	}

	//redirect employers away from any wp_admin pages
 	if (is_admin() && in_array('employer', (array) $current_user->roles)) :
 		if ( headers_sent() ) {
        	echo '<meta http-equiv="refresh" content="0;url=' . site_url('employer') . '">';
            echo '<script type="text/javascript">document.location.href="' . site_url('employer') . '"</script>';
        } else {
        	wp_safe_redirect(site_url('employer'));
            exit();
		}
  	endif;
  	//redirect when admin tries to edit an employer from wp-admin to custom form
 	/*if ('user-edit.php' == $pagenow) :
 		if ($_GET['user_id'] && is_numeric($_GET['user_id'])) {
 			$user_id = $_GET['user_id'];
  			wp_safe_redirect(site_url('employer/edit-profile?user_id=' . $user_id));

  			exit();
  		}
  	endif;*/
}

/**
 * -------------------------
 * REMOVE THE SHAKE ON LOGIN
 * -------------------------
 */
add_action('login_head', 'malinky_register_login_head');
function malinky_register_login_head() {
    remove_action('login_head', 'wp_shake_js', 12);
}

/**
 * -------------------------------------------------------
 * PROCESS THE CUSTOM REGISTRATION FORM AND ERROR CHECKING
 * -------------------------------------------------------
 */
function malinky_register_new_employer($organisation_type, $name_title, $first_name, $last_name, $phone_number, $user_login, $user_email, $password, $confirm_password, $company_name, $company_address, $company_town, $company_county, $company_postcode, $company_website, $company_twitter, $company_facebook, $company_googleplus, $company_logo, $register_tandc, $register_marketing_1)
{
	//check form submit and nonce
	if ( (empty($_POST['submit_registration'])) || (!isset($_POST['malinky_register_register_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_register_register_form_nonce'], 'malinky_register_register_form')) )
		wp_die(__('There was a fatal error with your form submission'));

	$errors = new WP_Error();
	$register_error_messages = malinky_register_error_messages();

	$sanitized_organisation_type 	= sanitize_text_field($organisation_type);
	$sanitized_name_title 			= sanitize_text_field($name_title);
	$sanitized_first_name 			= sanitize_text_field($first_name);
	$sanitized_last_name 			= sanitize_text_field($last_name);
	$sanitized_phone_number 		= sanitize_text_field($phone_number);	
	$sanitized_user_login 			= sanitize_email($user_login);
	$sanitized_user_email 			= apply_filters('user_registration_email', sanitize_email($user_email));
	$sanitized_company_name 		= sanitize_text_field($company_name);
	$sanitized_company_address 		= sanitize_text_field($company_address);
	$sanitized_company_town 		= sanitize_text_field($company_town);
	$sanitized_company_county 		= sanitize_text_field($company_county);
	$sanitized_company_postcode 	= sanitize_text_field($company_postcode);
	$company_website			 	= $company_website;
	$company_twitter 				= $company_twitter;
	$company_facebook 				= $company_facebook;
	$company_googleplus 			= $company_googleplus;	
	$company_logo 					= $company_logo;
	$register_tandc 				= $register_tandc;
	$register_marketing_1			= $register_marketing_1;

	//check the organisation_type
	if ($sanitized_organisation_type == 'please_choose') {
		$errors->add( 'organisation_type_error', __($register_error_messages['organisation_type_error']));
	} elseif(!is_numeric($sanitized_organisation_type)) {
		$errors->add( 'organisation_type_error', __($register_error_messages['organisation_type_error']));		
	}

	//check the name_title
	if ($sanitized_name_title == 'please_choose') {
		$errors->add( 'name_title_error', __($register_error_messages['name_title_error']));
	} elseif(!ctype_alpha($sanitized_name_title)) {
		$errors->add( 'name_title_error', __($register_error_messages['name_title_error']));		
	}	

	//check the first_name
	if ($sanitized_first_name == '') {
		$errors->add( 'first_name_error', __($register_error_messages['first_name_error']));
	}

	//check the last_name
	if ($sanitized_last_name == '') {
		$errors->add( 'last_name_error', __($register_error_messages['last_name_error']));
	}

	//check the phone_number
	if ($sanitized_phone_number == '') {
		$errors->add( 'phone_number_error', __($register_error_messages['phone_number_error']));
	} elseif (!preg_match('/[0-9 ]+$/', $sanitized_phone_number)) {		
		$errors->add( 'phone_number_error', __($register_error_messages['phone_number_error_2']));
	}		

	//check the user_login
	if ($sanitized_user_login == '') {
		$errors->add( 'user_login_error', __($register_error_messages['user_login_error']));
	} elseif (!is_email($sanitized_user_login)) {
		$errors->add('user_login_error', __($register_error_messages['user_login_error']));
	}

	//check the user_email
	if ($sanitized_user_email == '') {
		$errors->add( 'confirm_email_error', __($register_error_messages['confirm_email_error']));
	} elseif (!is_email($sanitized_user_email)) {
		$errors->add('confirm_email_error', __($register_error_messages['confirm_email_error']));
	}

	//check the user-login and user_email match
	if ( (is_email($sanitized_user_login)) && (is_email($sanitized_user_email)) && ($sanitized_user_login != $sanitized_user_email) ) {
		$errors->add('email_match_error', __($register_error_messages['email_match_error']));
	}

	//check the user-login or user_email don't exist
	if ( (is_email($sanitized_user_login)) && (is_email($sanitized_user_email)) && ($sanitized_user_login == $sanitized_user_email) ) {
		if ( username_exists($sanitized_user_login) || email_exists($sanitized_user_email) ) {
			$errors->add('email_exists', __($register_error_messages['email_exists']));
		}
	}

	//check the password
	if ($password == '') {
		$errors->add( 'password_error', __($register_error_messages['password_error']));
	}

	//check the confirm_password
	if ($confirm_password == '') {
		$errors->add( 'confirm_password_error', __($register_error_messages['confirm_password_error']));
	}

	//check the password and confirm_password fields match
	if ( ($password != '') && ($confirm_password != '') && ($password != $confirm_password) ) {
		$errors->add('password_match_error', __($register_error_messages['password_match_error']));
	}

	//check the password and confirm_password fields match
	if ( ($password != '') && ($confirm_password != '') && ($password == $confirm_password) ) {
		if ( !preg_match('/^(?=.*\d)(?=.*[A-Za-z])[a-zA-Z0-9]+$/', $password) || !preg_match('/^(?=.*\d)(?=.*[A-Za-z]).+$/', $confirm_password) )
		$errors->add('password_format_error', __($register_error_messages['password_format_error']));
	}	

	//check the company_name
	if ($sanitized_company_name == '') {
		$errors->add( 'company_name_error', __($register_error_messages['company_name_error']));
	}

	//check the company_address
	if ($sanitized_company_address == '') {
		$errors->add( 'company_address_error', __($register_error_messages['company_address_error']));
	}

	//check the company_town
	if ($sanitized_company_town == '') {
		$errors->add( 'company_town_error', __($register_error_messages['company_town_error']));
	}

	//check the company_county
	if ($sanitized_company_county == 'please_choose') {
		$errors->add( 'company_county_error', __($register_error_messages['company_county_error']));
	} elseif(!preg_match('/^[a-z\-]+$/', $sanitized_company_county)) {
		$errors->add( 'company_county_error', __($register_error_messages['company_county_error']));		
	}	

	//check the company_postcode
	if ($sanitized_company_county != 'co-carlow' && $sanitized_company_county != 'co-cavan' && $sanitized_company_county != 'co-clare' && $sanitized_company_county != 'co-cork' && $sanitized_company_county != 'co-donegal' && $sanitized_company_county != 'co-dublin' && $sanitized_company_county != 'co-galway' && $sanitized_company_county != 'co-kerry' && $sanitized_company_county != 'co-kildare' && $sanitized_company_county != 'co-kilkenny' && $sanitized_company_county != 'co-laois' && $sanitized_company_county != 'co-leitrim'&& $sanitized_company_county != 'co-limerick' && $sanitized_company_county != 'co-longford' && $sanitized_company_county != 'co-louth' && $sanitized_company_county != 'co-mayo' && $sanitized_company_county != 'co-meath' && $sanitized_company_county != 'co-monaghan' && $sanitized_company_county != 'co-offaly' && $sanitized_company_county != 'co-roscommon' && $sanitized_company_county != 'co-sligo' && $sanitized_company_county != 'co-tipperary' && $sanitized_company_county != 'co-waterford' && $sanitized_company_county != 'co-westmeath' && $sanitized_company_county != 'co-wexford' && $sanitized_company_county != 'co-wicklow' && $sanitized_company_county != 'europe') {
		if ($sanitized_company_postcode == '') {
			$errors->add( 'company_postcode_error', __($register_error_messages['company_postcode_error']));
		} elseif (!preg_match("~^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$~i", $sanitized_company_postcode)) {		
			$errors->add( 'company_postcode_error', __($register_error_messages['company_postcode_error_2']));
		}			
	}

	//check the company_website
	if ($company_website != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $company_website)) {
		$errors->add( 'company_website_error', __($register_error_messages['company_website_error']));
	}

	//check the company_twitter
	if ($company_twitter != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $company_twitter)) {
		$errors->add( 'company_twitter_error', __($register_error_messages['company_twitter_error']));
	}

	//check the company_facebook
	if ($company_facebook != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $company_facebook)) {
		$errors->add( 'company_facebook_error', __($register_error_messages['company_facebook_error']));
	}

	//check the company_googleplus
	if ($company_googleplus != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $company_googleplus)) {
		$errors->add( 'company_googleplus_error', __($register_error_messages['company_googleplus_error']));
	}	

	if ($company_logo['size'] == 0) {
		$errors->add( 'company_logo_error', __($register_error_messages['company_logo_error']));
	}

	//check if register tandc 
	if ($register_tandc != "1") {
		$errors->add( 'register_tandc_error', __($register_error_messages['register_tandc_error']));
	}

	//check if register marketing 1
	if ($register_marketing_1 != "" && $register_marketing_1 != "1") {
		$errors->add( 'register_marketing_1_error', __($register_error_messages['register_marketing_1_error']));
	}		
	
	do_action( 'register_post', $sanitized_user_login, $sanitized_user_email, $errors );
	$errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $sanitized_user_email );
	//if validation errors
	if ( $errors->get_error_code() )
		return $errors;

	//upload the logo first
	$company_logo_url = upload_image('company_logo');
	if ( is_object($company_logo_url) )
		return $company_logo_url;

	//no errors create the user
	$new_user = array(
		'user_login'	=> $sanitized_user_login,
		'user_pass'		=> $password,
		'user_email'	=> $sanitized_user_email,
		'first_name'	=> $sanitized_first_name,
		'last_name'		=> $sanitized_last_name
	);

	$user_id = wp_insert_user($new_user);

	//if new user isn't created send error at bottom of registration form
	if (!$user_id || is_wp_error($user_id)) {
		$errors->add('register_fail', __('There was a problem with your registration. Please try again.'));
		return $errors;
	}

	//generate email validation code
	$email_validation_code = malinky_register_generate_validation_code($sanitized_user_email);

	//must have new user so lets insert custom meta values
	$custom_usermeta = array(
		'_validation_code'		=> $email_validation_code,
		'_validated_user' 		=> 'false',
		'_organisation_type'	=> $sanitized_organisation_type,
		'_name_title'			=> $sanitized_name_title,
		'_company_phone_number'	=> $sanitized_phone_number,
		'_company_name' 		=> $sanitized_company_name,	
		'_company_address'		=> $sanitized_company_address,
		'_company_town'			=> $sanitized_company_town,
		'_company_county'		=> $sanitized_company_county,
		'_company_postcode'		=> $sanitized_company_postcode,
		'_company_website'		=> $company_website,
		'_company_twitter'		=> $company_twitter,
		'_company_facebook'		=> $company_facebook,
		'_company_googleplus'	=> $company_googleplus,
		'_company_logo'			=> $company_logo_url,
		'_accept_tandcs'		=> $register_tandc,
		'_accept_marketing_1'	=> $register_marketing_1		
	);

	foreach ($custom_usermeta as $k => $v) :
		if ($custom_usermeta[$k]) :
			update_user_meta($user_id, $k, $v);	
		endif;
	endforeach;

	//send registration email
	malinky_register_send_registration_email($sanitized_user_email, $email_validation_code);

	if ($user_id) {
		wp_safe_redirect('wp-login.php?action=registered');
		exit();
	}
}

/**
 * -------------------------------------------------------
 * PROCESS THE CUSTOM PROFILE FORM AND ERROR CHECKING
 * -------------------------------------------------------
 */
function malinky_register_update_employer($organisation_type, $name_title, $first_name, $last_name, $phone_number, $user_login, $user_email, $password, $confirm_password, $company_name, $company_address, $company_town, $company_county, $company_postcode, $company_website, $company_twitter, $company_facebook, $company_googleplus, $company_logo, $register_marketing_1, $profile_user)
{
	//check form submit and nonce
	if ( (empty($_POST['update_profile'])) || (!isset($_POST['malinky_register_profile_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_register_profile_form_nonce'], 'malinky_register_profile_form')) )
		wp_die(__('There was a fatal error with your form submission'));

	$errors = new WP_Error();
	$register_error_messages = malinky_register_error_messages();

	$sanitized_organisation_type 	= sanitize_text_field($organisation_type);
	$sanitized_name_title 			= sanitize_text_field($name_title);
	$sanitized_first_name 			= sanitize_text_field($first_name);
	$sanitized_last_name 			= sanitize_text_field($last_name);
	$sanitized_phone_number 		= sanitize_text_field($phone_number);	
	$sanitized_user_login 			= sanitize_email($user_login);
	$sanitized_user_email 			= apply_filters('user_registration_email', sanitize_email($user_email));
	$sanitized_company_name 		= sanitize_text_field($company_name);
	$sanitized_company_address 		= sanitize_text_field($company_address);
	$sanitized_company_town 		= sanitize_text_field($company_town);
	$sanitized_company_county 		= sanitize_text_field($company_county);
	$sanitized_company_postcode 	= sanitize_text_field($company_postcode);
	$company_website			 	= $company_website;
	$company_twitter 				= $company_twitter;
	$company_facebook 				= $company_facebook;
	$company_googleplus 			= $company_googleplus;
	$company_logo					= $company_logo;
	$register_marketing_1			= $register_marketing_1;						

	//check the organisation_type
	if ($sanitized_organisation_type == 'please_choose') {
		$errors->add( 'organisation_type_error', __($register_error_messages['organisation_type_error']));
	} elseif(!is_numeric($sanitized_organisation_type)) {
		$errors->add( 'organisation_type_error', __($register_error_messages['organisation_type_error']));		
	}

	//check the name_title
	if ($sanitized_name_title == 'please_choose') {
		$errors->add( 'name_title_error', __($register_error_messages['name_title_error']));
	} elseif(!ctype_alpha($sanitized_name_title)) {
		$errors->add( 'name_title_error', __($register_error_messages['name_title_error']));		
	}	

	//check the first_name
	if ($sanitized_first_name == '') {
		$errors->add( 'first_name_error', __($register_error_messages['first_name_error']));
	}

	//check the last_name
	if ($sanitized_last_name == '') {
		$errors->add( 'last_name_error', __($register_error_messages['last_name_error']));
	}

	//check the phone_number
	if ($sanitized_phone_number == '') {
		$errors->add( 'phone_number_error', __($register_error_messages['phone_number_error']));
	} elseif (!preg_match('/[0-9 ]+$/', $sanitized_phone_number)) {		
		$errors->add( 'phone_number_error', __($register_error_messages['phone_number_error_2']));
	}		

	//check the user_login
	if ($sanitized_user_login == '') {
		$errors->add( 'user_login_error', __($register_error_messages['user_login_error']));
	} elseif (!is_email($sanitized_user_login)) {
		$errors->add('user_login_error', __($register_error_messages['user_login_error']));
	}

	//check the user_email
	if ($sanitized_user_email == '') {
		$errors->add( 'confirm_email_error', __($register_error_messages['confirm_email_error']));
	} elseif (!is_email($sanitized_user_email)) {
		$errors->add('confirm_email_error', __($register_error_messages['confirm_email_error']));
	}

	//check the user-login and user_email match
	if ( (is_email($sanitized_user_login)) && (is_email($sanitized_user_email)) && ($sanitized_user_login != $sanitized_user_email) ) {
		$errors->add('email_match_error', __($register_error_messages['email_match_error']));
	}

	//check the user-login or user_email don't exist
	if ( (is_email($sanitized_user_login)) && (is_email($sanitized_user_email)) && ($sanitized_user_login == $sanitized_user_email) && ($sanitized_user_login != $profile_user->user_login) ) {
		if ( username_exists($sanitized_user_login) || email_exists($sanitized_user_email) ) {
			$errors->add('email_exists', __($register_error_messages['email_exists']));
		}
	}

	//check the password IT CAN LEFT EMPTY WHEN UPDATING
	//if ($password == '') {
	//	$errors->add( 'password_error', __($register_error_messages['password_error']));
	//}

	//check the confirm_password IT CAN LEFT EMPTY WHEN UPDATING
	//if ($confirm_password == '') {
	//	$errors->add( 'confirm_password_error', __($register_error_messages['confirm_password_error']));
	//}

	//check the password and confirm_password fields match
	if ( ($update_password != '') && ($update_confirm_password != '') && ($update_password != $update_confirm_password) ) {
		$errors->add('password_match_error', __($register_error_messages['password_match_error']));
	}

	//check the password and confirm_password fields match
	if ( ($update_password != '') && ($update_confirm_password != '') && ($update_password == $update_confirm_password) ) {
		if ( !preg_match('/^(?=.*\d)(?=.*[A-Za-z])[a-zA-Z0-9]+$/', $update_password) || !preg_match('/^(?=.*\d)(?=.*[A-Za-z]).+$/', $update_confirm_password) )
		$errors->add('password_format_error', __($register_error_messages['password_format_error']));
	}	

	//check the company_name
	if ($sanitized_company_name == '') {
		$errors->add( 'company_name_error', __($register_error_messages['company_name_error']));
	}

	//check the company_address
	if ($sanitized_company_address == '') {
		$errors->add( 'company_address_error', __($register_error_messages['company_address_error']));
	}

	//check the company_town
	if ($sanitized_company_town == '') {
		$errors->add( 'company_town_error', __($register_error_messages['company_town_error']));
	}

	//check the company_county
	if ($sanitized_company_county == 'please_choose') {
		$errors->add( 'company_county_error', __($register_error_messages['company_county_error']));
	} elseif(!preg_match('/^[a-z\-]+$/', $sanitized_company_county)) {
		$errors->add( 'company_county_error', __($register_error_messages['company_county_error']));		
	}	

	//check the company_postcode
	if ($sanitized_company_county != 'co-carlow' && $sanitized_company_county != 'co-cavan' && $sanitized_company_county != 'co-clare' && $sanitized_company_county != 'co-cork' && $sanitized_company_county != 'co-donegal' && $sanitized_company_county != 'co-dublin' && $sanitized_company_county != 'co-galway' && $sanitized_company_county != 'co-kerry' && $sanitized_company_county != 'co-kildare' && $sanitized_company_county != 'co-kilkenny' && $sanitized_company_county != 'co-laois' && $sanitized_company_county != 'co-leitrim'&& $sanitized_company_county != 'co-limerick' && $sanitized_company_county != 'co-longford' && $sanitized_company_county != 'co-louth' && $sanitized_company_county != 'co-mayo' && $sanitized_company_county != 'co-meath' && $sanitized_company_county != 'co-monaghan' && $sanitized_company_county != 'co-offaly' && $sanitized_company_county != 'co-roscommon' && $sanitized_company_county != 'co-sligo' && $sanitized_company_county != 'co-tipperary' && $sanitized_company_county != 'co-waterford' && $sanitized_company_county != 'co-westmeath' && $sanitized_company_county != 'co-wexford' && $sanitized_company_county != 'co-wicklow' && $sanitized_company_county != 'europe') {
		if ($sanitized_company_postcode == '') {
			$errors->add( 'company_postcode_error', __($register_error_messages['company_postcode_error']));
		} elseif (!preg_match("~^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$~i", $sanitized_company_postcode)) {		
			$errors->add( 'company_postcode_error', __($register_error_messages['company_postcode_error_2']));
		}			
	}

	//check the company_website
	if ($company_website != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $company_website)) {
		$errors->add( 'company_website_error', __($register_error_messages['company_website_error']));
	}

	//check the company_twitter
	if ($company_twitter != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $company_twitter)) {
		$errors->add( 'company_twitter_error', __($register_error_messages['company_twitter_error']));
	}

	//check the company_facebook
	if ($company_facebook != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $company_facebook)) {
		$errors->add( 'company_facebook_error', __($register_error_messages['company_facebook_error']));
	}

	//check the company_googleplus
	if ($company_googleplus != '' && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $company_googleplus)) {
		$errors->add( 'company_googleplus_error', __($register_error_messages['company_googleplus_error']));
	}	

	//check if register marketing 1
	if ($register_marketing_1 != "" && $register_marketing_1 != "1") {
		$errors->add( 'register_marketing_1_error', __($register_error_messages['register_marketing_1_error']));
	}				

	//do_action( 'register_post', $sanitized_user_login, $sanitized_user_email, $errors );
	//$errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $sanitized_user_email );
	//if validation errors
	if ( $errors->get_error_code() )
		return $errors;

	//upload the logo first if there is one and update logo meta data
	$company_logo_url = upload_image('update_company_logo');
	//echo $company_logo_url;
	if ( !is_object($company_logo_url) && $company_logo_url != '' ) {
		update_user_meta($profile_user->ID, '_company_logo', $company_logo_url);
	} elseif (is_object($company_logo_url)) {
		return $company_logo_url;
	}

	//no errors update the user
	$user_data = array(
		'ID'			=> $profile_user->ID,
		'first_name'	=> $sanitized_first_name,
		'last_name'		=> $sanitized_last_name
	);

	//check for a password change
	if ($password != '')
		$user_data['user_pass'] = $password;
	
	//check for a user_login change and update database manually
	if ($sanitized_user_login != $profile_user->user_login) {
		global $wpdb;
		$user_login_db_change = $wpdb->update($wpdb->users, array('user_login' => $sanitized_user_login), array('ID' => $profile_user->ID));
		if (!$user_login_db_change) {
			$errors->add('register_fail', __('There was a problem updating your profile. Please try again.'));
		return $errors;
	}
		$user_data['user_email'] = $sanitized_user_email;
	}

	$user_id = wp_update_user($user_data);

	//if user isn't updated send error at bottom of registration form
	if (!$user_id || is_wp_error($user_id)) {
		$errors->add('register_fail', __('There was a problem updating your profile. Please try again.'));
		return $errors;
	}

	//must have new user so lets insert custom meta values
	$custom_usermeta = array(
		'_organisation_type'	=> $sanitized_organisation_type,
		'_name_title'			=> $sanitized_name_title,
		'_company_phone_number'	=> $sanitized_phone_number,
		'_company_name' 		=> $sanitized_company_name,	
		'_company_address'		=> $sanitized_company_address,
		'_company_town'			=> $sanitized_company_town,
		'_company_county'		=> $sanitized_company_county,
		'_company_postcode'		=> $sanitized_company_postcode,
		'_company_website'		=> $company_website,
		'_company_twitter'		=> $company_twitter,
		'_company_facebook'		=> $company_facebook,
		'_company_googleplus'	=> $company_googleplus,
		'_accept_marketing_1'	=> $register_marketing_1			
	);

	foreach ($custom_usermeta as $k => $v) :
		if ($custom_usermeta[$k]) :
			update_user_meta($user_id, $k, $v);	
		endif;
	endforeach;

	if (empty($register_marketing_1))
		update_user_meta( $user_id, '_accept_marketing_1', 0 );

	//update all postmeta organisation types incase it has changed. even though this is a custom taxonomy it is user based
	//as such it isn't stored and update using wp_get_terms like a taxonomy would be
	$args = array(
		'post_type'		=> 'job_listing',
		'post_status'	=> 'any',
		'author' 		=> $user_id
	);
	$user_jobs = get_posts($args);
	foreach($user_jobs as $k => $v) {
		update_post_meta( $user_jobs[$k]->ID, '_job_organisation_type', $custom_usermeta['_organisation_type'] );
	}

	if ($user_id) {
		wp_safe_redirect('employer?action=profile_updated');
		exit();
	}
		
}

/**
 * ------------------------
 * REGISTRATION FORM ERRORS
 * ------------------------
 */
function malinky_register_error_messages()
{
	return array(
		'organisation_type_error'		=> 'Please choose an organisation type.',
		'name_title_error'				=> 'Please choose a title.',
		'first_name_error'				=> 'Please enter your first name.',
		'last_name_error'				=> 'Please enter your surname.',
		'phone_number_error'			=> 'Please enter your phone number.',
		'phone_number_error_2'			=> 'Please enter a valid phone number.',
		'user_login_error'				=> 'Please enter a valid email address.',
		'confirm_email_error'			=> 'Please enter a valid email address.',
		'email_match_error'				=> 'Please ensure your email addresses match.',
		'email_exists'					=> 'The email address already exists. Please enter another.',
		'password_error'				=> 'Please enter a password.',
		'confirm_password_error'		=> 'Please confirm your password.',
		'password_match_error'			=> 'Please ensure your passwords match.',
		'password_format_error'			=> 'Your password must contain atleast one letter and one number and no special characters.',
		'company_name_error'			=> 'Please enter your company name.',
		'company_address_error'			=> 'Please enter your company address.',
		'company_town_error'			=> 'Please enter your company town.',
		'company_county_error'			=> 'Please choose a county.',
		'company_postcode_error'		=> 'Please enter your company postcode.',
		'company_postcode_error_2'		=> 'Please enter a valid postcode.',	
		'company_website_error'			=> 'Please enter a valid company website URL.',
		'company_twitter_error'			=> 'Please enter a valid company Twitter URL.',
		'company_facebook_error'		=> 'Please enter a valid company Facebook URL.',
		'company_googleplus_error'		=> 'Please enter a valid company Google Plus URL.',
		'company_logo_error'			=> 'Please upload a company logo.',
		'company_logo_error2'			=> 'There was a problem uploading your logo.',
		'company_logo_error3'			=> 'Please upload a .jpg, .png or .gif company logo.',
		'company_logo_error4'			=> 'Your company logo has exceeded the maximum file size of 100kb.',
		'register_tandc_error'			=> 'Please accept our terms and conditions.',
		'register_marketing_1_error'	=> 'Invalid checkbox value.',
		'register_marketing_2_error'	=> 'Invalid checkbox value.'
	);
}

/**
 * ----------------------------------
 * GENERATE THE VALIDATION EMAIL CODE
 * ----------------------------------
 */
function malinky_register_generate_validation_code($code)
{
	return md5($code);
}

/**
 * ----------------------------------------------------
 * SEND THE REGISTRATION EMAIL AND VALIDATION CODE LINK
 * ----------------------------------------------------
 */
function malinky_register_send_registration_email($email, $validation_code)
{
	$subject = 'Please Activate Your Turf Jobs Account';
	$message = __('Thank you for registering with Turf Jobs. Please click on the link below to activate your account.') . "\r\n\r\n";
	$message .= '<' . network_site_url('wp-login.php?action=validate_user&validation_code=' . $validation_code) . '>' . "\r\n\r\n";
	$message .= __('Thanks') . "\r\n";

	wp_mail($email, $subject, $message);
}

/**
 * ------------------------------------------------------
 * RESEND THE REGISTRATION EMAIL AND VALIDATION CODE LINK
 * ------------------------------------------------------
 */
add_action('login_init', 'malinky_register_resend_registration_email');
function malinky_register_resend_registration_email()
{
	if ( $_GET['action'] == 'resend_registration_email' && is_email($_GET['email']) ) :
		global $wpdb;
		$email = $_GET['email'];
		$user_id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->users WHERE user_login = %s", $email));
		$email_validation_code = $wpdb->get_var($wpdb->prepare("SELECT meta_value FROM $wpdb->usermeta WHERE user_id = %d AND meta_key = '_validation_code'", $user_id));
		malinky_register_send_registration_email($email, $email_validation_code);
		wp_safe_redirect('?action=resent_registration_email');
		exit();
	endif;
}

/**
 * -----------------------------------------------------
 * SETUP THE REGISTRATION EMAIL FROM TO AND CONTENT TYPE
 * -----------------------------------------------------
 */
add_filter('wp_mail_from', 'set_email_from_email_address');
function set_email_from_email_address()
{
	return 'enquiries@turfjobs.co.uk';
}

add_filter('wp_mail_from_name', 'set_email_from_name');
function set_email_from_name()
{
	return 'Turf Jobs';
}

/**
 * ---------------------------------------------
 * VALIDATE THE USER FROM THE REGISTRATION EMAIL
 * ---------------------------------------------
 */
add_action('login_init', 'malinky_register_validate_user');
function malinky_register_validate_user()
{
	if ( (basename($_SERVER['PHP_SELF']) == 'wp-login.php') && ($_GET['action'] == 'validate_user') && (isset($_GET['validation_code'])) ) :
		$user_id = malinky_register_check_validation_code($_GET['validation_code']);
		if ($user_id) :
			update_user_meta($user_id, '_validated_user', 'true');			
			delete_user_meta($user_id, '_validation_code');
			wp_safe_redirect('?action=user_validated');
			exit();
		else :
			wp_safe_redirect('?action=user_validation_failed');
			exit();
		endif;
	endif;
}

function malinky_register_check_validation_code($validation_code)
{
	global $wpdb;

	if (!preg_match('/[a-zA-Z0-9]{32}/', $validation_code))
		return false;

	$user_id = $wpdb->get_var($wpdb->prepare("SELECT user_id FROM $wpdb->usermeta WHERE meta_key = '_validation_code' AND meta_value = %s;", $validation_code ) );
	
	if (empty($user_id))
		return false;

	return $user_id;
}

/**
 * ------------------------------------
 * SETUP THE REGISTRATION PAGE MESSAGES
 * ------------------------------------
 */
if ( (basename($_SERVER['PHP_SELF']) == 'wp-login.php' && isset($_GET['action'])) && ( ($_GET['action'] == 'registered') || ($_GET['action'] == 'resent_registration_email') || ($_GET['action'] == 'user_validated') || ($_GET['action'] == 'user_validation_failed') ) ) :
	add_filter('login_message', 'malinky_register_login_messages');
	function malinky_register_login_messages($messages)
	{
		//change to switch with default message
		if (empty($messages)) {
			if ($_GET['action'] == 'registered') {
				return '<p class="message">You have now been registered. Please check your email to activate your account.</p>';
			} elseif ($_GET['action'] == 'resent_registration_email') {
				return '<p class="message">Your activation email has been resent.</p>';
			} elseif ($_GET['action'] == 'user_validated') {
				return '<p class="message">Your account has now been activated. Please login.</p>';
			} elseif ($_GET['action'] == 'user_validation_failed') {
	        	return '<div id="login_error">Your account activation has failed.</div>';
			}
	    } else {
	        return $messages;
	    }
	}
endif;

/**
 * --------------------------------------------------------------------------------------
 * BLOCK UNVALIDATED USERS FROM LOGGING IN AND ENCOURAGE THE RESENDING OF VALIDATION EMAIL
 * --------------------------------------------------------------------------------------
 */
add_filter('wp_authenticate_user', 'malinky_register_is_validated_user', 10, 2);
function malinky_register_is_validated_user($user, $password)
{	
	$is_validated_user = get_user_meta($user->ID, '_validated_user', true);
	if ($is_validated_user == 'false') :
		return new WP_Error('no_validated_user', __('You need to validate your account before logging in. Please click the link below to resend the validation email.<br /><br /><a href="' . site_url('/wp-login.php?action=resend_registration_email&email=' . $user->user_login) . '">Resend Validation Email</a>'));
	else :
		return $user;
	endif;
}

/**
 * ---------------------------------------------------------------------------------------------------
 * REMOVE CUSTOM TAXONOMY ORGANSATION TYPE FROM A CREATE JOB LISTING POSTS PAGE WITHIN WORDPRESS ADMIN
 * ---------------------------------------------------------------------------------------------------
 */
add_action( 'admin_menu' , 'remove_job_listing_organisation_type_meta' );
function remove_job_listing_organisation_type_meta() {
	remove_meta_box( 'job_listing_organisation_typediv', 'job_listing', 'side' );
}

/**
 * ----------------------------------------------------------------
 * GET AN ARRAY OF TAXONOMY TERMS RETURNS TERM ID AND FRIENDLY NAME
 * ----------------------------------------------------------------
 */
function malinky_register_get_single_taxonomy_terms($taxonomy_name, array $args = null)
{
	$get_terms = get_terms($taxonomy_name, $args);
	$count = count($get_terms);
	if ($count > 0) :
		foreach ( $get_terms as $term ) :
			$terms[$term->term_id] = $term->name;
		endforeach;
	endif;
	return $terms;
}

/**
 * ----------------------
 * CREATE A FORM DROPDOWN
 * ----------------------
 */
function malinky_register_register_dropdown ($name, array $options, $selected = null)
{
    $dropdown = '<select name="' . $name . '" id="' . $name . '">' . "\n";
    $selected = $selected;
    $dropdown .= '<option value="please_choose">Please Choose</option>' . "\n";
    foreach( $options as $k => $option )
    {
        $select = $selected == $k ? ' selected' : null;
        $dropdown .= '<option value="' . esc_attr(wp_unslash($k)) . '"' . $select. '>' . esc_html($option) . '</option>' . "\n";
    }
    $dropdown .= '</select>' . "\n";
    return $dropdown;
}

/**
 * ----------------------
 * CREATE A FORM DROPDOWN
 * ----------------------
 */
function malinky_register_register_dropdown_county ($name, array $options, $selected = null)
{
    $dropdown = '<select name="' . $name . '" id="' . $name . '">' . "\n";
    $selected = $selected;
    $dropdown .= '<option value="please_choose">Please Choose</option>' . "\n";
    foreach( $options as $k => $option )
    {
    	$dropdown .= '<optgroup label="' . ucwords(str_replace('-', ' ', $k)) . '">';    	
	    foreach( $option as $county_label => $county_name )
	    {
	        $select = $selected == $county_label ? ' selected' : null;
	        $dropdown .= '<option value="' . esc_attr(wp_unslash($county_label)) . '"' . $select. '>' . esc_html($county_name) . '</option>' . "\n";
	    }
    	$dropdown .= '</optgroup>';
    }
    $dropdown .= '</select>' . "\n";
    return $dropdown;
}

/**
 * --------------------------------------
 * LIST OF COUNTIES FOR USER REGISTRATION
 * --------------------------------------
 */
function malinky_register_get_counties()
{
	$counties = array(
		'england' => array(
			'bedfordshire' => 'Bedfordshire',
			'berkshire' => 'Berkshire',
			'berwickshire' => 'Berwickshire',
			'bristol' => 'Bristol',
			'buckinghamshire' => 'Buckinghamshire',
			'cambridgeshire' => 'Cambridgeshire',
			'cheshire' => 'Cheshire',
			'city-of-london' => 'City of London',
			'cornwall' => 'Cornwall',
			'cumbria' => 'Cumbria',
			'derbyshire' => 'Derbyshire',
			'devon' => 'Devon',
			'dorset' => 'Dorset',
			'down' => 'Down',
			'durham' => 'Durham',
			'east-riding-of-yorkshire' => 'East Riding of Yorkshire',
			'east-sussex' => 'East Sussex',
			'essex' => 'Essex',
			'gloucestershire' => 'Gloucestershire',
			'greater-london' => 'Greater London',
			'greater-manchester' => 'Greater Manchester',
			'hampshire' => 'Hampshire',
			'herefordshire' => 'Herefordshire',
			'hertfordshire' => 'Hertfordshire',
			'isle-of-wight' => 'Isle of Wight',
			'kent' => 'Kent',
			'lancashire' => 'Lancashire',
			'leicestershire' => 'Leicestershire',
			'lincolnshire' => 'Lincolnshire',
			'merseyside' => 'Merseyside',
			'norfolk' => 'Norfolk',
			'north-yorkshire' => 'North Yorkshire',
			'northamptonshire' => 'Northamptonshire',
			'northumberland' => 'Northumberland',
			'nottinghamshire' => 'Nottinghamshire',
			'oxfordshire' => 'Oxfordshire',
			'rutland' => 'Rutland',
			'shropshire' => 'Shropshire',
			'somerset' => 'Somerset',
			'south-yorkshire' => 'South Yorkshire',
			'staffordshire' => 'Staffordshire',
			'suffolk' => 'Suffolk',
			'surrey' => 'Surrey',
			'tyne-and-wear' => 'Tyne and Wear',
			'warwickshire' => 'Warwickshire',
			'west-midlands' => 'West Midlands',
			'west-sussex' => 'West Sussex',
			'west-yorkshire' => 'West Yorkshire',
			'wiltshire' => 'Wiltshire',
			'worcestershire' => 'Worcestershire'
		),
		'ireland' => array(
			'co-carlow' => 'Co. Carlow',
			'co-cavan' => 'Co. Cavan',
			'co-clare' => 'Co. Clare',
			'co-cork' => 'Co. Cork',
			'co-donegal' => 'Co. Donegal',
			'co-dublin' => 'Co. Dublin',
			'co-galway' => 'Co. Galway',
			'co-kerry' => 'Co. Kerry',
			'co-kildare' => 'Co. Kildare',
			'co-kilkenny' => 'Co. Kilkenny',
			'co-laois' => 'Co. Laois',
			'co-leitrim' => 'Co. Leitrim',
			'co-limerick' => 'Co. Limerick',
			'co-longford' => 'Co. Longford',
			'co-louth' => 'Co. Louth',
			'co-mayo' => 'Co. Mayo',
			'co-meath' => 'Co. Meath',
			'co-monaghan' => 'Co. Monaghan',
			'co-offaly' => 'Co. Offaly',
			'co-roscommon' => 'Co. Roscommon',
			'co-sligo' => 'Co. Sligo',
			'co-tipperary' => 'Co. Tipperary',
			'co-waterford' => 'Co. Waterford',
			'co-westmeath' => 'Co. Westmeath',
			'co-wexford' => 'Co. Wexford',
			'co-wicklow' => 'Co. Wicklow',
		),
		'nothern-ireland' => array(
			'co-antrim' => 'Co. Antrim',
			'co-armagh' => 'Co. Armagh',
			'co-down' => 'Co. Down',
			'co-fermanagh' => 'Co. Fermanagh',
			'co-londonderry' => 'Co. Londonderry',
			'co-tyrone' => 'Co. Tyrone',
		),
		'scotland' => array(
			'aberdeenshire' => 'Aberdeenshire',
			'angus' => 'Angus',
			'argyll-and-bute' => 'Argyll and Bute',
			'ayrshire-and-arran' => 'Ayrshire and Arran',
			'banffshire' => 'Banffshire',
			'caithness' => 'Caithness',
			'dumfries' => 'Dumfries',
			'dunbartonshire' => 'Dunbartonshire',
			'east-lothian' => 'East Lothian',
			'edingburgh' => 'Edinburgh',
			'fife' => 'Fife',
			'glasgow' => 'Glasgow',
			'inverness' => 'Inverness',
			'kincardineshire' => 'Kincardineshire',
			'midlothian' => 'Midlothian',
			'moray' => 'Moray',
			'north-lanarkshire' => 'North Lanarkshire',
			'orkney' => 'Orkney',
			'perth-and-kinross' => 'Perth and Kinross',
			'renfrewshire' => 'Renfrewshire',
			'ross-and-cromarty' => 'Ross and Cromarty',
			'shetland' => 'Shetland',
			'stirling-and-falkirk' => 'Stirling and Falkirk',
			'sutherland' => 'Sutherland',
			'tayside' => 'Tayside',
			'the-stewartry-of-kirkcudbright' => 'The Stewartry of Kirkcudbright',
			'west-lothian' => 'West Lothian',
			'western-isles' => 'Western Isles',			
		),
		'wales' => array(
			'clwyd' => 'Clwyd',
			'dyfed' => 'Dyfed',
			'gwent' => 'Gwent',
			'gwynedd' => 'Gwynedd',
			'mid-glamorgan' => 'Mid Glamorgan',
			'powys' => 'Powys',
			'south-glamorgan' => 'South Glamorgan',
			'west-glamorgan' => 'West Glamorgan',
		),
		'europe' => array(
			'europe' => 'Europe'
		)		
	);
	return $counties;
}

/**
 * --------------------------------------------------
 * PROCESS THE CUSTOM CONTACT FORM AND ERROR CHECKING
 * --------------------------------------------------
 */
/*function malinky_register_contact($first_name, $phone_number, $email, $comments)
{
	//check form submit and nonce
	if ( (empty($_POST['submit_contact'])) || (!isset($_POST['malinky_register_contact_form_nonce'])) || (!wp_verify_nonce($_POST['malinky_register_contact_form_nonce'], 'malinky_register_contact_form')) )
		wp_die(__('There was a fatal error with your form submission'));

	$errors = new WP_Error();
	$contact_error_messages = malinky_contact_error_messages();

	$sanitized_first_name 			= sanitize_text_field($first_name);
	$sanitized_phone_number 		= sanitize_text_field($phone_number);	
	$sanitized_email 				= sanitize_email($email);
	$sanitized_comments 			= sanitize_text_field($comments);		

	//check the first_name
	if ($sanitized_first_name == '') {
		$errors->add( 'first_name_error', __($contact_error_messages['first_name_error']));
	}

	//check the phone_number
	if ($sanitized_phone_number != '') {
		if (!preg_match('/[0-9 ]+$/', $sanitized_phone_number)) {		
			$errors->add( 'phone_number_error', __($contact_error_messages['phone_number_error_2']));
		}
	}	

	//check the email
	if ($sanitized_email == '') {
		$errors->add( 'email_error', __($contact_error_messages['email_error']));
	} elseif (!is_email($sanitized_email)) {
		$errors->add('email_error', __($contact_error_messages['email_error']));
	}

	//check the comments
	if ($sanitized_comments == '') {
		$errors->add( 'comments_error', __($contact_error_messages['comments_error']));
	}

	//if validation errors
	if ( $errors->get_error_code() )
		return $errors;

	//no errors send email

	//send registration email
	malinky_register_contact_email($sanitized_first_name, $sanitized_phone_number, $sanitized_email, $sanitized_comments);

	wp_safe_redirect('contacted');
	exit();
}*/

/**
 * -------------------
 * CONTACT FORM ERRORS
 * -------------------
 */
/*function malinky_contact_error_messages()
{
	return array(
		'first_name_error'			=> 'Please enter your first name.',
		'phone_number_error'		=> 'Please enter your phone number.',
		'phone_number_error_2'		=> 'Please enter a valid phone number.',
		'email_error'				=> 'Please enter a valid email address.',
		'comments_error'			=> 'Please enter a comment.',
	);
}*/

/**
 * ----------------------
 * SEND THE CONTACT EMAIL
 * ----------------------
 */
/*function malinky_register_contact_email($sanitized_first_name, $sanitized_phone_number, $sanitized_email, $sanitized_comments)
{
	$email = get_option( 'admin_email' );
	$subject = 'Turfjobs Website Contact Form Message';
	$message = __('Name: ' . $sanitized_first_name) . "\r\n\r\n";
	$message .= __('Telephone: ' . $sanitized_first_name) . "\r\n\r\n";
	$message .= __('Email: ' . $sanitized_email) . "\r\n\r\n";
	$message .= __('Comments: ' . $sanitized_comments) . "\r\n";

	wp_mail($email, $subject, $message);
}*/

/**
 * -------------------
 * UPLOAD COMPANY LOGO
 * -------------------
 */
function upload_image($field_key)
{
	$errors = new WP_Error();
	$register_error_messages = malinky_register_error_messages();

	/** WordPress Administration File API */
	include_once( ABSPATH . 'wp-admin/includes/file.php' );

	/** WordPress Media Administration API */
	include_once( ABSPATH . 'wp-admin/includes/media.php' );

	if ( isset( $_FILES[ $field_key ] ) && ! empty( $_FILES[ $field_key ] ) && ! empty( $_FILES[ $field_key ]['name'] ) ) {
		$file = $_FILES[ $field_key ];

	    $check_file = getimagesize( $file['tmp_name'] );

	    if( !$check_file ) {
	        $errors->add( 'company_logo_error', __($register_error_messages['company_logo_error3']));
			return $errors;
		}

		$valid_image_type = array( IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG );

    	if( !in_array( $check_file[2], $valid_image_type ) ) {
	        $errors->add( 'company_logo_error', __($register_error_messages['company_logo_error3']));
			return $errors;
    	}

		/*if ( $_FILES[ $field_key ]["type"] != "image/jpeg" && $_FILES[ $field_key ]["type"] != "image/gif" && $_FILES[ $field_key ]["type"] != "image/png" ) {
			$errors->add( 'company_logo_error', __($register_error_messages['company_logo_error3']));
			return $errors;
		}*/

		if ( $_FILES[ $field_key ]["size"] > 102400) {
			$errors->add( 'company_logo_error', __($register_error_messages['company_logo_error4']));
			return $errors;
		}		

		add_filter( 'upload_dir', 'upload_dir' );

		$upload = wp_handle_upload( $file, array( 'test_form' => false ) );

		remove_filter('upload_dir', 'upload_dir' );

		if ( ! empty( $upload['error'] ) ) {
			$errors->add( 'company_logo_error', __($register_error_messages['company_logo_error2']));
			return $errors;
		} else {
			return $upload['url'];
		}
	}
}

/**
 * ---------------------------
 * CHANGE DIR FOR COMPANY LOGO
 * ---------------------------
 */
function upload_dir( $pathdata )
{
	$subdir             = '/job_listing_images';
	$pathdata['path']   = str_replace( $pathdata['subdir'], $subdir, $pathdata['path'] );
	$pathdata['url']    = str_replace( $pathdata['subdir'], $subdir, $pathdata['url'] );
	$pathdata['subdir'] = str_replace( $pathdata['subdir'], $subdir, $pathdata['subdir'] );
	return $pathdata;
}

/**
 * -----------------------------------------------------------------
 * ADD AND SAVE EMPLOYER ACCOUNT VALIDATION TO USERS SCREEN IN ADMIN
 * -----------------------------------------------------------------
 */
add_action('show_user_profile', 'malinky_register_employer_account_validation_fields');
add_action('edit_user_profile', 'malinky_register_employer_account_validation_fields');

function malinky_register_employer_account_validation_fields($user)
{ ?>
	<h3>Validated User</h3>
	<table class="form-table">
		<tr>
			<th><label for="validation_code">Validation Code</label></th>
			<td>
				<input type="text" name="validation_code" id="validation_code" value="<?php echo esc_attr(get_user_meta($user->ID, '_validation_code', true)); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="validated_user">Validated User</label></th>
			<td>
				<input type="checkbox" name="validated_user" id="validated_user" value="true" <?php checked(get_user_meta($user->ID, '_validated_user', true), 'true'); ?> /><br />
			</td>			
		</tr>
	</table>
<?php }

add_action('personal_options_update', 'malinky_register_save_employer_account_validation_fields');
add_action('edit_user_profile_update', 'malinky_register_save_employer_account_validation_fields');

function malinky_register_save_employer_account_validation_fields($user_id)
{
	if (!current_user_can( 'edit_user', $user_id))
		return false;

	/* Copy and paste this line for additional fields. Make sure to change 'twitter' to the field ID. */
	update_usermeta($user_id, '_validation_code', $_POST['validation_code']);
	if(empty($_POST['validated_user'])) {
		update_usermeta($user_id, '_validated_user', 'false');
	} else {
		update_usermeta($user_id, '_validated_user', $_POST['validated_user']);		
	}
}