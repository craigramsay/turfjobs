<?php

add_shortcode( 'malinky-register-register-form', 'malinky_register_register_form_shortcode' );

function malinky_register_register_form_shortcode()
{
	include( MALINKY_REGISTER_PLUGIN_DIR . '/forms/malinky-register-register-form.php' );
	return;
	//second option
	/*ob_start();
	include( MALINKY_REGISTER_PLUGIN_DIR . '/forms/malinky-register-register-form.php' );
	$form = ob_get_clean();
	return $form;*/
	//third option	
	/*$form = include( MALINKY_REGISTER_PLUGIN_DIR . '/forms/malinky-register-register-form.php' );
	if ($form) return;*/
}

add_shortcode( 'malinky-register-profile-form', 'malinky_register_profile_form_shortcode' );

function malinky_register_profile_form_shortcode()
{
	include( MALINKY_REGISTER_PLUGIN_DIR . '/forms/malinky-register-profile-form.php' );
	return;	
}