/**
 * -----------------------------
 * ADD JOB FORM VALIDATION
 * -----------------------------
 */
jQuery(document).ready(function($){              

    // validate contact form on keyup and submit
    $("#mc-embedded-subscribe-form").validate({
 
        invalidHandler: function(event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();
            if (errors) {
                try {
                    var toFocus = $(validator.findLastActive() || validator.errorList.length && validator.errorList[0].element || []);
                    if (toFocus.is("textarea")) {
                        tinymce.get(toFocus.attr('id')).focus();
                        var el = document.getElementById('wp-' + toFocus.attr('id') + '-wrap');
                        el.scrollIntoView(true);
                    } else {
                        toFocus.filter(":visible").focus();
                    }
                } catch(e) {
                    // ignore IE throwing errors when focusing hidden elements
                } 
            }
        },

        //set the rules for the fild names
        rules: {
            EMAIL: {
                required: true,
                email: true
            }                                           
        },
 
        //set error messages
        messages: {          
            EMAIL: {
                required: "Please enter a valid email address.",
                email: "Please enter a valid email address."
            }                                                                           
        },

        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
                error.insertAfter(element);
        }

    });

    // validate contact form on keyup and submit
    $("#mc-embedded-subscribe-form-mob").validate({
 
        invalidHandler: function(event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();
            if (errors) {
                try {
                    var toFocus = $(validator.findLastActive() || validator.errorList.length && validator.errorList[0].element || []);
                    if (toFocus.is("textarea")) {
                        tinymce.get(toFocus.attr('id')).focus();
                        var el = document.getElementById('wp-' + toFocus.attr('id') + '-wrap');
                        el.scrollIntoView(true);
                    } else {
                        toFocus.filter(":visible").focus();
                    }
                } catch(e) {
                    // ignore IE throwing errors when focusing hidden elements
                } 
            }
        },

        //set the rules for the fild names
        rules: {
            EMAIL: {
                required: true,
                email: true
            }                                           
        },
 
        //set error messages
        messages: {          
            EMAIL: {
                required: "Please enter a valid email address.",
                email: "Please enter a valid email address."
            }                                                                           
        },

        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
                error.insertAfter(element);
        }

    });

});


