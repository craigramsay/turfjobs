/**
 * -----------------------------
 * ADD JOB FORM VALIDATION
 * -----------------------------
 */
jQuery(document).ready(function($){

    //allows empty tiny mce to validate
    jQuery.validator.setDefaults({
        ignore: ''
    });

    jQuery.validator.addMethod("valid_select", function(value, element) {
        return this.optional(element) || value != 'please_choose';
    }, "Please Choose.");

    jQuery.validator.addMethod("valid_date", function(value, element) {
        return this.optional(element) || /^(\d{2})-(\d{2})-(\d{4})$/.test(value);
    }, "Please enter a valid date."); 

    jQuery.validator.addMethod("valid_salary", function(value, element) {
        return this.optional(element) || /^[0-9 \-,.]+$/.test(value);
    }, "Please enter a valid salary amount.");   

    jQuery.validator.addMethod("valid_lower_letters_hyphen", function(value, element) {
        return this.optional(element) || /^[a-z\-]+$/.test(value);
    }, "Small letters and hyphen only please"); 

    jQuery.validator.addMethod("valid_postcode", function(value, element) {
        return this.optional(element) || /^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$/i.test(value);
    }, "Please enter a valid postcode.");                

    // validate contact form on keyup and submit
    $("#submit-job-form").validate({
 
        invalidHandler: function(event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();
            if (errors) {
                try {
                    var toFocus = $(validator.findLastActive() || validator.errorList.length && validator.errorList[0].element || []);
                    if (toFocus.is("textarea")) {
                        tinymce.get(toFocus.attr('id')).focus();
                        var el = document.getElementById('wp-' + toFocus.attr('id') + '-wrap');
                        el.scrollIntoView(true);
                    } else {
                        toFocus.filter(":visible").focus();
                    }
                } catch(e) {
                    // ignore IE throwing errors when focusing hidden elements
                } 
            }
        },

        //set the rules for the fild names
        rules: {
            job_title: {
                required: true
            },
            job_type: {
                valid_select: true
            },
            job_category: {
                valid_select: true
            },
            job_description: {
                required: true
            },
            job_location: {
                valid_select: true
            },    
            job_person_specification: {
                required: true
            },                  
            job_employers_reference: {
                required: true
            },
            job_expires: {
                required: true,
                valid_date: true
            },                  
            job_how_to_apply: {
                required: true
            },
            job_salary_type: {
                required: true
            },                  
            job_salary_amount: {
                valid_salary: true
            },
            job_company_county: {
                valid_select: true,
                valid_lower_letters_hyphen: true
            },
            job_company_postcode: {
                valid_postcode: true
            },
            job_company_website: {
                url: true,
            },
            job_company_twitter: {
                url: true,
            },
            job_company_facebook: {
                url: true,
            },
            job_company_googleplus: {
                url: true,
            }                                                    
        },
 
        //set error messages
        messages: {          
            job_title: "Please enter a job title.",
            job_type: "Please choose a job type.",
            job_category: "Please choose a job category.",
            job_description: "Please enter a job description.",            
            job_location: "Please choose a job location.",
            job_person_specification: "Please enter a person specification.",
            job_employers_reference: "Please enter an employers reference.",
            job_expires: {
                required: "Please enter a valid date.",
                valid_date: "Please enter a valid date."
            },
            job_how_to_apply: "Please enter how to apply.",
            job_salary_type: "Please choose a salary type.",
            job_salary_amount: "Please enter a valid salary amount.", 
            job_company_county: {
                valid_select: "Please choose a county.",
                valid_lower_letters_hyphen: "Please choose a county."
            },  
            job_company_postcode: {
                valid_postcode: "Please enter a valid postcode."
            },
            job_company_website: "Please enter a valid company website URL.",
            job_company_twitter: "Please enter a valid company Twitter URL.",
            job_company_facebook: "Please enter a valid company Facebook URL.",
            job_company_googleplus: "Please enter a valid company Google Plus URL."                                                                             
        },

        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
                if ($(element).is("textarea")) {
                    error.insertAfter(element.parent());    
                } else if ($(element).is(":radio")) {
                    error.appendTo(element.parent()); 
                } else {
                    error.insertAfter(element);
                }
                if ( $(element).is("textarea") && $(element).hasClass('error') ) {
                    $(element).parents().eq(2).next().children('.field_error_icon').addClass('field_error_cross');
                } else if ($(element).hasClass('error')) {
                    $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');
                }
        },

        success: function(label) {
            $(label).parent().next().children('.field_error_icon').addClass('field_error_tick');
            $(label).parents().eq(1).next().children('.field_error_icon').addClass('field_error_tick');
            label.css('margin-top', '0');
            label.css('margin-bottom', '0');
        },

        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_tick');            
            $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');            
            $(element).next('p.error').css('margin-top', '1em');
            $(element).next('p.error').css('margin-bottom', '1em');
            if ($(element).is("textarea")) {
                $(element).parents().eq(2).next().children('.field_error_icon').removeClass('field_error_tick');            
                $(element).parents().eq(2).next().children('.field_error_icon').addClass('field_error_cross');            
                $(element).parent().next('p.error').css('margin-top', '1em');
                $(element).parent().next('p.error').css('margin-bottom', '1em');
            }
        },

        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_cross');
            $(element).parent().next().children('.field_error_icon').addClass('field_error_tick');
            $(element).next('p.error').css('margin-top', '0');
            $(element).next('p.error').css('margin-bottom', '0');
            if ($(element).is("textarea")) {
                $(element).parents().eq(2).next().children('.field_error_icon').removeClass('field_error_cross');            
                $(element).parents().eq(2).next().children('.field_error_icon').addClass('field_error_tick');            
                $(element).parent().next('p.error').css('margin-top', '0');
                $(element).parent().next('p.error').css('margin-bottom', '0');
            }            
        }

    });
    
    //ensure tiny mce is saved before validation otherwise submit must be clicked twice to clear any errors
    $('#submit').click(function() {
        tinymce.triggerSave();       
    });

    //ensure tinymce is validated after each one is deactivated
    if (typeof(tinyMCE) != "undefined") {
        setTimeout(function () {        
            for (var i = 0; i < tinymce.editors.length; i++) {
                tinymce.editors[i].onChange.add(function (editor, e) {
                    tinymce.triggerSave();
                    $("#" + editor.id).valid();
                });
           }
        }, 1000);
    }

});


