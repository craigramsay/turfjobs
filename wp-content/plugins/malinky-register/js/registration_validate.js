/**
 * -----------------------------
 * REGISTRATION FORM VALIDATION
 * -----------------------------
 */
jQuery(document).ready(function($){
    
    jQuery.validator.addMethod("valid_select", function(value, element) {
        return this.optional(element) || value != 'please_choose';
    }, "Please Choose.");

    jQuery.validator.addMethod("valid_letters", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
    }, "Letters only please");  

    jQuery.validator.addMethod("valid_lower_letters_hyphen", function(value, element) {
        return this.optional(element) || /^[a-z\-]+$/.test(value);
    }, "Small letters and hyphen only please"); 

    jQuery.validator.addMethod("valid_numbers_space", function(value, element) {
        return this.optional(element) || /^[0-9 ]+$/i.test(value);
    }, "Numbers and space only please");   

    jQuery.validator.addMethod("valid_registration_password", function(value, element) {
        return this.optional(element) || /^(?=.*\d)(?=.*[A-Za-z])[a-zA-Z0-9]+$/.test(value);
    }, "One letter one number please");  

    jQuery.validator.addMethod("valid_postcode", function(value, element) {
        return this.optional(element) || /^(GIR 0AA)|(TDCU 1ZZ)|(ASCN 1ZZ)|(BIQQ 1ZZ)|(BBND 1ZZ)|(FIQQ 1ZZ)|(PCRN 1ZZ)|(STHL 1ZZ)|(SIQQ 1ZZ)|(TKCA 1ZZ)|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}$/i.test(value);
    }, "Valid postcode please");                
 
    // validate contact form on keyup and submit
    $("#register_form").validate({
 
        //set the rules for the fild names
        rules: {
            organisation_type: {
                valid_select: true,
                digits: true
            },
            name_title: {
                valid_select: true,
                valid_letters: true
            },
            first_name: {
                required: true
            },
            last_name: {
                required: true
            },
            phone_number: {
                required: true,
                valid_numbers_space: true
            },    
            user_login: {
                required: true,
                email: true,
            },                  
            user_email: {
                required: true,
                email: true,
                equalTo: "#user_login"
            },
            password: {
                required: true, 
                valid_registration_password: true
            },                  
            confirm_password: {
                required: true,
                equalTo: "#password",
                valid_registration_password: true
            },
            update_password: {
                valid_registration_password: true
            },                  
            update_confirm_password: {
                equalTo: "#update_password",
                valid_registration_password: true
            },            
            company_name: {
                required: true  
            },
            company_address: {
                required: true  
            },
            company_town: {
                required: true
            },  
            company_county: {
                valid_select: true,
                valid_lower_letters_hyphen: true
            },
            company_postcode: {
                required: {
                    depends: function(element) {
                        return $('#company_county option:selected').parent().attr('label') != 'Ireland' && $('#company_county option:selected').parent().attr('label') != 'Europe';
                    }
                },
                valid_postcode: {
                    depends: function(element) {
                        return $('#company_county option:selected').parent().attr('label') != 'Ireland' && $('#company_county option:selected').parent().attr('label') != 'Europe';
                    }
                },
            },
            company_website: {
                url: true,
            },
            company_twitter: {
                url: true,
            },
            company_facebook: {
                url: true,
            },
            company_googleplus: {
                url: true,
            },
            company_logo: {
                required: true,
            },
            register_tandc: {
                required: true,
            }
        },
 
        //set error messages
        messages: {
            organisation_type: {
                valid_select: "Please choose an organisation type.",
                digits: "Please choose an organisation type."
            },
            name_title: {
                valid_select: "Please choose a title.",
                valid_letters: "Please choose a title.",
            },            
            first_name: "Please enter your first name.",
            last_name: "Please enter your surname.",            
            phone_number: {
                required: "Please enter your phone number.",
                valid_numbers_space: "Please enter a valid phone number."
            },
            user_login: {
                required: "Please enter a valid email address.",
                email: "Please enter a valid email address.",
            },
            user_email: {
                required: "Please enter a valid email address.",
                email: "Please enter a valid email address.",
                equalTo: "Please ensure your email addresses match."
            },
            password: {
                required: "Please enter a password.",
                valid_registration_password: "Your password must contain atleast one letter and one number and no special characters."
            },
            confirm_password: {
                required: "Please confirm your password.",
                equalTo: "Please ensure your passwords match.",
                valid_registration_password: "Your password must contain atleast one letter and one number and no special characters."
            },
            update_password: {
                valid_registration_password: "Your password must contain atleast one letter and one number and no special characters."
            },
            update_confirm_password: {
                equalTo: "Please ensure your passwords match.",
                valid_registration_password: "Your password must contain atleast one letter and one number and no special characters."
            },            
            company_name: "Please enter your company name.",
            company_address: "Please enter your company address.", 
            company_town: "Please enter your company town.",  
            company_county: {
                valid_select: "Please choose a county.",
                valid_lower_letters_hyphen: "Please choose a county."
            },  
            company_postcode: {
                required: "Please enter your company postcode.",
                valid_postcode: "Please enter a valid postcode."
            },
            company_website: "Please enter a valid company website URL.",
            company_twitter: "Please enter a valid company Twitter URL.",
            company_facebook: "Please enter a valid company Facebook URL.",
            company_googleplus: "Please enter a valid company Google Plus URL.",
            company_logo: "Please upload a company logo." ,
            register_tandc: "Please accept our terms and conditions."                                                                           
        },
 
        //our custom error placement
        errorElement: "p",
        errorPlacement: function(error, element) {
            error.insertAfter(element);
            if ($(element).hasClass('error')) {
                $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');
            }
        },

        success: function(label) {
            $(label).parent().next().children('.field_error_icon').addClass('field_error_tick');
            label.css('margin-top', '0');
            label.css('margin-bottom', '0');
        },

        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_tick');            
            $(element).parent().next().children('.field_error_icon').addClass('field_error_cross');            
            $(element).next('p.error').css('margin-top', '1em');
            $(element).next('p.error').css('margin-bottom', '1em');
        },

        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).parent().next().children('.field_error_icon').removeClass('field_error_cross');
            $(element).parent().next().children('.field_error_icon').addClass('field_error_tick');
            $(element).next('p.error').css('margin-top', '0');
            $(element).next('p.error').css('margin-bottom', '0');
        }        

    });

    $('#company_county').change(function() {
        if ($('#company_county option:selected').parent().attr('label') == 'Ireland') {
            $('label[for=company_postcode] small').text(' (optional)');
        } else {
            $('label[for=company_postcode] small').text(' (required)');
        }
    });

});