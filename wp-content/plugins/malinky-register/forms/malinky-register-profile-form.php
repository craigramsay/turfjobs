<?php
//check if a user is logged in
if (is_user_logged_in()) {
	global $current_user;
    get_currentuserinfo();
	//$current_user = malinky_register_get_current_user();
} else {
    wp_safe_redirect(site_url('wp-login.php'));
    exit();
}

//the below is used as employer/edit-profile is accessible also using employer/edit-profile?user_id=XX
if ($_GET['user_id']) {
	 if (is_numeric($_GET['user_id'])) {
		//check if employer and if user_id matches logged in user and if they can edit their own profile if not die
		if ( ($current_user->ID != $_GET['user_id'] || !current_user_can('edit_user', $current_user->ID)) && (in_array('employer', (array) $current_user->roles)) ) {
			wp_die(__('You can\'t edit this profile.'));
		}	
		
		//check if admin and if trying to edit their own profile die they should access this through wp-admin
		if ( ($current_user->ID == $_GET['user_id'] && current_user_can('edit_user', $current_user->ID) && in_array('administrator', (array) $current_user->roles)) ) {
			wp_die(__('Please use the admin section to edit your profile.'));
		}
	} else {
		//die if user id isn't numeric
		wp_die(__('Invalid User.'));
	}
//if there is no user_id in url and it is an admin they need to access page through wp-admin	
} elseif (!$_GET['user_id'] && current_user_can('edit_users') && in_array('administrator', (array) $current_user->roles)) {
	wp_die(__('Please use the admin section to edit your profile.'));
}

//if it's a logged in employer tring to edit their own profile carry on
if ( current_user_can('edit_user', $current_user->ID) && in_array('employer', (array) $current_user->roles) ) {
	
	//get user profile data
	$profile_user = get_userdata($current_user->ID);

	if(!$profile_user)
		wp_die(__('There was an error accessing the user information.'));

	if (!empty($_POST['update_profile'])) :
		$organisation_type 		= isset($_POST['organisation_type']) ? $_POST['organisation_type'] : '';	
		$name_title 			= isset($_POST['name_title']) ? $_POST['name_title'] : '';
		$first_name 			= isset($_POST['first_name']) ? $_POST['first_name'] : '';
		$last_name 				= isset($_POST['last_name']) ? $_POST['last_name'] : '';
		$phone_number 			= isset($_POST['phone_number']) ? $_POST['phone_number'] : '';	
		$user_login 			= isset($_POST['user_login']) ? $_POST['user_login'] : '';
		$user_email 			= isset($_POST['user_email']) ? $_POST['user_email'] : '';
		$password 				= isset($_POST['update_password']) ? $_POST['update_password'] : '';
		$confirm_password 		= isset($_POST['update_confirm_password']) ? $_POST['update_confirm_password'] : '';	
		$company_name 			= isset($_POST['company_name']) ? $_POST['company_name'] : '';
		$company_address 		= isset($_POST['company_address']) ? $_POST['company_address'] : '';
		$company_town 			= isset($_POST['company_town']) ? $_POST['company_town'] : '';
		$company_county 		= isset($_POST['company_county']) ? $_POST['company_county'] : '';
		$company_postcode		= isset($_POST['company_postcode']) ? $_POST['company_postcode'] : '';
		$company_website		= isset($_POST['company_website']) ? $_POST['company_website'] : '';
		$company_twitter		= isset($_POST['company_twitter']) ? $_POST['company_twitter'] : '';
		$company_facebook		= isset($_POST['company_facebook']) ? $_POST['company_facebook'] : '';
		$company_googleplus		= isset($_POST['company_googleplus']) ? $_POST['company_googleplus'] : '';
		$company_logo			= isset($_FILES['update_company_logo']) ? $_FILES['update_company_logo'] : '';
		$register_marketing_1 	= isset($_POST['register_marketing_1']) ? $_POST['register_marketing_1'] : '';	
		$company_logo_old	= $profile_user->_company_logo;						
		$errors = malinky_register_update_employer($organisation_type, $name_title, $first_name, $last_name, $phone_number, $user_login, $user_email, $password, $confirm_password, $company_name, $company_address, $company_town, $company_county, $company_postcode, $company_website, $company_twitter, $company_facebook, $company_googleplus, $company_logo, $register_marketing_1, $profile_user);
		if (is_wp_error($errors)) :
			$error_messages = $errors->errors;
		endif;
	else :
		$organisation_type  	= $profile_user->_organisation_type;
		$name_title 			= $profile_user->_name_title;
		$first_name 			= $profile_user->first_name;
		$last_name 				= $profile_user->last_name;
		$phone_number 			= $profile_user->_company_phone_number;
		$user_login 			= $profile_user->user_login;
		$user_email 			= $profile_user->user_email;
		$company_name 			= $profile_user->_company_name;
		$company_address 		= $profile_user->_company_address;
		$company_town 			= $profile_user->_company_town;
		$company_county 		= $profile_user->_company_county;
		$company_postcode		= $profile_user->_company_postcode;
		$company_website		= $profile_user->_company_website;
		$company_twitter		= $profile_user->_company_twitter;
		$company_facebook		= $profile_user->_company_facebook;
		$company_googleplus		= $profile_user->_company_googleplus;
		$company_logo_old		= $profile_user->_company_logo;
		$register_marketing_1	= $profile_user->_accept_marketing_1;
	endif;
?>
	<?php get_header('employer'); ?>

	<div id="content" class="clearfix">
		<div class="col col_span_10_10">

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h1><?php the_title(); ?></h1>
				<div id="register_form_intro" class="clearfix">
					<div class="col col_span_10_10">
						<p>To review or update your company and contact details, complete or amend the sections below.</p>
					</div>
				</div>
				<div id="register" class="clearfix">
					<div class="col col_span_10_10">
						<form id="register_form" action="" method="post" class="employer_form" enctype="multipart/form-data">

							<fieldset id="fieldset_register_login_details" class="employer_fieldset">
								<legend>Login Details</legend>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="organisation_type"><?php _e('Organisation Type') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<?php
										$terms = malinky_register_get_single_taxonomy_terms('job_listing_organisation_type', array('hide_empty' => false));
										echo malinky_register_register_dropdown('organisation_type', $terms, $organisation_type);
										if ($error_messages['organisation_type_error'][0]) echo '<p class="error">' . $error_messages['organisation_type_error'][0] . '</p>';
										?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="name_title"><?php _e('Title') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<?php
										$titles = array(
											'mr'	=> 'Mr',
											'mrs'	=> 'Mrs',
											'miss'	=> 'Miss',
										);
										echo malinky_register_register_dropdown('name_title', $titles , $name_title);
										if ($error_messages['name_title_error'][0]) echo '<p class="error">' . $error_messages['name_title_error'][0] . '</p>';
										?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="first_name"><?php _e('First Name') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">
										<input type="text" class="input-text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr(wp_unslash($first_name)); ?>" />
										<?php if ($error_messages['first_name_error'][0]) echo '<p class="error">' . $error_messages['first_name_error'][0] . '</p>'; ?>	
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>		
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="last_name"><?php _e('Surname') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="text" class="input-text" name="last_name" id="last_name" class="input" value="<?php echo esc_attr(wp_unslash($last_name)); ?>" />
										<?php if ($error_messages['last_name_error'][0]) echo '<p class="error">' . $error_messages['last_name_error'][0] . '</p>'; ?>	
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>		
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="phone_number"><?php _e('Phone Number') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="text" class="input-text" name="phone_number" id="phone_number" class="input" value="<?php echo esc_attr(wp_unslash($phone_number)); ?>" />
										<?php if ($error_messages['phone_number_error'][0]) echo '<p class="error">' . $error_messages['phone_number_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="user_login"><?php _e('Email Address') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="text" class="input-text" name="user_login" id="user_login" class="input" value="<?php echo esc_attr(wp_unslash($user_login)); ?>" />
										<small>Your email address will be your username.</small>
										<?php if ($error_messages['user_login_error'][0]) echo '<p class="error">' . $error_messages['user_login_error'][0] . '</p>'; ?>
										<?php if ($error_messages['email_match_error'][0]) echo '<p class="error">' . $error_messages['email_match_error'][0] . '</p>'; ?>
										<?php if ($error_messages['email_exists'][0]) echo '<p class="error">' . $error_messages['email_exists'][0] . '</p>'; ?>	
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>		
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="user_email"><?php _e('Confirm Email Address') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="text" class="input-text" name="user_email" id="user_email" class="input" value="<?php echo esc_attr(wp_unslash($user_email)); ?>" />
										<?php if ($error_messages['confirm_email_error'][0]) echo '<p class="error">' . $error_messages['confirm_email_error'][0] . '</p>'; ?>
										<?php if ($error_messages['email_match_error'][0]) echo '<p class="error">' . $error_messages['email_match_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="update_password"><?php _e('Password') ?><small> (required if changing)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="password" class="input-text" name="update_password" id="update_password" class="input" value="<?php echo esc_attr(wp_unslash($password)); ?>" autocomplete="off" />
										<small>Atleast one letter and one number and no special characters.</small>
										<?php if ($error_messages['password_match_error'][0]) echo '<p class="error">' . $error_messages['password_match_error'][0] . '</p>'; ?>
										<?php if ($error_messages['password_format_error'][0]) echo '<p class="error">' . $error_messages['password_format_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>	
								</div>		

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="update_confirm_password"><?php _e('Confirm Password') ?><small> (required if changing)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="password" class="input-text" name="update_confirm_password" id="update_confirm_password" class="input" value="<?php echo esc_attr(wp_unslash($confirm_password)); ?>" autocomplete="off" />
										<small>Atleast one letter and one number and no special characters.</small>
										<?php if ($error_messages['password_match_error'][0]) echo '<p class="error">' . $error_messages['password_match_error'][0] . '</p>'; ?>
										<?php if ($error_messages['password_format_error'][0]) echo '<p class="error">' . $error_messages['password_format_error'][0] . '</p>'; ?>	
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>		

							</fieldset>		

							<fieldset id="fieldset_register_company_details" class="employer_fieldset">
								<legend>Company Details</legend>

								<div class="field clearfix">
									<div class="col col_span_3_10">			
										<label for="company_name"><?php _e('Company Name') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="text" class="input-text" name="company_name" id="company_name" class="input" value="<?php echo esc_attr(wp_unslash($company_name)); ?>" />
										<?php if ($error_messages['company_name_error'][0]) echo '<p class="error">' . $error_messages['company_name_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>		

								<div class="field clearfix">
									<div class="col col_span_3_10">		
										<label for="company_address"><?php _e('Address') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">		
										<input type="text" class="input-text" name="company_address" id="company_address" class="input" value="<?php echo esc_attr(wp_unslash($company_address)); ?>" />
										<?php if ($error_messages['company_address_error'][0]) echo '<p class="error">' . $error_messages['company_address_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>	
								
								<div class="field clearfix">
									<div class="col col_span_3_10">		
										<label for="company_town"><?php _e('Town') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="text" class="input-text" name="company_town" id="company_town" class="input" value="<?php echo esc_attr(wp_unslash($company_town)); ?>" />
										<?php if ($error_messages['company_town_error'][0]) echo '<p class="error">' . $error_messages['company_town_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="company_county"><?php _e('County') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">
										<?php	
										$counties = malinky_register_get_counties();
										echo malinky_register_register_dropdown_county('company_county', $counties, $company_county);
										if ($error_messages['company_county_error'][0]) echo '<p class="error">' . $error_messages['company_county_error'][0] . '</p>';
										?>	
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>	

								<div class="field clearfix">
									<div class="col col_span_3_10">		
										<label for="company_postcode"><?php _e('Postcode') ?><small> (required)</small></label>
									</div>
									<div class="col col_span_6_10">		
										<input type="text" class="input-text" name="company_postcode" id="company_postcode" class="input" value="<?php echo esc_attr(wp_unslash($company_postcode)); ?>" />
										<?php if ($error_messages['company_postcode_error'][0]) echo '<p class="error">' . $error_messages['company_postcode_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="company_website"><?php _e('Website URL') ?><small> (optional)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="text" class="input-text" name="company_website" id="company_website" class="input" value="<?php echo esc_attr(wp_unslash($company_website)); ?>" />
										<small>Example: http://www.turfjobs.co.uk</small>
										<?php if ($error_messages['company_website_error'][0]) echo '<p class="error">' . $error_messages['company_website_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="company_twitter"><?php _e('Twitter URL') ?><small> (optional)</small></label>
									</div>
									<div class="col col_span_6_10">
										<input type="text" class="input-text" name="company_twitter" id="company_twitter" class="input" value="<?php echo esc_attr(wp_unslash($company_twitter)); ?>" />
										<small>Example: http://www.twitter.com/turfjobs</small>
										<?php if ($error_messages['company_twitter_error'][0]) echo '<p class="error">' . $error_messages['company_twitter_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="company_facebook"><?php _e('Facebook URL') ?><small> (optional)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="text" class="input-text" name="company_facebook" id="company_facebook" class="input" value="<?php echo esc_attr(wp_unslash($company_facebook)); ?>" />
										<small>Example: http://www.facebook.com/turfjobs</small>
										<?php if ($error_messages['company_facebook_error'][0]) echo '<p class="error">' . $error_messages['company_facebook_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="company_googleplus"><?php _e('Google Plus URL') ?><small> (optional)</small></label>
									</div>
									<div class="col col_span_6_10">	
										<input type="text" class="input-text" name="company_googleplus" id="company_googleplus" class="input" value="<?php echo esc_attr(wp_unslash($company_googleplus)); ?>" />
										<small>Example: http://plus.google.com/+turfjobscouk</small>
										<?php if ($error_messages['company_googleplus_error'][0]) echo '<p class="error">' . $error_messages['company_googleplus_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="update_company_logo"><?php _e('Company Logo') ?><small> (required if changing)</small></label>
									</div>
									<div class="col col_span_6_10">	
									<input type="file" class="input-text" name="update_company_logo" id="update_company_logo" />
									<small>Max. file size 100kb. Allowed images: jpg, gif, png</small>
									<?php if ($error_messages['company_logo_error'][0]) echo '<p class="error">' . $error_messages['company_logo_error'][0] . '</p>'; ?>
									<img id="employer_form_company_logo" src="<?php echo $company_logo_old; ?>" />
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>								

							</fieldset>				

							<fieldset id="fieldset_register_tandcs" class="employer_fieldset">
								<legend>Terms and Conditions</legend>

								<div class="field clearfix">
									<div class="col col_span_3_10">
										<label for="register_marketing_1">Email Partners<small> (optional)</small></label>
									</div>
									<div class="col col_span_6_10">	
									<input type="checkbox" class="input-text" name="register_marketing_1" id="register_marketing_1" value="1" <?php checked( esc_attr(wp_unslash($register_marketing_1)), 1 ); ?> /><br />Tick this box to receive information from carefully selected partners by email.
									<?php if ($error_messages['register_marketing_1_error'][0]) echo '<p class="error">' . $error_messages['register_marketing_1_error'][0] . '</p>'; ?>
									</div>
									<div class="col col_span_1_10">
										<div class="field_error_icon"></div>					
									</div>
								</div>																			

							</fieldset>

							<?php
							//wp registration error
							if ($error_messages['register_fail'][0]) echo '<p>' . $error_messages['register_fail'][0] . '</p>';
							?>	

							<?php wp_nonce_field( 'malinky_register_profile_form', 'malinky_register_profile_form_nonce' ); ?>
							<input type="submit" name="update_profile" class="button button_full_width" value="<?php esc_attr_e('Update Profile'); ?>" />

						</form>

					</div>
				</div><!-- #register_form -->
			
			</article>

		</div>
	</div>
	
<?php get_footer();

} else { 
	wp_die(__('You can\'t edit this profile.'));
}
?>
