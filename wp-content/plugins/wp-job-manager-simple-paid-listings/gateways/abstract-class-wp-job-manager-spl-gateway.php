<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) )
	exit;

/**
 * WP_Job_Manager_SPL_Gateway class.
 */
abstract class WP_Job_Manager_SPL_Gateway {

	protected $settings     = array();
	protected $gateway_id   = '';
	protected $gateway_name = '';

	/**
	 * __construct function.
	 */
	public function __construct() {
		add_filter( 'wp_job_manager_spl_gateways', array( $this, 'add_gateway' ) );
		add_filter( 'wp_job_manager_spl_settings', array( $this, 'add_settings' ) );
	}

	/**
	 * Add gateway to settings page
	 *
	 * @param array $gateways
	 */
	public function add_gateway( $gateways ) {
		$gateways[ $this->gateway_id ] = $this->gateway_name;
		return $gateways;
	}

	/**
	 * Add settings for the gateway
	 *
	 * @param array $settings
	 */
	public function add_settings( $settings ) {
		return array_merge( $settings, $this->settings );
	}

	/**
	 * Handle API calls (optional - used for IPN)
	 */
	public function api_handler() {}

	/**
	 * Handle the return page (optional - used for getting return values from gateways if posted)
	 */
	public function return_handler() {}

	/**
	 * Pay for a job listing action
	 */
	public function pay_for_listing( $job_id ) {
		return false;
	}

	/**
	 * Payment is complete - update listing
	 *
	 * @param  int $job_id
	 */
	public function payment_complete( $job_id ) {
		$job = get_post( $job_id );

		if ( $job->post_status == 'pending_payment' ) {
			$update_job                = array();
			$update_job['ID']          = $job_id;
			$update_job['post_status'] = get_option( 'job_manager_submission_requires_approval' ) ? 'pending' : 'publish';
			wp_update_post( $update_job );
		}
	}

	/**
	 * Send a message to admin about payment
	 *
	 * @param  int $job_id
	 * @param  string $message
	 */
	public function send_admin_email( $job_id, $message ) {
		$message = "Hi Ellie,\n\n" . $message . "\n\nView this job on the live site: " . get_post_permalink( $job_id ) . "\n\nView this job in the admin section if you are logged in: " . get_edit_post_link( $job_id, '' ) . "\n\nThanks.";

		wp_mail( array('ellie@turfjobs.co.uk', 'michael@turfjobs.co.uk'), sprintf( __('Job ID: %d Payment Update', 'job_manager_spl' ), $job_id ), $message );
	}

	/**
	 * Send a message to the customer about payment
	 *
	 * @param  int $job_id
	 * @param  string $message
	 */
	public function send_customer_email( $job_id, $stripe_email, $stripe_payment_id ) {

		$job_sub_total = get_option( 'job_manager_spl_listing_cost' ) / 1.2;
		$job_vat = get_option( 'job_manager_spl_listing_cost' ) - $job_sub_total;
		$job_total = get_option( 'job_manager_spl_listing_cost' );

		$message = '<table bgcolor="ffffff" align="center" border="0" cellpadding="0" cellspacing="0" width="480">';
		$message .= '<tr><td><img src="' . get_template_directory_uri() . '/img/logo.png" /></td></tr>';
		$message .= '<tr><td>&nbsp;</td></tr>';
		$message .= '<tr><td colspan="2">Company: Turf Links Ltd</td></tr>';
		$message .= '<tr><td>&nbsp;</td></tr>';
		$message .= '<tr><td colspan="2">Date: ' . date('d/m/Y') . '</td></tr>';
		$message .= '<tr><td colspan="2">Transaction ID: ' . $stripe_payment_id . '</td></tr>';
		$message .= '<tr><td>&nbsp;</td></tr>';
		$message .= '<tr><td colspan="2">Receipt for payment for job listing on www.turfjobs.co.uk.</td></tr>';
		$message .= '<tr><td>&nbsp;</td></tr>';
		$message .= '<tr><td colspan="2">Job ID: ' . $job_id . '</td></tr>';
		$message .= '<tr><td colspan="2">Job Title: ' . get_the_title( $job_id ) . '</td></tr>';
		$message .= '<tr><td>&nbsp;</td></tr>';
		//$message .= '<tr><td width="180">Sub Total:</td><td width="380">&pound;' . number_format( $job_sub_total, 2 ) . '</td></tr>';
		//$message .= '<tr><td width="180">VAT (at 20%):</td><td width="380">&pound;' . number_format( $job_vat, 2 ) . '</td></tr>';
		$message .= '<tr><td width="180">Total:</td><td width="380">&pound;' . number_format( $job_total, 2 ) . '</td></tr>';
		$message .= '<tr><td>&nbsp;</td></tr>';
		//$message .= '<tr><td colspan="2">Turf Links Ltd VAT number: 176 0265 10</td></tr>';
		$message .= '</table>';
				
		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-Type: text/html';
		$headers[] = 'Bcc: ellie@turfjobs.co.uk';
		$headers[] = 'Bcc: michael@turfjobs.co.uk';

		wp_mail( $stripe_email, sprintf( __('TurfJobs Payment Receipt', 'job_manager_spl' ), $job_id ), $message, $headers );
	}

}