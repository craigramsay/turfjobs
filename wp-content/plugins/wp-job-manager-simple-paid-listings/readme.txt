=== Simple Paid Listings ===
Contributors: mikejolley
Requires at least: 3.5
Tested up to: 3.5
Stable tag: 1.1.1
License: GNU General Public License v3.0

Add paid listing functionality. Set a price per listing and take payment via Stripe or PayPal before the listing becomes published.

= Documentation =

Usage instructions for this plugin can be found on the wiki: [https://github.com/mikejolley/WP-Job-Manager/wiki/Simple-Paid-Listings](https://github.com/mikejolley/WP-Job-Manager/wiki/Simple-Paid-Listings).

= Support Policy =

I will happily patch any confirmed bugs with this plugin, however, I will not offer support for:

1. Customisations of this plugin or any plugins it relies upon
2. Conflicts with "premium" themes from ThemeForest and similar marketplaces (due to bad practice and not being readily available to test)
3. CSS Styling (this is customisation work)

If you need help with customisation you will need to find and hire a developer capable of making the changes.

== Installation ==

To install this plugin, please refer to the guide here: [http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation)

== Changelog ==

= 1.1.1 =
* Fix PayPal headers for upcoming API changes

= 1.1.0 =
* Allow payment from job dashboard. Requires Job Manager 1.1.2.

= 1.0.3 =
* Fixed issue where expirey date was not set on new submissions

= 1.0.0 =
* First release.